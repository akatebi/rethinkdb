#!/bin/bash

source ./config.sh

# Stop machines

remove() {
  for node in $(seq 0 $2);
  do
    docker-machine stop $1$node
    docker-machine rm $1$node
  done
}

remove 'm' $managers;
remove 'w' $workers;
