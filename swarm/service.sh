#!/bin/bash

dmc() {
  docker-machine create -d virtualbox --virtualbox-memory=2048 $1
}

init() {
  docker swarm init --advertise-addr=$1
  docker network create -d overlay mynet;
  docker volume create --name rdb
}


rdb() {
  docker service create --name=rdb --network=mynet \
    --mount type=volume,source=rdb,target=/data \
    -p 8080:8080 -p 28015:28015 rethinkdb
}

web() {
  docker service create --name=web --network=mynet \
    -p 3000:3000 -p 4000:4000 akatebi/rethinkdb
}
