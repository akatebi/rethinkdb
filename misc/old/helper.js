import protodef from './protodef.js';
const termTypes = protodef.Term.TermType;
// const datumTypes = protodef.Datum.DatumType;
import net from 'net';


function createLogger(poolMaster, silent) {
  /* eslint no-console:0 */
  return message => {
    if (silent !== true) {
      console.error(message);
    }
    poolMaster.emit('log', message);
  };
}
export { createLogger };

function isPlainObject(obj) {
  return Object.prototype.toString.call(obj) === '[object Object]';
}
export { isPlainObject };

function toArray(args) {
  return Array.prototype.slice.call(args);
}
export { toArray };

function hasImplicit(arg) {
  if (Array.isArray(arg)) {
    if (arg[0] === termTypes.IMPLICIT_VAR) return true;

    if (Array.isArray(arg[1])) {
      for(let i=0; i<arg[1].length; i++) {
        if (hasImplicit(arg[1][i])) return true;
      }
    }
    if (isPlainObject(arg[2])) {
      return Object.keys(arg[2]).find(key => hasImplicit(arg[2][key]));
    }
  }
  else if (isPlainObject(arg)) {
    return Object.keys(arg).find(key => hasImplicit(arg[key]));
  }
  return false;
}
export { hasImplicit };

function loopKeys(obj, fn) {
  const keys = Object.keys(obj);
  let result;
  const keysLength = keys.length;
  for(let i=0; i<keysLength; i++) {
    result = fn(obj, keys[i]);
    if (result === false) return;
  }
}
export { loopKeys };

function convertPseudotype(obj, options) {
  const reqlType = obj.$reql_type$;
  if (reqlType === 'TIME' && options.timeFormat !== 'raw') {
    return new Date(obj.epoch_time * 1000);
  }
  else if (reqlType === 'GROUPED_DATA' && options.groupFormat !== 'raw') {
    const result = [];
    for (let i = 0, len = obj.data.length, ref; i < len; i++) {
      ref = obj.data[i];
      result.push({
        group: ref[0],
        reduction: ref[1],
      });
    }
    return result;
  }
  else if (reqlType === 'BINARY' && options.binaryFormat !== 'raw') {
    return new Buffer(obj.data, 'base64');
  }
  return obj;
}
function recursivelyConvertPseudotype(arg1, options) {
  let obj = arg1;
  if (Array.isArray(obj)) {
    for (let i = 0, len = obj.length; i < len; i++) {
      const value = obj[i];
      obj[i] = recursivelyConvertPseudotype(value, options);
    }
  }
  else if (obj && typeof obj === 'object') {
    Object.keys(obj).forEach(key => {
      const value = obj[key];
      obj[key] = recursivelyConvertPseudotype(value, options);
    });
    obj = convertPseudotype(obj, options);
  }
  return obj;
}
function makeAtom(response, options={}) {
  return recursivelyConvertPseudotype(response.r[0], options);
}
export { makeAtom };

function makeSequence(response, options={}) {
  return recursivelyConvertPseudotype(response.r, options);
}

export { makeSequence };

// function changeProto(object, other) {
//   object.__proto__ = other.__proto__;
// }
// export { changeProto };

// Try to extract the most global address
// Note: Mutate the input
function getCanonicalAddress(addresses) {
  // We suppose that the addresses are all valid, and therefore use loose regex
  for(let i=0; i<addresses.length; i++) {
    const addresse = addresses[i];
    if ((/^127(\.\d{1,3}){3}$/.test(addresse.host)) || (/0?:?0?:?0?:?0?:?0?:?0?:0?:1/.test(addresse.host))) {
      addresse.value = 0;
    }
    else if ((net.isIPv6(addresse.host)) && (/^[fF]|[eE]80:.*:.*:/.test(addresse.host))) {
      addresse.value = 1;
    }
    else if (/^169\.254\.\d{1,3}\.\d{1,3}$/.test(addresse.host)) {
      addresse.value = 2;
    }
    else if (/^192\.168\.\d{1,3}\.\d{1,3}$/.test(addresse.host)) {
      addresse.value = 3;
    }
    else if (/^172\.(1\d|2\d|30|31)\.\d{1,3}\.\d{1,3}$/.test(addresse.host)) {
      addresse.value = 4;
    }
    else if (/^10(\.\d{1,3}){3}$/.test(addresse.host)) {
      addresse.value = 5;
    }
    else if ((net.isIPv6(addresse.host)) && (/^[fF]|[cCdD].*:.*:/.test('addresse.host'))) {
      addresse.value = 6;
    }
    else {
      addresse.value = 7;
    }
  }
  let result = addresses[0];
  let max = addresses[0].value;
  for(let i=0; i<addresses.length; i++) {
    if (addresses[i].value > max) {
      result = addresses[i];
      max = addresses[i].value;
    }
  }
  return result;
}
export { getCanonicalAddress };

export const localhostAliases = {
  localhost: true,
  '127.0.0.1': true,
  '::1': true,
};

export function tryCatch(toTry, handleError) {
  try {
    toTry();
  }
  catch(err) {
    handleError(err);
  }
}

function splitCommaEqual(message) {
  const result = {};
  const messageParts = message.split(',');
  for(let i=0; i<messageParts.length; i++) {
    const equalPosition = messageParts[i].indexOf('=');
    result[messageParts[i].slice(0, equalPosition)] = messageParts[i].slice(equalPosition+1);
  }
  return result;
}
export { splitCommaEqual };

function xorBuffer(a, b) {
  const result = [];
  const len = Math.min(a.length, b.length);
  for(let i=0; i<len; i++) {
    result.push(a[i] ^ b[i]);
  }
  return new Buffer(result);
}
export { xorBuffer };

function compareDigest(a, b) {
  // let left = undefined;
  // const right = b;
  let result = undefined;
  if (a.length === b.length) {
    // left = a;
    result = 0;
  } else {
    // left = b;
    result = 1;
  }
  const len = Math.min(a.length, b.length);
  for(let i=0; i<len; i++) {
    result |= xorBuffer(a[i], b[i]);
  }
  return result === 0;
}
export { compareDigest };
