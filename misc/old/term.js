/* eslint new-cap:1 */

import { Observable } from 'rx';
import protodef from './protodef.js';
const termTypes = protodef.Term.TermType;

import Error from './error.js';
import * as helper from './helper.js';


class Term {

  static translateOptions(opts) {
    const translatedOpt = {};
    helper.loopKeys(opts, (options, key) => {
      const keyServer = Term.prototype._translateArgs[key] || key;
      translatedOpt[keyServer] = options[key];
    });
    return translatedOpt;
  }

  static Var(r, id) {
    const term = new Term(r);
    term._query.push(termTypes.VAR);
    term._query.push([new Term(r).expr(id)._query]);
    return term;
  }

  // Datums
  static Func(ro, func) {
    // We can retrieve the names of the arguments with
    // func.toString().match(/\(([^\)]*)\)/)[1].split(/\s*,\s*/)
    const r = ro;
    const term = new Term(r);
    term._query.push(termTypes.FUNC);
    const args = [];
    const argVars = [];
    const argNums = [];

    for(let i=0; i<func.length; i++) {
      argVars.push(new Term.Var(r, r.nextVarId));
      argNums.push(r.nextVarId);

      if (r.nextVarId === 9007199254740992) { // That seems like overdoing it... but well maybe...
        r.nextVarId = 0;
      }
      else {
        r.nextVarId++;
      }
    }

    let body = func.apply(func, argVars);
    if (body === undefined) throw new Error.ReqlDriverError(`Anonymous function returned \`undefined\`. Did you forget a \`return\`? In:\n${func.toString()}`, this._query);
    body = new Term(r).expr(body);
    args.push(new Term(r).expr(argNums));
    args.push(body);

    term._fillArgs(args);

    return term;
  }

  constructor(r, value, error) {
    // const self = this;
    const term = function(field) {
      if (Term.prototype._fastArity(arguments.length, 1) === false) {
        const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
        Term.prototype._arity(_args, 1, '(...)', self);
      }
      return term.bracket(field);
    };

    /* eslint no-proto:1 */
    term.__proto__ = this.__proto__;

    if (value === undefined) {
      term._query = [];
    }
    else {
      term._query = value;
    }
    term._r = r; // Keep a reference to r for global settings

    if (error !== undefined) {
      term._error = error;
      term._frames = [];
    }

    return term;
  }

  //=====================================//
  run(connection, opts = {}) {
    /* eslint camelcase:0 */
    if (this._error) {
      const error = new Error.ReqlRuntimeError(this._error, this._query,
        { b: this._frames });
      return Observable.error(error);
    }
    if (!connection._isOpen()) {
      const error = new Error.ReqlDriverError('`run` was called with a closed connection', this._query).setOperational();
      return Observable.error(error);
    }
    const profile = !true;
    const max_batch_rows = 10;
    const durability = 'hard';
    const options = { ...opts, profile, max_batch_rows, durability };
    return connection._send([...this.serialize(), options]);
  }

  serialize() {
    return [protodef.Query.QueryType.START, this._query];
  }

  toSerial() {
    return JSON.stringify(this.serialize());
  }

  dbCreate(db) {
    // Check for arity is done in r.prototype.dbCreate
    this._noPrefix(this, 'dbCreate');

    const term = new Term(this._r);
    term._query.push(termTypes.DB_CREATE);
    const args = [new Term(this._r).expr(db)._query];
    term._fillArgs(args);
    return term;
  }

  dbDrop(db) {
    this._noPrefix(this, 'dbDrop');

    const term = new Term(this._r);
    term._query.push(termTypes.DB_DROP);
    const args = [new Term(this._r).expr(db)._query];
    term._fillArgs(args);
    return term;
  }

  dbList() {
    this._noPrefix(this, 'dbList');

    const term = new Term(this._r);
    term._query.push(termTypes.DB_LIST);
    return term;
  }

  tableCreate(table, options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 1, 2, 'tableCreate', self);
    }


    const term = new Term(self._r);
    term._query.push(termTypes.TABLE_CREATE);

    const args = [];
    if (Array.isArray(self._query) && (self._query.length > 0)) {
      args.push(self); // Push db
    }
    args.push(new Term(self._r).expr(table));
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      // Check for non valid key
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'primaryKey')
            && (key !== 'durability')
            && (key !== 'shards')
            && (key !== 'replicas')
            && (key !== 'primaryReplicaTag')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`tableCreate\``, self._query, 'Available options are primaryKey <string>, durability <string>, shards <number>, replicas <number/object>, primaryReplicaTag <object>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  tableDrop(table) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'tableDrop', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.TABLE_DROP);

    const args = [];
    if (!Array.isArray(this._query) || (this._query.length > 0)) {
      args.push(this); // push db
    }
    args.push(new Term(this._r).expr(table));
    term._fillArgs(args);
    return term;
  }

  tableList() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'tableList', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.TABLE_LIST);

    const args = [];
    if (!Array.isArray(this._query) || (this._query.length > 0)) {
      args.push(this);
    }
    term._fillArgs(args);
    return term;
  }

  indexList() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'indexList', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.INDEX_LIST);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  indexCreate(name, func, opts) {
    let fn = func;
    let options = opts;
    if (this._fastArityRange(arguments.length, 1, 3) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 1, 3, 'indexCreate', this);
    }

    if ((options == null) && (helper.isPlainObject(fn))) {
      options = fn;
      fn = undefined;
    }

    const term = new Term(this._r);
    term._query.push(termTypes.INDEX_CREATE);
    const args = [this];
    args.push(new Term(this._r).expr(name));
    if (typeof fn !== 'undefined') args.push(new Term(this._r).expr(fn)._wrap());
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      // There is no need to translate here
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'multi') && (key !== 'geo')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`indexCreate\``, self._query, 'Available option is multi <bool> and geo <bool>');
        }
      });
      term._query.push(new Term(this._r).expr(options)._query);
    }
    return term;
  }

  indexDrop(name) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'indexDrop', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.INDEX_DROP);
    const args = [this, new Term(this._r).expr(name)];
    term._fillArgs(args);
    return term;
  }

  indexStatus() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    const term = new Term(this._r);
    term._query.push(termTypes.INDEX_STATUS);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  indexWait() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    const term = new Term(this._r);
    term._query.push(termTypes.INDEX_WAIT);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  indexRename(oldName, newName, options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 2, 3) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 2, 3, 'indexRename', self);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.INDEX_RENAME);
    const args = [this, new Term(this._r).expr(oldName), new Term(this._r).expr(newName)];
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if (key !== 'overwrite') {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`indexRename\``, self._query, 'Available options are overwrite <bool>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }


    return term;
  }

  changes(options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 0, 1, 'changes', self);
    }

    const term = new Term(self._r);
    term._query.push(termTypes.CHANGES);
    const args = [self];
    term._fillArgs(args);
    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'squash') && (key !== 'includeStates') && (key !== 'includeTypes')
            && (key !== 'includeInitial') && (key !== 'includeOffsets')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`changes\``, self._query,
              'Available options are squash <bool>, includeInitial <bool>, includeStates <bool>, includeOffsets <bool>, includeTypes <bool>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  insert(documents, options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 1, 2, 'insert', self);
    }

    const term = new Term(self._r);
    term._query.push(termTypes.INSERT);
    const args = [self, new Term(self._r).expr(documents)];
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'returnChanges') && (key !== 'durability') && (key !== 'conflict')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`insert\``, self._query, 'Available options are returnChanges <bool>, durability <string>, conflict <string>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  update(newValue, options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 1, 2, 'update', self);
    }

    const term = new Term(self._r);
    term._query.push(termTypes.UPDATE);
    const args = [self, new Term(self._r).expr(newValue)._wrap()];
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'returnChanges') && (key !== 'durability') && (key !== 'nonAtomic')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`update\``, self._query, 'Available options are returnChanges <bool>, durability <string>, nonAtomic <bool>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  replace(newValue, options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 1, 2, 'replace', self);
    }

    const term = new Term(self._r);
    term._query.push(termTypes.REPLACE);
    const args = [self, new Term(self._r).expr(newValue)._wrap()];
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'returnChanges') && (key !== 'durability') && (key !== 'nonAtomic')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`replace\``, self._query, 'Available options are returnChanges <bool>, durability <string>, nonAtomic <bool>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  delete(options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 0, 1, 'delete', self);
    }

    const term = new Term(self._r);
    term._query.push(termTypes.DELETE);
    const args = [self];
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'returnChanges') && (key !== 'durability')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`delete\``, self._query, 'Available options are returnChanges <bool>, durability <string>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  sync() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'sync', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SYNC);
    const args = [this._query];
    term._fillArgs(args);
    return term;
  }

  db(db) {
    this._noPrefix(this, 'db');
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'db', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.DB);
    const args = [new Term(this._r).expr(db)];
    term._fillArgs(args);
    return term;
  }

  table(table, options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 1, 2, 'table', self);
    }

    const term = new Term(self._r);
    term._query.push(termTypes.TABLE);

    const args = [];
    if (Array.isArray(self._query) && (self._query.length > 0)) {
      args.push(self);
    }
    args.push(new Term(self._r).expr(table));
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if (key !== 'readMode') {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`table\``, self._query, 'Available option is readMode <string>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  get(primaryKey) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'get', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.GET);
    const args = [this, new Term(this._r).expr(primaryKey)];
    term._fillArgs(args);
    return term;
  }

  getAll() {
    // We explicitly _args here, so fastArityRange is not useful
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}

    const term = new Term(this._r);
    term._query.push(termTypes.GET_ALL);

    const args = [];
    args.push(this);
    for(let i=0; i<_args.length-1; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    if ((_args.length > 0) && (helper.isPlainObject(_args[_args.length-1])) && (_args[_args.length-1].index !== undefined)) {
      term._fillArgs(args);
      term._query.push(new Term(this._r).expr(Term.translateOptions(_args[_args.length-1]))._query);
    }
    else if (_args.length > 0) {
      args.push(new Term(this._r).expr(_args[_args.length-1]));
      term._fillArgs(args);
    } else {
      term._fillArgs(args);
    }
    return term;
  }

  between(start, end, options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 2, 3) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 2, 3, 'between', self);
    }

    const term = new Term(self._r);
    term._query.push(termTypes.BETWEEN);
    const args = [self, new Term(self._r).expr(start), new Term(self._r).expr(end)];
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'index') && (key !== 'leftBound') && (key !== 'rightBound')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`between\``, self._query, 'Available options are index <string>, leftBound <string>, rightBound <string>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  minval() {
    const term = new Term(this._r);
    term._query.push(termTypes.MINVAL);
    return term;
  }

  maxval() {
    const term = new Term(this._r);
    term._query.push(termTypes.MAXVAL);
    return term;
  }

  filter(filter, options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 1, 2, 'filter', self);
    }

    const term = new Term(self._r);
    term._query.push(termTypes.FILTER);
    const args = [self, new Term(self._r).expr(filter)._wrap()];
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if (key !== 'default') {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`filter\``, self._query, 'Available option is filter');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  innerJoin(sequence, predicate) {
    if (this._fastArity(arguments.length, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 2, 'innerJoin', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.INNER_JOIN);
    const args = [this._query];
    args.push(new Term(this._r).expr(sequence)._query);
    args.push(new Term(this._r).expr(predicate)._wrap()._query);
    term._fillArgs(args);

    return term;
  }

  outerJoin(sequence, predicate) {
    if (this._fastArity(arguments.length, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 2, 'outerJoin', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.OUTER_JOIN);
    const args = [this];
    args.push(new Term(this._r).expr(sequence));
    args.push(new Term(this._r).expr(predicate)._wrap());
    term._fillArgs(args);

    return term;
  }

  eqJoin(rightKey, sequence, options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 2, 3) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 2, 3, 'eqJoin', self);
    }

    const term = new Term(self._r);
    term._query.push(termTypes.EQ_JOIN);
    const args = [self];
    args.push(new Term(self._r).expr(rightKey)._wrap());
    args.push(new Term(self._r).expr(sequence));
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'index') && (key !== 'ordered')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`eqJoin\``, self._query, 'Available options are index <string>, ordered <boolean>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  zip() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'zip', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.ZIP);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  map() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'map', this);

    const term = new Term(this._r);
    term._query.push(termTypes.MAP);
    const args = [];
    if (!Array.isArray(this._query) || (this._query.length > 0)) {
      args.push(this);
    }
    for(let i=0; i<_args.length-1; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    // Make sure that we don't push undefined if no argument is passed to map,
    // in which case the server will handle the case and return an error.
    if (_args.length> 0) {
      args.push(new Term(this._r).expr(_args[_args.length-1])._wrap());
    }
    term._fillArgs(args);

    return term;
  }

  withFields() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'withFields', this);

    const term = new Term(this._r);
    term._query.push(termTypes.WITH_FIELDS);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);

    return term;
  }

  concatMap(transformation) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'concatMap', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.CONCAT_MAP);
    const args = [this];
    args.push(new Term(this._r).expr(transformation)._wrap());
    term._fillArgs(args);

    return term;
  }

  orderBy() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'orderBy', this);

    const term = new Term(this._r);
    term._query.push(termTypes.ORDER_BY);

    const args = [this];
    for(let i=0; i<_args.length-1; i++) {
      if ((_args[i] instanceof Term) &&
          ((_args[i]._query[0] === termTypes.DESC) || (_args[i]._query[0] === termTypes.ASC))) {
        args.push(new Term(this._r).expr(_args[i]));
      }
      else {
        args.push(new Term(this._r).expr(_args[i])._wrap());
      }
    }
    // We actually don't need to make the difference here, but...
    if ((_args.length > 0) && (helper.isPlainObject(_args[_args.length-1])) && (_args[_args.length-1].index !== undefined)) {
      term._fillArgs(args);
      term._query.push(new Term(this._r).expr(Term.translateOptions(_args[_args.length-1]))._query);
    }
    else {
      if ((_args[_args.length-1] instanceof Term) &&
        ((_args[_args.length-1]._query[0] === termTypes.DESC) || (_args[_args.length-1]._query[0] === termTypes.ASC))) {
        args.push(new Term(this._r).expr(_args[_args.length-1]));
      }
      else {
        args.push(new Term(this._r).expr(_args[_args.length-1])._wrap());
      }
      term._fillArgs(args);
    }
    return term;
  }

  desc(field) {
    this._noPrefix(this, 'desc');
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'desc', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.DESC);
    const args = [new Term(this._r).expr(field)._wrap()];
    term._fillArgs(args);
    return term;
  }

  asc(field) {
    this._noPrefix(this, 'asc');
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'asc', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.ASC);
    const args = [new Term(this._r).expr(field)._wrap()];
    term._fillArgs(args);
    return term;
  }

  skip(value) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'skip', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SKIP);
    const args = [this, new Term(this._r).expr(value)];
    term._fillArgs(args);
    return term;
  }

  limit(value) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'limit', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.LIMIT);
    const args = [this, new Term(this._r).expr(value)];
    term._fillArgs(args);
    return term;
  }

  slice(start, end, options) {
    if (this._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 1, 3, 'slice', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SLICE);

    const args = [];
    args.push(this);
    args.push(new Term(this._r).expr(start));

    if ((end !== undefined) && (options !== undefined)) {
      args.push(new Term(this._r).expr(end));
      term._fillArgs(args);
      term._query.push(new Term(this._r).expr(Term.translateOptions(options))._query);
    }
    else if ((end !== undefined) && (options === undefined)) {
      if (helper.isPlainObject(end) === false) {
        args.push(new Term(this._r).expr(end));
        term._fillArgs(args);
      }
      else {
        term._fillArgs(args);
        term._query.push(new Term(this._r).expr(Term.translateOptions(end))._query);
      }
    }
    else { // end and options are both undefined
      term._fillArgs(args);
    }
    return term;
  }

  nth(value) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'nth', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.NTH);
    const args = [this._query, new Term(this._r).expr(value)];
    term._fillArgs(args);
    return term;
  }

  offsetsOf(predicate) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'indexesOf', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.OFFSETS_OF);
    const args = [this, new Term(this._r).expr(predicate)._wrap()];
    term._fillArgs(args);
    return term;
  }

  isEmpty() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'isEmpty', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.IS_EMPTY);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  union() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}

    const term = new Term(this._r);
    term._query.push(termTypes.UNION);
    const args = [];
    if (!Array.isArray(this._query) || (this._query.length > 0)) {
      args.push(this);
    }
    for(let i=0; i<_args.length-1; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    if ((_args.length > 1) && (helper.isPlainObject(_args[_args.length-1])) && (_args[_args.length-1].interleave !== undefined)) {
      term._fillArgs(args);
      term._query.push(new Term(this._r).expr(Term.translateOptions(_args[_args.length-1]))._query);
    }
    else if (_args.length > 0) {
      args.push(new Term(this._r).expr(_args[_args.length-1]));
      term._fillArgs(args);
    }
    return term;
  }

  sample(size) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'sample', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SAMPLE);
    const args = [this, new Term(this._r).expr(size)];
    term._fillArgs(args);
    return term;
  }

  reduce(func) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'reduce', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.REDUCE);
    const args = [this, new Term(this._r).expr(func)._wrap()];
    term._fillArgs(args);
    return term;
  }

  count(filter) {
    if (this._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 0, 1, 'count', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.COUNT);
    const args = [];
    args.push(this);
    if (filter !== undefined) {
      args.push(new Term(this._r).expr(filter)._wrap());
    }
    term._fillArgs(args);
    return term;
  }

  distinct(options) {
    const self= this;
    if (self._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 0, 1, 'distinct', self);
    }

    const term = new Term(self._r);
    term._query.push(termTypes.DISTINCT);
    const args = [self];
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      const keepGoing = true;
      helper.loopKeys(options, (obj, key) => {
        if ((keepGoing === true) && (key !== 'index')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`distinct\``, self._query, 'Available option is index: <string>');
          // keepGoing = false;
        }
      });
      if (keepGoing === true) {
        term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
      }
    }

    return term;
  }

  group() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    const self = this;
    self._arityRange(_args, 1, Infinity, 'group', self);

    const term = new Term(self._r);
    term._query.push(termTypes.GROUP);
    const args = [self];
    for(let i=0; i<_args.length-1; i++) {
      args.push(new Term(self._r).expr(_args[i])._wrap());
    }
    if (_args.length > 0) {
      if (helper.isPlainObject(_args[_args.length-1])) {
        helper.loopKeys(_args[_args.length-1], (obj, key) => {
          if ((key !== 'index')
          && (key !== 'multi')) {
            throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`group\``, self._query, 'Available options are index: <string>, multi <boolean>');
          }
        });
        term._fillArgs(args);
        term._query.push(new Term(self._r).expr(Term.translateOptions(_args[_args.length-1]))._query);
      }
      else {
        args.push(new Term(self._r).expr(_args[_args.length-1])._wrap());
        term._fillArgs(args);
      }
    }
    else {
      term._fillArgs(args);
    }

    return term;
  }

  split(separator, max) {
    if (this._fastArityRange(arguments.length, 0, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 0, 2, 'split', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SPLIT);
    const args = [this];
    if (separator !== undefined) {
      args.push(new Term(this._r).expr(separator));
      if (max !== undefined) {
        args.push(new Term(this._r).expr(max));
      }
    }
    term._fillArgs(args);

    return term;
  }

  ungroup() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'ungroup', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.UNGROUP);
    const args = [this._query];
    term._fillArgs(args);
    return term;
  }

  contains() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'contains', this);

    const term = new Term(this._r);
    term._query.push(termTypes.CONTAINS);
    const args = [this._query];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i])._wrap());
    }
    term._fillArgs(args);
    return term;
  }

  sum(field) {
    if (this._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 0, 1, 'sum', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SUM);
    const args = [this];
    if (field !== undefined) {
      args.push(new Term(this._r).expr(field)._wrap());
    }
    term._fillArgs(args);
    return term;
  }

  avg(field) {
    if (this._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 0, 1, 'avg', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.AVG);
    const args = [this];
    if (field !== undefined) {
      args.push(new Term(this._r).expr(field)._wrap());
    }
    term._fillArgs(args);
    return term;
  }

  min(field) {
    if (this._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 0, 1, 'min', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.MIN);
    const args = [this];
    if (field !== undefined) {
      if (helper.isPlainObject(field)) {
        term._fillArgs(args);
        term._query.push(new Term(this._r).expr(Term.translateOptions(field))._query);
      }
      else {
        args.push(new Term(this._r).expr(field)._wrap());
        term._fillArgs(args);
      }
    }
    else {
      term._fillArgs(args);
    }
    return term;
  }

  max(field) {
    if (this._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 0, 1, 'max', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.MAX);
    const args = [this];
    if (field !== undefined) {
      if (helper.isPlainObject(field)) {
        term._fillArgs(args);
        term._query.push(new Term(this._r).expr(Term.translateOptions(field))._query);
      }
      else {
        args.push(new Term(this._r).expr(field)._wrap());
        term._fillArgs(args);
      }
    }
    else {
      term._fillArgs(args);
    }
    return term;
  }

  fold(base, func, options) {
    if (this._fastArityRange(arguments.length, 2, 3) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 2, 3, 'range', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.FOLD);
    const args = [this, new Term(this._r).expr(base), new Term(this._r).expr(func)._wrap()];
    term._fillArgs(args);
    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'emit') && (key !== 'finalEmit')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`fold\`. Available options are emit <function>, finalEmit <function>`);
        }
      });
      term._query.push(new Term(this._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  row() {
    this._noPrefix(this, 'row');
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'r.row', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.IMPLICIT_VAR);
    return term;
  }

  pluck() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'pluck', this);

    const term = new Term(this._r);
    term._query.push(termTypes.PLUCK);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  without() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'without', this);

    const term = new Term(this._r);
    term._query.push(termTypes.WITHOUT);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  merge() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'merge', this);

    const term = new Term(this._r);
    term._query.push(termTypes.MERGE);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i])._wrap());
    }
    term._fillArgs(args);
    return term;
  }

  literal(obj) {
    this._noPrefix(this, 'literal');
    // The test for arity is performed in r.literal

    const term = new Term(this._r);
    term._query.push(termTypes.LITERAL);
    if (arguments.length > 0) {
      const args = [new Term(this._r).expr(obj)];
      term._fillArgs(args);
    }
    return term;
  }

  append(value) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'append', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.APPEND);
    const args = [this, new Term(this._r).expr(value)];
    term._fillArgs(args);
    return term;
  }

  prepend(value) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'prepend', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.PREPEND);
    const args = [this, new Term(this._r).expr(value)];
    term._fillArgs(args);
    return term;
  }

  difference(other) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'difference', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.DIFFERENCE);
    const args = [this, new Term(this._r).expr(other)];
    term._fillArgs(args);
    return term;
  }

  setInsert(other) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'setInsert', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SET_INSERT);
    const args = [this, new Term(this._r).expr(other)];
    term._fillArgs(args);
    return term;
  }

  setUnion(other) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'setUnion', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SET_UNION);
    const args = [this, new Term(this._r).expr(other)];
    term._fillArgs(args);
    return term;
  }

  setIntersection(other) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'setIntersection', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SET_INTERSECTION);
    const args = [this, new Term(this._r).expr(other)];
    term._fillArgs(args);
    return term;
  }

  setDifference(other) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'setDifference', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SET_DIFFERENCE);
    const args = [this, new Term(this._r).expr(other)];
    term._fillArgs(args);
    return term;
  }

  getField(field) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, '(...)', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.GET_FIELD);
    const args = [this, new Term(this._r).expr(field)];
    term._fillArgs(args);
    return term;
  }

  bracket(field) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, '(...)', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.BRACKET);
    const args = [this, new Term(this._r).expr(field)];
    term._fillArgs(args);
    return term;
  }

  hasFields() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'hasFields', this);

    const term = new Term(this._r);
    term._query.push(termTypes.HAS_FIELDS);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  insertAt(index, value) {
    if (this._fastArity(arguments.length, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 2, 'insertAt', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.INSERT_AT);
    const args = [this, new Term(this._r).expr(index), new Term(this._r).expr(value)];
    term._fillArgs(args);
    return term;
  }

  spliceAt(index, array) {
    if (this._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 1, 2, 'spliceAt', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SPLICE_AT);
    const args = [this, new Term(this._r).expr(index), new Term(this._r).expr(array)];
    term._fillArgs(args);
    return term;
  }

  deleteAt(start, end) {
    if (this._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 1, 2, 'deleteAt', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.DELETE_AT);
    const args = [this, new Term(this._r).expr(start)];
    if (end !== undefined) {
      args.push(new Term(this._r).expr(end));
    }
    term._fillArgs(args);
    return term;
  }

  changeAt(index, value) {
    if (this._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 1, 2, 'changeAt', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.CHANGE_AT);
    const args = [this];
    args.push(new Term(this._r).expr(index));
    args.push(new Term(this._r).expr(value));
    term._fillArgs(args);
    return term;
  }

  keys() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'keys', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.KEYS);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  values() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'keys', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.VALUES);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  object() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._noPrefix(this, 'object');
    this._arityRange(_args, 0, Infinity, 'object', this);

    const term = new Term(this._r);
    term._query.push(termTypes.OBJECT);
    const args = [];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  match(regex) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'match', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.MATCH);
    const args = [this, new Term(this._r).expr(regex)];
    term._fillArgs(args);
    return term;
  }

  upcase() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'upcase', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.UPCASE);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  downcase() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'upcase', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.DOWNCASE);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  add() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'add', this);

    const term = new Term(this._r);
    term._query.push(termTypes.ADD);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  sub() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'sub', this);

    const term = new Term(this._r);
    term._query.push(termTypes.SUB);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  mul() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'mul', this);

    const term = new Term(this._r);
    term._query.push(termTypes.MUL);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  div() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'div', this);

    const term = new Term(this._r);
    term._query.push(termTypes.DIV);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  mod(b) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'mod', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.MOD);
    const args = [this, new Term(this._r).expr(b)];
    term._fillArgs(args);
    return term;
  }

  and() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}

    const term = new Term(this._r);
    term._query.push(termTypes.AND);
    const args = [];
    if (!Array.isArray(this._query) || (this._query.length > 0)) {
      args.push(this);
    }
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  or() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}

    const term = new Term(this._r);
    term._query.push(termTypes.OR);
    const args = [];
    if (!Array.isArray(this._query) || (this._query.length > 0)) {
      args.push(this);
    }
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  eq() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'eq', this);

    const term = new Term(this._r);
    term._query.push(termTypes.EQ);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  ne() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'ne', this);

    const term = new Term(this._r);
    term._query.push(termTypes.NE);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  gt() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'gt', this);

    const term = new Term(this._r);
    term._query.push(termTypes.GT);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  ge() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'ge', this);

    const term = new Term(this._r);
    term._query.push(termTypes.GE);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  lt() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'lt', this);

    const term = new Term(this._r);
    term._query.push(termTypes.LT);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  le() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'le', this);

    const term = new Term(this._r);
    term._query.push(termTypes.LE);
    const args = [this];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  not() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'not', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.NOT);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  random() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    const self = this;
    self._noPrefix(this, 'random');
    self._arityRange(_args, 0, 3, 'random', self);

    const term = new Term(self._r);
    term._query.push(termTypes.RANDOM);

    const args = [];
    for(let i=0; i<_args.length-1; i++) {
      args.push(new Term(self._r).expr(_args[i]));
    }
    if (_args.length > 0) {
      if (helper.isPlainObject(_args[_args.length-1])) {
        helper.loopKeys(_args[_args.length-1], (obj, key) => {
          if (key !== 'float') {
            throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`random\``, self._query, 'Available option is float: <boolean>');
          }
        });
        term._fillArgs(args);
        term._query.push(new Term(self._r).expr(Term.translateOptions(_args[_args.length-1]))._query);
      }
      else {
        args.push(new Term(self._r).expr(_args[_args.length-1]));
        term._fillArgs(args);
      }
    }
    else {
      term._fillArgs(args);
    }
    return term;
  }

  floor() {
    if (this._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 0, 1, 'floor', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.FLOOR);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  ceil() {
    if (this._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 0, 1, 'ceil', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.CEIL);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  round() {
    if (this._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 0, 1, 'round', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.ROUND);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  now() {
    this._noPrefix(this, 'now');

    const term = new Term(this._r);
    term._query.push(termTypes.NOW);
    return term;
  }

  time() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._noPrefix(this, 'time');
    // Special check for arity
    let foundArgs = false;
    for(let i=0; i<_args.length; i++) {
      if ((_args[i] instanceof Term) && (_args[i]._query[0] === termTypes.ARGS)) {
        foundArgs = true;
        break;
      }
    }
    if (foundArgs === false) {
      if ((_args.length !== 4) && (_args.length !== 7)) {
        throw new Error.ReqlDriverError(`\`r.time\` called with ${_args.length} argument${(_args.length>1)?'s':''}`, null, '`r.time` takes 4 or 7 arguments');
      }
    }

    const term = new Term(this._r);
    term._query.push(termTypes.TIME);
    const args = [];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  epochTime(epochTime) {
    this._noPrefix(this, 'epochTime');

    const term = new Term(this._r);
    term._query.push(termTypes.EPOCH_TIME);
    const args = [new Term(this._r).expr(epochTime)];
    term._fillArgs(args);
    return term;
  }

  ISO8601(isoTime, options) {
    this._noPrefix(this, 'ISO8601');
    if (this._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 1, 2, 'ISO8601', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.ISO8601);
    const args = [new Term(this._r).expr(isoTime)._query];
    term._fillArgs(args);
    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if (key !== 'defaultTimezone') {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`ISO8601\`. Available options are primaryKey <string>, durability <string>, datancenter <string>`);
        }
      });
      term._query.push(new Term(this._r).expr(Term.translateOptions(options))._query);
    }

    return term;
  }

  inTimezone(timezone) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'inTimezone', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.IN_TIMEZONE);
    const args = [this, new Term(this._r).expr(timezone)];
    term._fillArgs(args);
    return term;
  }

  timezone() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'timezone', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.TIMEZONE);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  during(left, right, options) {
    if (this._fastArityRange(arguments.length, 2, 3) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 2, 3, 'during', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.DURING);
    const args = [];
    args.push(this);
    args.push(new Term(this._r).expr(left));
    args.push(new Term(this._r).expr(right));

    term._fillArgs(args);
    if (helper.isPlainObject(options)) {
      term._query.push(new Term(this._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  date() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'date', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.DATE);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  timeOfDay() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'timeOfDay', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.TIME_OF_DAY);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  year() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'year', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.YEAR);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  month() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'month', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.MONTH);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  day() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'day', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.DAY);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  dayOfYear() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'dayOfYear', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.DAY_OF_YEAR);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  dayOfWeek() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'dayOfWeek', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.DAY_OF_WEEK);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  hours() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'hours', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.HOURS);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  minutes() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'minutes', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.MINUTES);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  seconds() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'seconds', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.SECONDS);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  toISO8601() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'toISO8601', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.TO_ISO8601);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  toEpochTime() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'toEpochTime', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.TO_EPOCH_TIME);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  monday() {
    const term = new Term(this._r);
    term._query.push(termTypes.MONDAY);
    return term;
  }

  tuesday() {
    const term = new Term(this._r);
    term._query.push(termTypes.TUESDAY);
    return term;
  }

  wednesday() {
    const term = new Term(this._r);
    term._query.push(termTypes.WEDNESDAY);
    return term;
  }

  thursday() {
    const term = new Term(this._r);
    term._query.push(termTypes.THURSDAY);
    return term;
  }

  friday() {
    const term = new Term(this._r);
    term._query.push(termTypes.FRIDAY);
    return term;
  }

  saturday() {
    const term = new Term(this._r);
    term._query.push(termTypes.SATURDAY);
    return term;
  }

  sunday() {
    const term = new Term(this._r);
    term._query.push(termTypes.SUNDAY);
    return term;
  }

  january() {
    const term = new Term(this._r);
    term._query.push(termTypes.JANUARY);
    return term;
  }

  february() {
    const term = new Term(this._r);
    term._query.push(termTypes.FEBRUARY);
    return term;
  }

  march() {
    const term = new Term(this._r);
    term._query.push(termTypes.MARCH);
    return term;
  }

  april() {
    const term = new Term(this._r);
    term._query.push(termTypes.APRIL);
    return term;
  }

  may() {
    const term = new Term(this._r);
    term._query.push(termTypes.MAY);
    return term;
  }

  june() {
    const term = new Term(this._r);
    term._query.push(termTypes.JUNE);
    return term;
  }

  july() {
    const term = new Term(this._r);
    term._query.push(termTypes.JULY);
    return term;
  }

  august() {
    const term = new Term(this._r);
    term._query.push(termTypes.AUGUST);
    return term;
  }

  september() {
    const term = new Term(this._r);
    term._query.push(termTypes.SEPTEMBER);
    return term;
  }

  october() {
    const term = new Term(this._r);
    term._query.push(termTypes.OCTOBER);
    return term;
  }

  november() {
    const term = new Term(this._r);
    term._query.push(termTypes.NOVEMBER);
    return term;
  }

  december() {
    const term = new Term(this._r);
    term._query.push(termTypes.DECEMBER);
    return term;
  }

  args() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._noPrefix(this, 'args');

    const term = new Term(this._r);
    term._query.push(termTypes.ARGS);
    const args = [];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  do() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 1, Infinity, 'do', this);

    const term = new Term(this._r);
    term._query.push(termTypes.FUNCALL);
    const args = [new Term(this._r).expr(_args[_args.length-1])._wrap()._query];
    args.push(this);
    for(let i=0; i<_args.length-1; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  branch() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    this._arityRange(_args, 2, Infinity, '', this);

    const term = new Term(this._r);
    term._query.push(termTypes.BRANCH);
    const args = [];
    args.push(this);
    for(let i=0; i<_len; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  forEach(func) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'forEach', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.FOR_EACH);
    const args = [this, new Term(this._r).expr(func)._wrap()];
    term._fillArgs(args);
    return term;
  }

  default(expression) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'default', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.DEFAULT);
    const args = [this, new Term(this._r).expr(expression)];
    term._fillArgs(args);
    return term;
  }

  expr(expression, nestingLevel=100) {
    const self = this;
    self._noPrefix(self, 'expr');
    if (self._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 1, 2, 'expr', self);
    }

    // undefined will be caught in the last else
    // let ar;
    // let obj;

    if (expression === undefined) {
      const error = 'Cannot convert `undefined` with r.expr()';
      return new Term(self._r, expression, error);
    }

    let _nestingLevel = nestingLevel;
    if (_nestingLevel == null) {
      _nestingLevel = self._r.nestingLevel;
    }
    //if (nestingLevel == null) nestingLevel = self._r.nestingLevel;
    if (_nestingLevel < 0) throw new Error.ReqlDriverError('Nesting depth limit exceeded.\nYou probably have a circular reference somewhere');

    if (expression instanceof Term) {
      return expression;
    }
    else if (expression instanceof Function) {
      return new Term.Func(self._r, expression);
    }
    else if (expression instanceof Date) {
      return new Term(self._r).ISO8601(expression.toISOString());
    }
    else if (Array.isArray(expression)) {
      const term = new Term(self._r);
      term._query.push(termTypes.MAKE_ARRAY);

      const args = [];
      for(let i=0; i<expression.length; i++) {
        args.push(new Term(self._r).expr(expression[i], _nestingLevel-1));
      }
      term._fillArgs(args);
      return term;
    }
    else if (expression instanceof Buffer) {
      return self._r.binary(expression);
    }
    else if (helper.isPlainObject(expression)) {
      const term = new Term(self._r);
      const optArgs = {};
      let foundError = false;
      helper.loopKeys(expression, (expr, key) => {
        if (expr[key] !== undefined) {
          const optArg = new Term(self._r).expr(expr[key], _nestingLevel-1);
          if (optArg instanceof Term && !foundError && optArg._error != null) {
            foundError = true;
            term._error = optArg._error;
            term._frames = [key].concat(optArg._frames);
          }
          optArgs[key] = optArg._query;
        }
      });
      term._query = optArgs;
      return term;
    }
    // else { // Primitive
    if (expression === null) {
      return new Term(self._r, null, expression);
    }
    else if (typeof expression === 'string') {
      return new Term(self._r, expression);
    }
    else if (typeof expression === 'number') {
      if (expression !== expression) {
        const err = 'Cannot convert `NaN` to JSON';
        return new Term(self._r, expression, err);
      }
      else if (!isFinite(expression)) {
        const err = 'Cannot convert `Infinity` to JSON';
        return new Term(self._r, expression, err);
      }
      return new Term(self._r, expression);
    }
    else if (typeof expression === 'boolean') {
      return new Term(self._r, expression);
    }
      //TODO
    self._error = new Error.ReqlDriverError(`Cannot convert \`${expression}\` to datum.`);
      // }
    // }
    return self;
  }

  binary(bin) {
    this._noPrefix(this, 'binary');
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'binary', this);
    }

    let term;
    if (bin instanceof Buffer) {
      // We could use BINARY, and coerce `bin` to an ASCII string, but that
      // will break if there is a null char
      term = new Term(this._r, {
        $reql_type$: 'BINARY',
        data: bin.toString('base64'),
      });
    }
    else {
      term = new Term(this._r);
      term._query.push(termTypes.BINARY);
      const args = [new Term(this._r).expr(bin)];
      term._fillArgs(args);
    }
    return term;
  }

  js(arg, options) {
    this._noPrefix(this, 'js');
    if (this._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 1, 2, 'js', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.JAVASCRIPT);
    const args = [new Term(this._r).expr(arg)];
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      term._query.push(new Term(this._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  coerceTo(type) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'coerceTo', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.COERCE_TO);
    const args = [this, new Term(this._r).expr(type)];
    term._fillArgs(args);
    return term;
  }

  typeOf() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'typeOf', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.TYPE_OF);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  info() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'info', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.INFO);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  json(json) {
    this._noPrefix(this, 'json');
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'info', this);
    }
    /*
    if ((/\\u0000/.test(json)) || (/\0/.test(json))) {
      this._error = new Error.ReqlDriverError('The null character is currently not supported by RethinkDB');
    }
    */
    const term = new Term(this._r);
    term._query.push(termTypes.JSON);

    const args = [new Term(this._r).expr(json)];
    term._fillArgs(args);
    return term;
  }

  http(url, options) {
    this._noPrefix(this, 'http');
    if (this._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 1, 2, 'http', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.HTTP);
    const args = [new Term(this._r).expr(url)];
    term._fillArgs(args);
    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'timeout')
          && (key !== 'attempts')
          && (key !== 'redirects')
          && (key !== 'verify')
          && (key !== 'resultFormat')
          && (key !== 'method')
          && (key !== 'auth')
          && (key !== 'params')
          && (key !== 'header')
          && (key !== 'data')
          && (key !== 'page')
          && (key !== 'pageLimit')
          && (key !== '')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`http\`. Available options are attempts <number>, redirects <number>, verify <boolean>, resultFormat: <string>, method: <string>, auth: <object>, params: <object>, header: <string>, data: <string>, page: <string/function>, pageLimit: <number>`);
        }
      });

      term._query.push(new Term(this._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  uuid(str) {
    this._noPrefix(this, 'uuid');

    const term = new Term(this._r);
    term._query.push(termTypes.UUID);

    if (str !== undefined) {
      const args = [new Term(this._r).expr(str)];
      term._fillArgs(args);
    }
    return term;
  }

  circle(center, radius, options) {
    const self = this;

    // Arity check is done by r.circle
    self._noPrefix(self, 'circle');
    const term = new Term(self._r);
    term._query.push(termTypes.CIRCLE);
    const args = [new Term(self._r).expr(center), new Term(self._r).expr(radius)];
    term._fillArgs(args);

    if (helper.isPlainObject(options)) {
      // There is no need to translate here
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'numVertices') && (key !== 'geoSystem') && (key !== 'unit') && (key !== 'fill')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`circle\``, self._query, 'Available options are numVertices <number>, geoSsystem <string>, unit <string> and fill <bool>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }

    return term;
  }

  distance(geometry, options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 1, 2, 'distance', self);
    }
    const term = new Term(self._r);
    term._query.push(termTypes.DISTANCE);
    const args = [self, new Term(self._r).expr(geometry)];
    term._fillArgs(args);
    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'geoSystem') && (key !== 'unit')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`distance\``, self._query, 'Available options are geoSystem <string>, unit <string>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  fill() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'fill', this);
    }
    const term = new Term(this._r);
    term._query.push(termTypes.FILL);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  geojson(geometry) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'geojson', this);
    }
    this._noPrefix(this, 'geojson');
    const term = new Term(this._r);
    term._query.push(termTypes.GEOJSON);
    const args = [new Term(this._r).expr(geometry)];
    term._fillArgs(args);
    return term;
  }

  toGeojson() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'toGeojson', this);
    }
    const term = new Term(this._r);
    term._query.push(termTypes.TO_GEOJSON);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  getIntersecting(geometry, options) {
    if (this._fastArity(arguments.length, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 2, 'getIntersecting', this);
    }
    const term = new Term(this._r);
    term._query.push(termTypes.GET_INTERSECTING);
    const args = [this, new Term(this._r).expr(geometry)];
    term._fillArgs(args);
    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if (key !== 'index') {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`distance\``, self._query, 'Available options are index <string>');
        }
      });
      term._query.push(new Term(this._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  getNearest(geometry, options) {
    const self = this;
    if (self._fastArity(arguments.length, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arity(_args, 2, 'getNearest', self);
    }
    const term = new Term(self._r);
    term._query.push(termTypes.GET_NEAREST);
    const args = [self, new Term(self._r).expr(geometry)];
    term._fillArgs(args);
    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'index') && (key !== 'maxResults') && (key !== 'maxDist') && (key !== 'unit') && (key !== 'geoSystem')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`getNearest\``, self._query, 'Available options are index <string>, maxResults <number>, maxDist <number>, unit <string>, geoSystem <string>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }
    return term;
  }

  includes(geometry) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'includes', this);
    }
    const term = new Term(this._r);
    term._query.push(termTypes.INCLUDES);
    const args = [this, new Term(this._r).expr(geometry)];
    term._fillArgs(args);
    return term;
  }

  intersects(geometry) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'intersects', this);
    }
    const term = new Term(this._r);
    term._query.push(termTypes.INTERSECTS);
    const args = [this, new Term(this._r).expr(geometry)];
    term._fillArgs(args);
    return term;
  }

  line() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    // Arity check is done by r.line
    this._noPrefix(this, 'line');

    const term = new Term(this._r);
    term._query.push(termTypes.LINE);

    const args = [];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);
    return term;
  }

  point(longitude, latitude) {
    // Arity check is done by r.point
    this._noPrefix(this, 'point');

    const term = new Term(this._r);
    term._query.push(termTypes.POINT);
    const args = [new Term(this._r).expr(longitude), new Term(this._r).expr(latitude)];
    term._fillArgs(args);
    return term;
  }

  polygon() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    // Arity check is done by r.polygon
    this._noPrefix(this, 'polygon');

    const term = new Term(this._r);
    term._query.push(termTypes.POLYGON);

    const args = [];
    for(let i=0; i<_args.length; i++) {
      args.push(new Term(this._r).expr(_args[i]));
    }
    term._fillArgs(args);

    return term;
  }

  polygonSub(geometry) {
    if (this._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 1, 'polygonSub', this);
    }
    const term = new Term(this._r);
    term._query.push(termTypes.POLYGON_SUB);
    const args = [this, new Term(this._r).expr(geometry)];
    term._fillArgs(args);
    return term;
  }

  range(start, end) {
    this._noPrefix(this, 'range');
    if (this._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arityRange(_args, 1, 2, 'r.range', this);
    }
    const term = new Term(this._r);
    term._query.push(termTypes.RANGE);
    const args = [];
    args.push(new Term(this._r).expr(start));
    if (end !== undefined) {
      args.push(new Term(this._r).expr(end));
    }
    term._fillArgs(args);
    return term;
  }

  toJsonString() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'toJSON', this);
    }
    const term = new Term(this._r);
    term._query.push(termTypes.TO_JSON_STRING);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  config() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'config', this);
    }
    const term = new Term(this._r);
    term._query.push(termTypes.CONFIG);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  status() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'status', this);
    }
    const term = new Term(this._r);
    term._query.push(termTypes.STATUS);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  wait(options) {
    const self = this;
    if (self._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arityRange(_args, 0, 1, 'wait', self);
    }
    const term = new Term(self._r);
    term._query.push(termTypes.WAIT);
    const args = [self];
    term._fillArgs(args);
    if (helper.isPlainObject(options)) {
      helper.loopKeys(options, (obj, key) => {
        if ((key !== 'waitFor') && (key !== 'timeout')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`wait\``, self._query, 'Available options are waitFor: <string>, timeout: <number>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(options))._query);
    }

    return term;
  }

  reconfigure(config) {
    const self = this;
    if (self._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      self._arity(_args, 1, 'reconfigure', self);
    }
    const term = new Term(self._r);
    term._query.push(termTypes.RECONFIGURE);

    const args = [this];
    term._fillArgs(args);
    if (helper.isPlainObject(config)) {
      helper.loopKeys(config, (obj, key) => {
        if ((key !== 'shards') && (key !== 'replicas') &&
          (key !== 'dryRun') && (key !== 'primaryReplicaTag') &&
          (key !== 'nonvotingReplicaTags') && (key !== 'emergencyRepair')) {
          throw new Error.ReqlDriverError(`Unrecognized option \`${key}\` in \`reconfigure\``, self._query, 'Available options are shards: <number>, replicas: <number>, primaryReplicaTag: <object>, dryRun <boolean>, emergencyRepair: <string>, nonvotingReplicaTags: <array<string>>');
        }
      });
      term._query.push(new Term(self._r).expr(Term.translateOptions(config))._query);
    }
    else {
      throw new Error.ReqlDriverError('First argument of `reconfigure` must be an object');
    }
    return term;
  }

  rebalance() {
    if (this._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 0, 'rebalance', this);
    }
    const term = new Term(this._r);
    term._query.push(termTypes.REBALANCE);
    const args = [this];
    term._fillArgs(args);
    return term;
  }

  grant(name, access) {
    if (this._fastArity(arguments.length, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      this._arity(_args, 2, 'grant', this);
    }

    const term = new Term(this._r);
    term._query.push(termTypes.GRANT);
    const args = [this, new Term(this._r).expr(name), new Term(this._r).expr(access)];
    term._fillArgs(args);
    return term;
  }

  then(resolve, reject) {
    return this.run().then(resolve, reject);
  }

  error(reject) {
    return this.run().error(reject);
  }

  catch(reject) {
    return this.run().catch(reject);
  }

  finally(handler) {
    return this.run().finally(handler);
  }

  delay(msecs) {
    return this.run().delay(msecs);
  }

  toString() {
    return Error.generateBacktrace(this._query, 0, null, [],
      { indent: 0, extra: 0 }).str;
  }

  _wrap() {
    if (helper.hasImplicit(this._query)) {
      if (this._query[0] === termTypes.ARGS) {
        throw new Error.ReqlDriverError('Implicit variable `r.row` cannot be used inside `r.args`');
      }
      //Must pass at least one variable to the function or it won't accept r.row
      return new Term(this._r).expr(doc => doc);
    }
    return this;
  }

  _fillArgs(args) {
    let foundError = false;
    const internalArgs = [];
    for(let i=0; i<args.length; i++) {
      if (args[i] instanceof Term) {
        internalArgs.push(args[i]._query);
        if (!foundError && (args[i]._error != null)) {
          this._error = args[i]._error;
          this._frames = args[i]._frames;
          this._frames.unshift(i);
          foundError = true;
        }
      } else {
        internalArgs.push(args[i]);
      }
    }
    this._query.push(internalArgs);
    return this;
  }

  _setNestingLevel(nestingLevel) {
    Term.prototype._nestingLevel = nestingLevel;
  }

  _setArrayLimit(arrayLimit) {
    Term.prototype._arrayLimit = arrayLimit;
  }

  _noPrefix(term, method) {
    if ((!Array.isArray(term._query)) || (term._query.length > 0)) {
      throw new Error.ReqlDriverError(`\`${method}\` is not defined`, term._query);
    }
  }

  _arityRange(args, min, max, method, term) {
    let foundArgs = false;
    if (args.length < min) {
      for(let i=0; i<args.length; i++) {
        if ((args[i] instanceof Term) && (args[i]._query[0] === termTypes.ARGS)) {
          foundArgs = true;
          break;
        }
      }
      if (foundArgs === false) {
        throw new Error.ReqlDriverError(`\`${method}\` takes at least ${min} argument${(min>1)?'s':''}, ${args.length} provided`, term._query);
      }
    }
    else if (args.length > max) {
      for(let i=0; i<args.length; i++) {
        if ((args[i] instanceof Term) && (args[i]._query[0] === termTypes.ARGS)) {
          foundArgs = true;
          break;
        }
      }
      if (foundArgs === false) {
        throw new Error.ReqlDriverError(`\`${method}\` takes at most ${max} argument${(max>1)?'s':''}, ${args.length} provided`, term._query);
      }
    }
  }

  _arity(args, num, method, term) {
    let foundArgs = false;
    for(let i=0; i<args.length; i++) {
      if ((args[i] instanceof Term) && (args[i]._query[0] === termTypes.ARGS)) {
        foundArgs = true;
        break;
      }
    }
    if (foundArgs === false) {
      throw new Error.ReqlDriverError(`\`${method}\` takes ${num} argument${(num>1)?'s':''}, ${args.length} provided`, term._query);
    }
  }

  _fastArity(len, num) {
    return (len === num);
  }

  _fastArityRange(len, min, max) {
    return ((len >= min) && (len <= max));
  }
}

Term.prototype.indexesOf = Term.prototype.offsetsOf;

Term.prototype.toJSON = Term.prototype.toJsonString;

Term.prototype._translateArgs = {
  returnChanges: 'return_changes',
  includeInitial: 'include_initial',
  primaryKey: 'primary_key',
  readMode: 'read_mode',
  nonAtomic: 'non_atomic',
  leftBound: 'left_bound',
  rightBound: 'right_bound',
  defaultTimezone: 'default_timezone',
  noReply: 'noreply',
  resultFormat: 'result_format',
  pageLimit: 'page_limit',
  arrayLimit: 'array_limit',
  numVertices: 'num_vertices',
  geoSystem: 'geo_system',
  maxResults: 'max_results',
  maxDist: 'max_dist',
  dryRun: 'dry_run',
  waitFor: 'wait_for',
  includeStates: 'include_states',
  primaryReplicaTag: 'primary_replica_tag',
  emergencyRepair: 'emergency_repair',
  minBatchRows: 'min_batch_rows',
  maxBatchRows: 'max_batch_rows',
  maxBatchBytes: 'max_batch_bytes',
  maxBatchSeconds: 'max_batch_seconds',
  firstBatchScaledownFactor: 'first_batch_scaledown_factor',
  includeOffsets: 'include_offsets',
  includeTypes: 'include_types',
  finalEmit: 'final_emit',
};


Term.Func.prototype.nextVarId = 1;


export default Term;
