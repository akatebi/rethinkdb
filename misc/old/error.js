import helper from './helper.js';
const INDENT = 4;
const LIMIT = 80;
const IS_OPERATIONAL = 'isOperational';

import protodef from './protodef.js';
// const responseTypes = protodef.Response.ResponseType;
const protoErrorType = protodef.Response.ErrorType;
const termTypes = protodef.Term.TermType;
// const datumTypes = protodef.Datum.DatumType;
// const frameTypes = protodef.Frame.FrameType;

const space = (n) => new Array(n+1).join(' ');

/* eslint no-param-reassign:1 */

class ReqlDriverError {

  static carify(result, str, underline) {
    if (underline === true) {
      result.str += str;
      result.car += str.replace(/[^\n]/g, '^');
    }
    else {
      result.str += str;
      result.car += str.replace(/[^\n]/g, ' ');
    }
  }

  static makeOptargs(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    if (helper.isPlainObject(term[2])) {
      const underline = false;
      if (Array.isArray(term[1]) && (term[1].length > 1)) {
        ReqlDriverError.carify(result, ', ', underline);
      }
      else if (Array.isArray(term[1]) && (term[1].length > 0) && (noPrefixOptargs[term[0]])) {
        ReqlDriverError.carify(result, ', ', underline);
      }
      backtrace = specialType[termTypes.DATUM](term[2], index, term[2], frames, options, true);
      result.str += backtrace.str;
      result.car += backtrace.car;
      if (underline) result.car = result.str.replace(/./g, '^');
    }
    return result;
  }

  static generateNormalBacktrace(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    let currentFrame;
    const underline = Array.isArray(frames) && (frames.length === 0);
    if (Array.isArray(frames)) currentFrame = frames.shift();
    if ((currentFrame != null) && (currentFrame === 0)) {
      backtrace = ReqlDriverError.generateBacktrace(term[1][0], 0, term, frames, options);
    }
    else {
      backtrace = ReqlDriverError.generateBacktrace(term[1][0], 0, term, null, options);
    }
    result.str = backtrace.str;
    result.car = backtrace.car;
    const lines = backtrace.str.split('\n');
    const line = lines[lines.length-1];
    let pos = line.match(/[^\s]/);
    pos = (pos) ? pos.index : 0;
    if (line.length-pos > LIMIT) {
      if (options.extra === 0) options.extra += INDENT;
      ReqlDriverError.carify(result, `\n${space(options.indent+options.extra)}`, underline);
    }
    ReqlDriverError.carify(result, `.${typeToString[term[0]]}(`, underline);
    options.indent += options.extra;
    const extraToRemove = options.extra;
    options.extra = 0;
    for(let i=1; i<term[1].length; i++) {
      if (i !== 1) {
        ReqlDriverError.carify(result, ', ', underline);
      }
      if ((currentFrame != null) && (currentFrame === i)) {
        backtrace = ReqlDriverError.generateBacktrace(term[1][i], i, term, frames, options);
      }
      else {
        backtrace = ReqlDriverError.generateBacktrace(term[1][i], i, term, null, options);
      }
      result.str += backtrace.str;
      result.car += backtrace.car;
    }
    backtrace = ReqlDriverError.makeOptargs(term, i, term, frames, options, currentFrame);
    result.str += backtrace.str;
    result.car += backtrace.car;
    options.indent -= extraToRemove;
    options.extra = extraToRemove;
    ReqlDriverError.carify(result, ')', underline);
    if (underline) result.car = result.str.replace(/./g, '^');
    return result;
  }

  static generateWithoutPrefixBacktrace(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    let currentFrame;
    const underline = Array.isArray(frames) && (frames.length === 0);
    if (Array.isArray(frames)) currentFrame = frames.shift();
    if (constants[term[0]]) {
      ReqlDriverError.carify(result, `r.${typeToString[term[0]]}`, underline);
      return result;
    }
    ReqlDriverError.carify(result, `r.${typeToString[term[0]]}(`, underline);

    if (Array.isArray(term[1])) {
      for(let i=0; i<term[1].length; i++) {
        if (i !== 0) ReqlDriverError.carify(result, ', ', underline);

        if ((currentFrame != null) && (currentFrame === i)) {
          backtrace = ReqlDriverError.generateBacktrace(term[1][i], i, term, frames, options);
        }
        else {
          backtrace = ReqlDriverError.generateBacktrace(term[1][i], i, term, null, options);
        }
        result.str += backtrace.str;
        result.car += backtrace.car;
      }
    }

    backtrace = ReqlDriverError.makeOptargs(term, i, term, frames, options, currentFrame);
    result.str += backtrace.str;
    result.car += backtrace.car;

    ReqlDriverError.carify(result, ')', underline);

    if (underline) result.car = result.str.replace(/./g, '^');

    return result;
  }

  static generateBacktrace(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    // let currentFrame;
    // let underline;

    // frames = null -> do not underline
    // frames = [] -> underline

    if (Array.isArray(term)) {
      if (term.length === 0) {
        const underline = Array.isArray(frames) && (frames.length === 0);
        ReqlDriverError.carify(result, 'undefined', underline);
      }
      else if (specialType[term[0]]) {
        backtrace = specialType[term[0]](term, index, father, frames, options);
        result.str = backtrace.str;
        result.car = backtrace.car;
      }
      else if (nonPrefix[term[0]]) {
        backtrace = ReqlDriverError.generateWithoutPrefixBacktrace(term, index, father, frames, options);
        result.str = backtrace.str;
        result.car = backtrace.car;
      }
      else { // normal type -- this.<method>( this.args... )
        backtrace = ReqlDriverError.generateNormalBacktrace(term, index, father, frames, options);
        result.str = backtrace.str;
        result.car = backtrace.car;
      }
    }
    else if (term !== undefined) {
      backtrace = specialType[termTypes.DATUM](term, index, father, frames, options);

      result.str = backtrace.str;
      result.car = backtrace.car;
    }
    else {
      //throw new Error('The driver should never enter this condition. Please report the query to the developers -- End 2')
    }
    return result;
  }

  constructor(message, query, secondMessage) {
    Error.captureStackTrace(this, ReqlDriverError);
    this.message = message;
    if ((Array.isArray(query) && (query.length > 0)) || (!Array.isArray(query) && query != null)) {
      if ((this.message.length > 0) && (this.message[this.message.length-1] === '.')) {
        this.message = this.message.slice(0, this.message.length-1);
      }

      this.message += ' after:\n';

      const backtrace = ReqlDriverError.generateBacktrace(query, 0, null, [], { indent: 0, extra: 0 });

      this.message += backtrace.str;
    }
    else {
      if (this.message[this.message.length-1] !== '?') this.message += '.';
    }
    if (secondMessage) this.message += `\n${secondMessage}`;
  }

  setOperational() {
    this[IS_OPERATIONAL] = true;
    return this;
  }
}

ReqlDriverError.prototype = new Error();
ReqlDriverError.prototype.name = 'ReqlDriverError';
export { ReqlDriverError };


function ReqlServerError(message, query) {
  Error.captureStackTrace(this, ReqlServerError);
  this.message = message;

  if ((Array.isArray(query) && (query.length > 0)) || (!Array.isArray(query) && query != null)) {
    if ((this.message.length > 0) && (this.message[this.message.length-1] === '.')) {
      this.message = this.message.slice(0, this.message.length-1);
    }

    this.message += ' for:\n';

    const backtrace = ReqlDriverError.generateBacktrace(query, 0, null, [], { indent: 0, extra: 0 });

    this.message += backtrace.str;
  }
  else {
    if (this.message[this.message.length-1] !== '?') this.message += '.';
  }
}
ReqlServerError.prototype = new Error();
ReqlServerError.prototype.name = 'ReqlServerError';
ReqlServerError.prototype[IS_OPERATIONAL] = true;

export { ReqlServerError };

class ReqlRuntimeError {
  constructor(message, query, frames) {
    Error.captureStackTrace(this, ReqlRuntimeError);
    this.message = message;

    if ((query != null) && (frames)) {
      if ((this.message.length > 0) && (this.message[this.message.length-1] === '.')) {
        this.message = this.message.slice(0, this.message.length-1);
      }
      this.message += ' in:\n';

      frames = frames.b;
      if (frames) this.frames = frames.slice(0);
      //this.frames = JSON.stringify(frames, null, 2);

      const backtrace = ReqlDriverError.generateBacktrace(query, 0, null, frames, { indent: 0, extra: 0 });

      const queryLines = backtrace.str.split('\n');
      const carrotLines = backtrace.car.split('\n');

      for(let i=0; i<queryLines.length; i++) {
        this.message += `${queryLines[i]}\n`;
        if (carrotLines[i].match(/\^/)) {
          const pos = queryLines[i].match(/[^\s]/);
          if ((pos) && (pos.index)) {
            this.message += `${space(pos.index)+carrotLines[i].slice(pos.index)}\n`;
          }
          else {
            this.message += `${carrotLines[i]}\n`;
          }
        }
      }
    }
    //this.query = JSON.stringify(query, null, 2);
  }

  setName(type) {
    switch(type) {
      case protoErrorType.INTERNAL:
        this.name = 'ReqlInternalError';
        break;
      case protoErrorType.RESOURCE_LIMIT:
        this.name = 'ReqlResourceError';
        break;
      case protoErrorType.QUERY_LOGIC:
        this.name = 'ReqlLogicError';
        break;
      case protoErrorType.OP_FAILED:
        this.name = 'ReqlOpFailedError';
        break;
      case protoErrorType.OP_INDETERMINATE:
        this.name = 'ReqlOpIndeterminateError';
        break;
      case protoErrorType.USER:
        this.name = 'ReqlUserError';
        break;
      default: // Do nothing
    }
  }
}

ReqlRuntimeError.prototype = new Error();
ReqlRuntimeError.prototype.name = 'ReqlRuntimeError';
ReqlRuntimeError.prototype[IS_OPERATIONAL] = true;

export { ReqlRuntimeError };


function ReqlCompileError(message, query, frames) {
  Error.captureStackTrace(this, ReqlCompileError);
  this.message = message;

  if ((query != null) && (frames)) {
    if ((this.message.length > 0) && (this.message[this.message.length-1] === '.')) {
      this.message = this.message.slice(0, this.message.length-1);
    }

    this.message += ' in:\n';

    frames = frames.b;
    if (frames) this.frames = frames.slice(0);
    //this.frames = JSON.stringify(frames, null, 2);

    const backtrace = ReqlDriverError.generateBacktrace(query, 0, null, frames, { indent: 0, extra: 0 });

    const queryLines = backtrace.str.split('\n');
    const carrotLines = backtrace.car.split('\n');

    for(let i=0; i<queryLines.length; i++) {
      this.message += `${queryLines[i]}\n`;
      if (carrotLines[i].match(/\^/)) {
        const pos = queryLines[i].match(/[^\s]/);
        if ((pos) && (pos.index)) {
          this.message += `${space(pos.index)+carrotLines[i].slice(pos.index)}\n`;
        }
        else {
          this.message += `${carrotLines[i]}\n`;
        }
      }
    }
  }
}
ReqlCompileError.prototype = new Error();
ReqlCompileError.prototype.name = 'ReqlCompileError';
ReqlCompileError.prototype[IS_OPERATIONAL] = true;

export { ReqlCompileError };


function ReqlClientError(message) {
  Error.captureStackTrace(this, ReqlClientError);
  this.message = message;
}
ReqlClientError.prototype = new Error();
ReqlClientError.prototype.name = 'ReqlClientError';
ReqlClientError.prototype[IS_OPERATIONAL] = true;

export { ReqlClientError };

const _constants = {
  MONDAY: true,
  TUESDAY: true,
  WEDNESDAY: true,
  THURSDAY: true,
  FRIDAY: true,
  SATURDAY: true,
  SUNDAY: true,
  JANUARY: true,
  FEBRUARY: true,
  MARCH: true,
  APRIL: true,
  MAY: true,
  JUNE: true,
  JULY: true,
  AUGUST: true,
  SEPTEMBER: true,
  OCTOBER: true,
  NOVEMBER: true,
  DECEMBER: true,
  MINVAL: true,
  MAXVAL: true,
};
const constants = {};
Object.keys(_constants).forEach(key => (constants[termTypes[key]] = true));


const _nonPrefix = {
  DB: true,
  DB_CREATE: true,
  DB_LIST: true,
  DB_DROP: true,
  JS: true,
  NOW: true,
  TIME: true,
  EPOCH_TIME: true,
  ISO8601: true,
  BRANCH: true,
  JAVASCRIPT: true,
  ERROR: true,
  MAKE_ARRAY: true,
  JSON: true,
  ARGS: true,
  HTTP: true,
  RANDOM: true,
  BINARY: true,
  OBJECT: true,
  CIRCLE: true,
  GEOJSON: true,
  POINT: true,
  LINE: true,
  POLYGON: true,
  UUID: true,
  DESC: true,
  ASC: true,
  RANGE: true,
  LITERAL: 'true',
};
const nonPrefix = {};
Object.keys(_nonPrefix).forEach(key => (nonPrefix[termTypes[key]] = true));
// Constants are also in nonPrefix
Object.keys(_constants).forEach(key => (nonPrefix[termTypes[key]] = true));

const _typeToString = {
  DB: 'db',
  DB_CREATE: 'dbCreate',
  DB_LIST: 'dbList',
  DB_DROP: 'dbDrop',
  TABLE_CREATE: 'tableCreate',
  TABLE_LIST: 'tableList',
  TABLE_DROP: 'tableDrop',
  TABLE: 'table',
  INDEX_CREATE: 'indexCreate',
  INDEX_DROP: 'indexDrop',
  INDEX_LIST: 'indexList',
  INDEX_WAIT: 'indexWait',
  INDEX_STATUS: 'indexStatus',
  INSERT: 'insert',
  UPDATE: 'update',
  REPLACE: 'replace',
  DELETE: 'delete',
  SYNC: 'sync',
  GET: 'get',
  GET_ALL: 'getAll',
  BETWEEN: 'between',
  FILTER: 'filter',
  INNER_JOIN: 'innerJoin',
  OUTER_JOIN: 'outerJoin',
  EQ_JOIN: 'eqJoin',
  ZIP: 'zip',
  MAP: 'map',
  WITH_FIELDS: 'withFields',
  CONCAT_MAP: 'concatMap',
  ORDER_BY: 'orderBy',
  DESC: 'desc',
  ASC: 'asc',
  SKIP: 'skip',
  LIMIT: 'limit',
  SLICE: 'slice',
  NTH: 'nth',
  OFFSETS_OF: 'offsetsOf',
  IS_EMPTY: 'isEmpty',
  UNION: 'union',
  SAMPLE: 'sample',
  REDUCE: 'reduce',
  COUNT: 'count',
  SUM: 'sum',
  AVG: 'avg',
  MIN: 'min',
  MAX: 'max',
  FOLD: 'fold',
  OBJECT: 'object',
  DISTINCT: 'distinct',
  GROUP: 'group',
  UNGROUP: 'ungroup',
  CONTAINS: 'contains',
  IMPLICIT_VAR: 'row',
  PLUCK: 'pluck',
  WITHOUT: 'without',
  MERGE: 'merge',
  APPEND: 'append',
  PREPEND: 'prepend',
  DIFFERENCE: 'difference',
  SET_INSERT: 'setInsert',
  SET_UNION: 'setUnion',
  SET_INTERSECTION: 'setIntersection',
  SET_DIFFERENCE: 'setDifference',
  HAS_FIELDS: 'hasFields',
  INSERT_AT: 'insertAt',
  SPLICE_AT: 'spliceAt',
  DELETE_AT: 'deleteAt',
  CHANGE_AT: 'changeAt',
  KEYS: 'keys',
  VALUES: 'values',
  MATCH: 'match',
  UPCASE: 'upcase',
  DOWNCASE: 'downcase',
  ADD: 'add',
  SUB: 'sub',
  MUL: 'mul',
  DIV: 'div',
  MOD: 'mod',
  AND: 'and',
  OR: 'or',
  EQ: 'eq',
  NE: 'ne',
  GT: 'gt',
  GE: 'ge',
  LT: 'lt',
  LE: 'le',
  NOT: 'not',
  FLOOR: 'floor',
  CEIL: 'ceil',
  ROUND: 'round',
  NOW: 'now',
  TIME: 'time',
  EPOCH_TIME: 'epochTime',
  ISO8601: 'ISO8601',
  IN_TIMEZONE: 'inTimezone',
  TIMEZONE: 'timezone',
  DURING: 'during',
  DATE: 'date',
  TIME_OF_DAY: 'timeOfDay',
  YEAR: 'year',
  MONTH: 'month',
  DAY: 'day',
  DAY_OF_WEEK: 'dayOfWeek',
  DAY_OF_YEAR: 'dayOfYear',
  HOURS: 'hours',
  MINUTES: 'minutes',
  SECONDS: 'seconds',
  TO_ISO8601: 'toISO8601',
  TO_EPOCH_TIME: 'toEpochTime',
  FUNCALL: 'do',
  BRANCH: 'branch',
  FOR_EACH: 'forEach',
  ERROR: 'error',
  DEFAULT: 'default',
  JAVASCRIPT: 'js',
  COERCE_TO: 'coerceTo',
  TYPE_OF: 'typeOf',
  INFO: 'info',
  JSON: 'json',
  ARGS: 'args',
  HTTP: 'http',
  RANDOM: 'random',
  CHANGES: 'changes',
  BINARY: 'binary',
  INDEX_RENAME: 'indexRename',
  CIRCLE: 'circle',
  DISTANCE: 'distance',
  FILL: 'fill',
  GEOJSON: 'geojson',
  TO_GEOJSON: 'toGeojson',
  GET_INTERSECTING: 'getIntersecting',
  GET_NEAREST: 'getNearest',
  INCLUDES: 'includes',
  INTERSECTS: 'intersects',
  LINE: 'line',
  POINT: 'point',
  POLYGON: 'polygon',
  POLYGON_SUB: 'polygonSub',
  UUID: 'uuid',
  RANGE: 'range',
  TO_JSON_STRING: 'toJSON',
  CONFIG: 'config',
  STATUS: 'status',
  WAIT: 'wait',
  RECONFIGURE: 'reconfigure',
  REBALANCE: 'rebalance',
  GRANT: 'grant',
  SPLIT: 'split',
  LITERAL: 'literal',
  MONDAY: 'monday',
  TUESDAY: 'tuesday',
  WEDNESDAY: 'wednesday',
  THURSDAY: 'thursday',
  FRIDAY: 'friday',
  SATURDAY: 'saturday',
  SUNDAY: 'sunday',
  JANUARY: 'january',
  FEBRUARY: 'february',
  MARCH: 'march',
  APRIL: 'april',
  MAY: 'may',
  JUNE: 'june',
  JULY: 'july',
  AUGUST: 'august',
  SEPTEMBER: 'september',
  OCTOBER: 'october',
  NOVEMBER: 'november',
  DECEMBER: 'december',
  MINVAL: 'minval',
  MAXVAL: 'maxval',
};
const typeToString = {};
Object.keys(_typeToString).forEach(key => (typeToString[termTypes[key]] = _typeToString[key]));

const _noPrefixOptargs = {
  ISO8601: true,
};
const noPrefixOptargs = {};
Object.keys(_noPrefixOptargs).forEach(key => (noPrefixOptargs[termTypes[key]] = true));

const _specialType = {
  DATUM(term, index, father, frames, options, optarg=false) {
    const underline = Array.isArray(frames) && (frames.length === 0);
    let currentFrame;
    let backtrace;
    if (Array.isArray(frames)) currentFrame = frames.shift();

    const result = {
      str: '',
      car: '',
    };

    if ((helper.isPlainObject(term)) && (term.$reql_type$ === 'BINARY')) {
      ReqlDriverError.carify(result, 'r.binary(<Buffer>)', underline);
      return result;
    }

    if ((index === 0) && ((father == null) || (!nonPrefix[father[0]]))) ReqlDriverError.carify(result, 'r.expr(', underline);

    if (typeof term === 'string') {
      ReqlDriverError.carify(result, `"${term}"`, underline);
    }
    else if (helper.isPlainObject(term)) {
      const totalKeys = Object.keys(term).length;
      if (totalKeys === 0) {
        ReqlDriverError.carify(result, '{}', underline);
      }
      else {
        ReqlDriverError.carify(result, '{\n', underline);
        let countKeys = 0;
        const extraToRemove = options.extra;
        options.indent += INDENT+options.extra;
        options.extra = 0;
        // for(const key in term) {
        Object.keys(term).forEach(key => {
          countKeys++;
          //if (!((father) && (Array.isArray(father[2])) && (Object.keys(father[2]).length > 0))) options.extra = 0;

          if (optarg) {
            ReqlDriverError.carify(result, `${space(options.indent)+camelCase(key)}: `, underline);
          }
          else {
            ReqlDriverError.carify(result, `${space(options.indent)+key}: `, underline);
          }
          if ((currentFrame != null) && (currentFrame === key)) {
            backtrace = ReqlDriverError.generateBacktrace(term[key], i, term, frames, options);
          }
          else {
            backtrace = ReqlDriverError.generateBacktrace(term[key], i, term, null, options);
          }
          result.str += backtrace.str;
          result.car += backtrace.car;

          if (countKeys !== totalKeys) {
            ReqlDriverError.carify(result, ',\n', underline);
          }
        });
        options.indent -= INDENT+extraToRemove;
        ReqlDriverError.carify(result, `\n${space(options.indent+extraToRemove)}}`, underline);
      }
    }
    else if (Array.isArray(term)) {
      ReqlDriverError.carify(result, '[', underline);
      for(let i=0; i<term.length; i++) {
        if ((currentFrame != null) && (currentFrame === i)) {
          backtrace = ReqlDriverError.generateBacktrace(term[i], i, term, frames, options);
        }
        else {
          backtrace = ReqlDriverError.generateBacktrace(term[i], i, term, null, options);
        }
        result.str += backtrace.str;
        result.car += backtrace.car;
      }
      ReqlDriverError.carify(result, ']', underline);
    }
    else {
      ReqlDriverError.carify(result, `${term}`, underline);
    }

    if ((index === 0) && ((father == null) || (!nonPrefix[father[0]]))) ReqlDriverError.carify(result, ')', underline);

    if (underline) result.car = result.str.replace(/./g, '^');

    return result;
  },
  TABLE(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    let currentFrame;


    if ((term.length === 1) || (term[1].length === 0) || (term[1][0][0] !== termTypes.DB)) {
      const underline = Array.isArray(frames) && (frames.length === 0);
      if (Array.isArray(frames)) currentFrame = frames.shift();

      ReqlDriverError.carify(result, `r.${typeToString[term[0]]}(`, underline);
      if (Array.isArray(term[1])) {
        for(let i=0; i<term[1].length; i++) {
          if (i !==0) result.str += ', ';


          if ((currentFrame != null) && (currentFrame === 1)) {
            // +1 for index because it's like if there was a r.db(...) before .table(...)
            backtrace = ReqlDriverError.generateBacktrace(term[1][i], i+1, term, frames, options);
          }
          else {
            backtrace = ReqlDriverError.generateBacktrace(term[1][i], i+1, term, null, options);
          }
          result.str += backtrace.str;
          result.car += backtrace.car;
        }
      }

      backtrace = ReqlDriverError.makeOptargs(term, i, term, frames, options, currentFrame);
      result.str += backtrace.str;
      result.car += backtrace.car;

      ReqlDriverError.carify(result, ')', underline);

      if (underline) result.car = result.str.replace(/./g, '^');
    }
    else {
      backtrace = ReqlDriverError.generateNormalBacktrace(term, index, father, frames, options);
      result.str = backtrace.str;
      result.car = backtrace.car;
    }

    return result;
  },
  GET_FIELD(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    let currentFrame;

    const underline = Array.isArray(frames) && (frames.length === 0);
    if (Array.isArray(frames)) currentFrame = frames.shift();

    if ((currentFrame != null) && (currentFrame === 0)) {
      backtrace = ReqlDriverError.generateBacktrace(term[1][0], 0, term, frames, options);
    }
    else {
      backtrace = ReqlDriverError.generateBacktrace(term[1][0], 0, term, null, options);
    }
    result.str = backtrace.str;
    result.car = backtrace.car;

    ReqlDriverError.carify(result, '(', underline);

    if ((currentFrame != null) && (currentFrame === 1)) {
      backtrace = ReqlDriverError.generateBacktrace(term[1][1], 1, term, frames, options);
    }
    else {
      backtrace = ReqlDriverError.generateBacktrace(term[1][1], 1, term, null, options);
    }
    result.str += backtrace.str;
    result.car += backtrace.car;

    ReqlDriverError.carify(result, ')', underline);

    if (underline) result.car = result.str.replace(/./g, '^');

    return result;
  },
  MAKE_ARRAY(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    let currentFrame;

    const underline = Array.isArray(frames) && (frames.length === 0);
    if (Array.isArray(frames)) currentFrame = frames.shift();

    if ((index === 0) && ((father == null) || (!nonPrefix[father[0]])))      ReqlDriverError.carify(result, 'r.expr(', underline);

    if (!((options) && (options.noBracket))) {
      ReqlDriverError.carify(result, '[', underline);
    }
    for(let i=0; i<term[1].length; i++) {
      if (i !== 0) {
        ReqlDriverError.carify(result, ', ', underline);
      }

      if ((currentFrame != null) && (currentFrame === i)) {
        backtrace = ReqlDriverError.generateBacktrace(term[1][i], i, term, frames, options);
      }
      else {
        backtrace = ReqlDriverError.generateBacktrace(term[1][i], i, term, null, options);
      }
      result.str += backtrace.str;
      result.car += backtrace.car;
    }

    if (!((options) && (options.noBracket))) {
      ReqlDriverError.carify(result, ']', underline);
    }

    if ((index === 0) && ((father == null) || (!nonPrefix[father[0]]))) {
      ReqlDriverError.carify(result, ')', underline);
    }

    if (underline) result.car = result.str.replace(/./g, '^');

    return result;
  },
  FUNC(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    let currentFrame;

    const underline = Array.isArray(frames) && (frames.length === 0);
    if (Array.isArray(frames)) currentFrame = frames.shift();

    if ((term[1][0][1].length === 1) && (helper.hasImplicit(term[1][1]))) {
      if ((currentFrame != null) && (currentFrame === 1)) {
        backtrace = ReqlDriverError.generateBacktrace(term[1][1], 1, term, frames, options);
      }
      else {
        backtrace = ReqlDriverError.generateBacktrace(term[1][1], 1, term, null, options);
      }
      result.str = backtrace.str;
      result.car = backtrace.car;
    }
    else {
      ReqlDriverError.carify(result, 'function(', underline);

      for(let i=0; i<term[1][0][1].length; i++) {
        if (i !== 0) {
          ReqlDriverError.carify(result, ', ', underline);
        }
        ReqlDriverError.carify(result, `var_${term[1][0][1][i]}`, underline);
      }

      options.indent += INDENT+options.extra;
      const extraToRemove = options.extra;
      options.extra = 0;
      //if (!((Array.isArray(term[2])) && (term[2].length > 0))) options.extra = 0;

      ReqlDriverError.carify(result, `) {\n${space(options.indent)}return `, underline);

      if ((currentFrame != null) && (currentFrame === 1)) {
        backtrace = ReqlDriverError.generateBacktrace(term[1][1], 1, term, frames, options);
      }
      else {
        backtrace = ReqlDriverError.generateBacktrace(term[1][1], 1, term, null, options);
      }

      result.str += backtrace.str;
      result.car += backtrace.car;

      options.indent -= INDENT+extraToRemove;
      options.extra = extraToRemove;

      ReqlDriverError.carify(result, `\n${space(options.indent+extraToRemove)}}`, underline);
    }

    if (underline) result.car = result.str.replace(/./g, '^');

    return result;
  },
  VAR(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    let currentFrame;

    const underline = Array.isArray(frames) && (frames.length === 0);
    if (Array.isArray(frames)) currentFrame = frames.shift();

    ReqlDriverError.carify(result, `var_${term[1][0]}`, underline);

    if (underline) result.car = result.str.replace(/./g, '^');
    return result;
  },
  FUNCALL(term, index, father, frames, options) {
    // The syntax is args[1].do(args[0])
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    let currentFrame;

    const underline = Array.isArray(frames) && (frames.length === 0);
    if (Array.isArray(frames)) currentFrame = frames.shift();

    if (term[1].length === 2) {
      if ((currentFrame != null) && (currentFrame === 1)) {
        backtrace = ReqlDriverError.generateBacktrace(term[1][1], 0, term, frames, options);
      }
      else {
        backtrace = ReqlDriverError.generateBacktrace(term[1][1], 0, term, null, options);
      }
      result.str = backtrace.str;
      result.car = backtrace.car;

      ReqlDriverError.carify(result, '.do(', underline);
    }
    else {
      ReqlDriverError.carify(result, 'r.do(', underline);

      for(let i=1; i<term[1].length; i++) {
        if ((currentFrame != null) && (currentFrame === i)) {
          backtrace = ReqlDriverError.generateBacktrace(term[1][i], i, term, frames, options);
        }
        else {
          backtrace = ReqlDriverError.generateBacktrace(term[1][i], i, term, null, options);
        }
        result.str += backtrace.str;
        result.car += backtrace.car;

        if (i !== term[1].length) {
          ReqlDriverError.carify(result, ', ', underline);
        }
      }
    }

    if ((currentFrame != null) && (currentFrame === 0)) {
      backtrace = ReqlDriverError.generateBacktrace(term[1][0], 0, term, frames, options);
    }
    else {
      backtrace = ReqlDriverError.generateBacktrace(term[1][0], 0, term, null, options);
    }
    result.str += backtrace.str;
    result.car += backtrace.car;

    ReqlDriverError.carify(result, ')', underline);

    if (underline) result.car = result.str.replace(/./g, '^');

    return result;
  },
  IMPLICIT_VAR(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    let currentFrame;

    const underline = Array.isArray(frames) && (frames.length === 0);
    if (Array.isArray(frames)) currentFrame = frames.shift();

    ReqlDriverError.carify(result, 'r.row', underline);

    if (underline) result.car = result.str.replace(/./g, '^');
    return result;
  },
  WAIT(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    let underline;
    let currentFrame;

    if (term.length === 1 || term[1].length === 0) {
      backtrace = ReqlDriverError.generateWithoutPrefixBacktrace(term, index, father, frames, options);
      result.str = backtrace.str;
      result.car = backtrace.car;
    }
    else {
      backtrace = ReqlDriverError.generateNormalBacktrace(term, index, father, frames, options);
      result.str = backtrace.str;
      result.car = backtrace.car;
    }
    return result;
  },
  MAP(term, index, father, frames, options) {
    const result = {
      str: '',
      car: '',
    };
    let backtrace;
    let underline;
    let currentFrame;

    if (term.length > 1 && term[1].length > 2) {
      backtrace = ReqlDriverError.generateWithoutPrefixBacktrace(term, index, father, frames, options);
      result.str = backtrace.str;
      result.car = backtrace.car;
    }
    else {
      backtrace = ReqlDriverError.generateNormalBacktrace(term, index, father, frames, options);
      result.str = backtrace.str;
      result.car = backtrace.car;
    }
    return result;
  },
};
_specialType.TABLE_CREATE = _specialType.TABLE;
_specialType.TABLE_DROP = _specialType.TABLE;
_specialType.TABLE_LIST = _specialType.TABLE;
_specialType.RECONFIGURE = _specialType.WAIT;
_specialType.REBALANCE = _specialType.WAIT;
_specialType.BRACKET = _specialType.GET_FIELD;

const specialType = {};
Object.keys(_specialType).forEach(key => (specialType[termTypes[key]] = _specialType[key]));

function camelCase(str) {
  return str.replace(/_(.)/g, (m, char) => char.toUpperCase());
}

const { generateBacktrace } = ReqlDriverError;
export { generateBacktrace };

export function setOperational(error) {
  error[IS_OPERATIONAL] = true;
  return error;
}
