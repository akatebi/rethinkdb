import Connection from './connection';
import Error from './error.js';
// import helper from './helper.js';
import Term from './term.js';
const termTypes = require('./protodef.js').Term.TermType;

class r extends Term {
  constructor(/*options*/) {
    super();
    const _r = x => new Term(_r).expr(x);

    /* eslint no-proto:1 */
    _r.__proto__ = this.__proto__;

    Term.prototype._setNestingLevel(r.prototype.nestingLevel);
    Term.prototype._setArrayLimit(r.prototype.arrayLimit);

    _r.row = new Term(_r).row();

    _r.monday = new Term(_r).monday();
    _r.tuesday = new Term(_r).tuesday();
    _r.wednesday = new Term(_r).wednesday();
    _r.thursday = new Term(_r).thursday();
    _r.friday = new Term(_r).friday();
    _r.saturday = new Term(_r).saturday();
    _r.sunday = new Term(_r).sunday();

    _r.january = new Term(_r).january();
    _r.february = new Term(_r).february();
    _r.march = new Term(_r).march();
    _r.april = new Term(_r).april();
    _r.may = new Term(_r).may();
    _r.june = new Term(_r).june();
    _r.july = new Term(_r).july();
    _r.august = new Term(_r).august();
    _r.september = new Term(_r).september();
    _r.october = new Term(_r).october();
    _r.november = new Term(_r).november();
    _r.december = new Term(_r).december();
    _r.minval = new Term(_r).minval();
    _r.maxval = new Term(_r).maxval();

    _r.nextVarId = 1;
    _r._Term = Term;
    return _r;
  }

  connect(options) {
    return new Connection(options);
  }

  setNestingLevel(nestingLevel) {
    if (typeof nestingLevel !== 'number') throw new Error.ReqlDriverError('The first argument of `setNestingLevel` must be a number.');
    this.nestingLevel = nestingLevel;
  }

  setArrayLimit(arrayLimit) {
    if (typeof arrayLimit !== 'number') throw new Error.ReqlDriverError('The first argument of `setArrayLimit` must be a number.');
    this.arrayLimit = arrayLimit;
  }

  expr(expression, nestingLevel) {
    if (Term.prototype._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arityRange(_args, 1, 2, 'expr', this);
    }
    const _nestingLevel = nestingLevel || this.nestingLevel;
    return new Term(this).expr(expression, _nestingLevel);
  }

  db(db) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.db', this);
    }
    return new Term(this).db(db);
  }

  table(table, options) {
    if (Term.prototype._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arityRange(_args, 1, 2, 'table', this);
    }
    return new Term(this).table(table, options);
  }

  js(jsString, options) {
    if (Term.prototype._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arityRange(_args, 1, 2, 'r.js', this);
    }
    return new Term(this).js(jsString, options);
  }

  tableCreate(table, options) {
    if (Term.prototype._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arityRange(_args, 1, 2, 'r.tableCreate', this);
    }
    return new Term(this).tableCreate(table, options);
  }

  tableDrop(db) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.tableDrop', this);
    }
    return new Term(this).tableDrop(db);
  }

  tableList() {
    if (Term.prototype._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 0, 'r.tableList', this);
    }
    return new Term(this).tableList();
  }

  dbCreate(db) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'dbCreate', this);
    }
    return new Term(this).dbCreate(db);
  }

  dbDrop(db) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'dbDrop', this);
    }
    return new Term(this).dbDrop(db);
  }

  dbList() {
    if (Term.prototype._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 0, 'dbList', this);
    }
    return new Term(this).dbList();
  }

  literal(obj) {
    if (Term.prototype._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arityRange(_args, 1, 2, 'r.literal', this);
    }
    if (obj === undefined) {
      return new Term(this).literal();
    }
    // else {
    return new Term(this).literal(obj);
    // }
  }

  desc(field) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.desc', this);
    }
    return new Term(this).desc(field);
  }

  asc(field) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.asc', this);
    }
    return new Term(this).asc(field);
  }

  union() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}

    const term = new Term(this).expr(_args[0]);
    return term.union(..._args.slice(1));
  }

  add() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.add', this);

    const term = new Term(this).expr(_args[0]);
    return term.add(..._args.slice(1));
  }

  sub() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.sub', this);

    const term = new Term(this).expr(_args[0]);
    return term.sub(..._args.slice(1));
  }

  div() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.div', this);

    const term = new Term(this).expr(_args[0]);
    return term.div(..._args.slice(1));
  }

  mul() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.mul', this);

    const term = new Term(this).expr(_args[0]);
    return term.mul(..._args.slice(1));
  }

  mod(a, b) {
    if (Term.prototype._fastArity(arguments.length, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 2, 'r.mod', this);
    }

    return new Term(this).expr(a).mod(b);
  }

  and() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}

    const term = new Term(this);
    return term.and(..._args);
  }

  or() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}

    const term = new Term(this);
    return term.or(..._args);
  }

  eq() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.eq', this);

    const term = new Term(this).expr(_args[0]);
    return term.eq(..._args.slice(1));
  }

  ne() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.ne', this);

    const term = new Term(this).expr(_args[0]);
    return term.ne(..._args.slice(1));
  }

  gt() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.gt', this);

    const term = new Term(this).expr(_args[0]);
    return term.gt(..._args.slice(1));
  }

  ge() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.ge', this);

    const term = new Term(this).expr(_args[0]);
    return term.ge(..._args.slice(1));
  }

  lt() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.lt', this);

    const term = new Term(this).expr(_args[0]);
    return term.lt(..._args.slice(1));
  }

  le() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.le', this);

    const term = new Term(this).expr(_args[0]);
    return term.le(..._args.slice(1));
  }

  not(bool) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.not', this);
    }
    return new Term(this).expr(bool).not();
  }

  floor(num) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.floor', this);
    }
    return new Term(this).expr(num).floor();
  }

  ceil(num) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.ceil', this);
    }
    return new Term(this).expr(num).ceil();
  }

  round(num) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.round', this);
    }
    return new Term(this).expr(num).round();
  }

  now() {
    if (Term.prototype._fastArity(arguments.length, 0) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 0, 'now', this);
    }
    return new Term(this).now();
  }

  time() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    const term = new Term(this);
    return term.time(..._args);
  }

  epochTime(epochTime) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.epochTime', this);
    }
    return new Term(this).epochTime(epochTime);
  }

  ISO8601(isoTime, options) {
    if (Term.prototype._fastArityRange(arguments.length, 1, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arityRange(_args, 1, 2, 'r.ISO8601', this);
    }
    /* eslint new-cap:0 */
    return new Term(this).ISO8601(isoTime, options);
  }

  branch(predicate /*, trueBranch, falseBranch*/) {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 3, Infinity, 'r.branch', this);

    const term = new Term(this).expr(predicate);
    return term.branch(..._args.slice(1));
  }

  error(errorStr) {
    if (Term.prototype._fastArityRange(arguments.length, 0, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arityRange(_args, 0, 1, 'r.error', this);
    }
    const term = new Term(this);
    term._query.push(termTypes.ERROR);
    if (errorStr !== undefined) {
      term._query.push([new Term(this).expr(errorStr)._query]);
    }
    return term;
  }

  json(json) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.json', this);
    }
    return new Term(this).json(json);
  }

  object() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    const term = new Term(this);
    return term.object(..._args);
  }

  args() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    const term = new Term(this);
    return term.args(..._args);
  }

  random() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    const term = new Term(this);
    return term.random(..._args);
  }

  http() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    const term = new Term(this);
    return term.http(..._args);
  }

  do() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.do', this);

    const term = new Term(this).expr(_args[0]);
    return term.do(..._args.slice(1));
  }

  binary(bin) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.binary', this);
    }
    const term = new Term(this);
    return term.binary(bin);
  }

  uuid() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 0, 1, 'r.uuid', this);
    const term = new Term(this);
    return term.uuid(_args[0]);
  }

  line() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, Infinity, 'r.line', this);

    const term = new Term(this);
    return term.line(..._args);
  }

  point(longitude, latitude) {
    if (Term.prototype._fastArity(arguments.length, 2) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 2, 'r.point', this);
    }
    return new Term(this).point(longitude, latitude);
  }

  polygon() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 3, Infinity, 'r.polygon', this);

    const term = new Term(this);
    return term.polygon(..._args);
  }

  circle(center, radius, options) {
    if (Term.prototype._fastArityRange(arguments.length, 2, 3) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arityRange(_args, 2, 3, 'r.circle', this);
    }
    const term = new Term(this);
    if (options !== undefined) {
      return term.circle(center, radius, options);
    }
    // else {
    return term.circle(center, radius);
    // }
  }

  geojson(value) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.geojson', this);
    }
    const term = new Term(this);
    return term.geojson(value);
  }

  distance() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 2, 3, 'r.add', this);

    const term = new Term(this).expr(_args[0]);
    return term.distance(..._args.slice(1));
  }

  range(start, end) {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 1, 2, 'r.range', this);

    const term = new Term(this);
    if (end !== undefined) {
      return term.range(start, end);
    }
    // else {
    return term.range(start);
    // }
  }

  wait() {
    // `wait` on the top level has been removed in 2.3.
    throw new Error.ReqlDriverError('`wait` can only be called on a table or a database since 2.3');
  }

  reconfigure(/*config*/) {
    // `reconfigure` on the top level has been removed in 2.3.
    throw new Error.ReqlDriverError('`reconfigure` can only be called on a table or a database since 2.3');
  }

  rebalance(/*config*/) {
    // `rebalance` on the top level has been removed in 2.3.
    throw new Error.ReqlDriverError('`rebalance` can only be called on a table or a database since 2.3');
  }

  map() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 1, Infinity, 'r.map', this);

    const term = new Term(this);
    return term.map(..._args);
  }

  typeOf(value) {
    if (Term.prototype._fastArity(arguments.length, 1) === false) {
      const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
      Term.prototype._arity(_args, 1, 'r.typeOf', this);
    }
    const term = new Term(this);
    return term.expr(value).typeOf();
  }

  min() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 1, Infinity, 'r.min', this);
    const term = new Term(this).expr(_args[0]);
    return term.min(..._args.slice(1));
  }

  max() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 1, Infinity, 'r.max', this);
    const term = new Term(this).expr(_args[0]);
    return term.max(..._args.slice(1));
  }

  sum() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 1, Infinity, 'r.sum', this);
    const term = new Term(this).expr(_args[0]);
    return term.sum(..._args.slice(1));
  }

  avg() {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 1, Infinity, 'r.avg', this);
    const term = new Term(this).expr(_args[0]);
    return term.avg(..._args.slice(1));
  }

  distinct(/*value*/) {
    const _len = arguments.length;const _args = new Array(_len); for(let _i = 0; _i < _len; _i++) {_args[_i] = arguments[_i];}
    Term.prototype._arityRange(_args, 1, Infinity, 'r.distinct', this);
    const term = new Term(this).expr(_args[0]);
    return term.distinct(..._args.slice(1));
  }
}

r.prototype._host = 'localhost';
r.prototype._port = 28015;
r.prototype._authKey = '';
r.prototype._user = 'admin';
r.prototype._password = '';
r.prototype._timeoutConnect = 20; // seconds
r.prototype._pingInterval = -1; // seconds

r.prototype._nestingLevel = 100;
r.prototype._arrayLimit = 100000;
r.prototype._db = 'test';
r.prototype._useOutdated = false;
r.prototype._timeFormat = 'native';
r.prototype._profile = false;

r.prototype.Error = Error;

export default new r;
