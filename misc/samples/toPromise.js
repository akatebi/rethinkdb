import { Observable } from 'rx';

var source = Observable.just(1).toArray().toPromise();

source.then(x => console.log(x));

var source2 = Observable.throw(new Error('reason')).toPromise();

source2.then(
  function (value) {
    console.log('Resolved value: %s', value);
  },
  function (reason) {
    console.log('Rejected reason: %s', reason);
  });
