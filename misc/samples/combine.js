import { Observable } from 'rx';


const s1 = Observable.range(1, 3);
const s2 = Observable.range(4, 3);

// s1.subscribe(x => console.log('s1', x));

// s2.subscribe(x => console.log('s2', x));

s1.concat(s2).subscribe(x => console.log(x));
