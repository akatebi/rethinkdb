import { Observable } from 'rx';

var source = Observable.return(0)
    .expand(function (x) { return Observable.return(10 + x); })
    .take(5);

var subscription = source.subscribe(
  function (x) {
    console.log('Next: %s', x);
  },
  function (err) {
    console.log('Error: %s', err);
  },
  function () {
    console.log('Completed');
  });
