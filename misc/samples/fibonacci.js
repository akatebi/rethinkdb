import { Observable } from 'rx';

function* fibonacci() {
  var fn1 = 1;
  var fn2 = 1;
  while (1) {
    var current = fn2;
    fn2 = fn1;
    fn1 = fn1 + current;
    yield current;
  }
}

Observable.from(fibonacci())
.take(50)
.subscribe(function (x) {
  console.log(x);
});
