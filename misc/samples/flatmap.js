import { Observable } from 'rx';

var source = Observable.range(0, 5)
  .flatMap(function (x) {
      return x === 4 ? Promise.reject(x) : Promise.resolve(x * x * x);
    });

var subscription = source.subscribe(
  function (x) { console.log('onNext: %s', x); },
  function (e) { console.log('onError: %s', e); },
  function () { console.log('onCompleted'); });
