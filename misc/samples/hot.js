import { Observable } from 'rx';

// console.log('Current time: ' + Date.now());
// Creates a sequence
let count = 0;
let myObserver;
setInterval(() => myObserver.next(count++), 1000);
const hot = Observable.create(observer => {
  myObserver = observer;
}).publish();
// Convert the sequence into a hot sequence
hot.connect();

// No value is pushed to 1st sub at this point
const sub1 = hot.subscribe(
  (x) => console.log('1: next:', x),
  (e) => console.log('1: error:', e),
  () => console.log('1: completed'));
setTimeout(function () {
  hot.connect();
  setTimeout(function () {
    const sub2 = hot.subscribe(
      (x) => console.log('2: next', x),
      (e) => console.log('2: error', e),
      () => console.log('2: completed'));
  }, 5000);
}, 5000);
