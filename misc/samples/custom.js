import { Observable, Disposable } from 'rx';

const source = Observable.create((observer) => {
  let count = 10;
  const tmr = setInterval(() => {
    observer.next(count);
    if (!--count) {
      // clearInterval(tmr);
      observer.completed(count)
    }
  }, 100);
  return Disposable.create(() => {
    console.log('Disposed');
    clearInterval(tmr);
  });
});

const subscription = source.subscribe(
  (x) => console.log('value', x),
  (err) => console.error('err`', err),
  () => console.log('Completed'));

// setTimeout(() => subscription.dispose(), 800);
