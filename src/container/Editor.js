import { connect } from 'react-redux';
import Editor from '../component/Editor';

function mapStateToProps(state) {
  const { userID } = state.signin;
  return { userID };
}

// function mapDispatchToProps(dispatch) {
  // const Actions = { onUserID };
  // return bindActionCreators(Actions, dispatch);
// }

export default connect(mapStateToProps)(Editor);
