import { USER_ID } from '../constant';

export const onUserID = (userID) =>
  ({
    type: USER_ID,
    userID,
  });
