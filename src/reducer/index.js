import { combineReducers } from 'redux';
import signin from './signin';


const rootReducer = combineReducers({
  signin,
});

export default rootReducer;
