import React from 'react';
import { PrismCode } from 'react-prism';

/* eslint import/no-unresolved:0 */
const source = require('!!raw!./Editor.js');

const GettingStarted = () =>
  <pre>
    <PrismCode className="language-jsx">
      {source}
    </PrismCode>
  </pre>;

export default GettingStarted;
