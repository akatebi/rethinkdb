import React from 'react';
import { Container, Button, Row, Col } from 'reactstrap';
import { Link } from 'react-router';
import Editor from '../container/Editor';

const Home = () =>
(
  <section className="jumbotron text-xs-center m-b-3">
    <Container fluid>
      <Row>
        <Col>
          <p className="lead">
            <img src="rejs.png" alt="" width="280px" style={{ opacity: 1 }} />
          </p>
          <h1 className="jumbotron-heading display-4">Containers-as-a-Service (CaaS) for RethinkDB</h1>
          <p className="lead">
            Our Web API gives you direct access to the Rethinkdb Query Language API in your Web and Mobile Apps. We have improved the original <Link to="/reql-api">ReQL API</Link> by replacing the database cursor in favor of the <Link to="/reactivex">Reactive Extensions</Link> Observable <a rel="noopener noreferrer" target="_blank" href="https://github.com/Reactive-Extensions/RxJS">RxJS</a>. We are using the <Link to="/socketio">Socket.io</Link> library for realtime duplex comunication with our services running on the <a rel="noopener noreferrer" target="_blank" href="http://docker.com">Docker Containers</a>.
          </p>
          <p>
            <Button color="primary" tag={Link} to="/getting-started/">
              Getting started
            </Button>
          </p>
          <Editor />
        </Col>
      </Row>
    </Container>
  </section>
);

export default Home;
