import React, { Component } from 'react';

class Socketio extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    setTimeout(() => {
      const iframe = (
        <iframe
          scrolling="auto"
          src="http://socket.io"
          className="embed-responsive-item"
        >
          <p>Your browser does not support iframes.</p>
        </iframe>);
      this.setState({ iframe });
    });
  }

  render() {
    return (
      <div
        className="embed-responsive embed-responsive-16by9"
        style={{ height: 1400 }}
      >
        {this.state.iframe}
      </div>
    );
  }
}

export default Socketio;
