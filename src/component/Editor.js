import { Disposable } from 'rx';
import React, { Component, PropTypes } from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import r from '../../rethinkdb/web';

const debug = require('debug')('app.editor');

class TextArea extends Component {

  static propTypes = {
    userID: PropTypes.string.isRequired,
  }

  constructor(props) {
    super(props);
    this.changes = this.changes.bind(this);
    this.onChange = this.onChange.bind(this);
    this.state = {};
  }

  async componentDidMount() {
    this.changes();
  }

  componentDidUpdate(props) {
    if (props !== this.props) {
      this.changes();
    }
  }

  async componentWillUnmount() {
    await this.conn.close();
  }

  async onChange(e) {
    const { value } = e.target;
    const { userID: id } = this.props;
    debug('userID', id);
    try {
      const result = await r.table('editor').get(id)
        .replace({ id, value }).promise(this.conn);
      debug('insert', result);
    } catch(err) { debug(err); }
  }

  // setup for a new user
  async changes() {
    // stop previous user's change feed
    if (Disposable.isDisposable(this.subscription)) {
      this.subscription.dispose();
      debug('subscription disposed');
    }
    // close previous user's connection
    if (this.conn) await this.conn.close();
    // connect for new user
    const jwToken = localStorage.getItem('jwToken');
    this.conn = await r.connect({ timeout: 5, db: 'test', jwToken });
    // get the current user's ID
    const { userID: id } = this.props;
    // insert a row for this user if missing
    try {
      const value = '';
      const result = await r.table('editor').insert({ id, value })
        .promise(this.conn);
      debug('insert id', result);
    } catch(err) { debug(err); }
    // get the current value from this user
    try {
      const { value } = await r.table('editor').get(id).promise(this.conn);
      debug('get', value);
      this.setState({ value });
    } catch(err) { debug(err); }
    // observe changes for user
    r.table('editor').get(id).changes()
      .observable(this.conn).subscribe(
        ({ new_val }) => {
          const { value } = new_val;
          debug('value', value);
          this.setState({ value });
        },
        (err) => debug('subscription err', err),
        () => debug('subscription completed')
      );
  }

  render() {
    return (
      <div className="col-xs-12">
        <TextareaAutosize
          className="col-xs-offset-2 col-xs-8"
          style={{ boxSizing: 'border-box' }}
          minRows={5}
          value={this.state.value}
          onChange={this.onChange}
          placeholder="Enter some text..."
        />
      </div>
    );
  }
}

export default TextArea;
