import 'babel-polyfill';

import { Observable, Disposable } from 'rx';
import EventEmitter from 'events';

import net from 'net';
import tls from 'tls';
import crypto from 'crypto';

import r from './ast';
import err from './errors.js';
import util from './util';
import protodef from './proto-def.js';

const debug = require('debug')('app.net');

// 1. Connection
// 2. _checkProtocolVersion
// 3. _computeSaltedPassword
// 4. _sendProof
// 5. _compareDigest
// 6. _getToken
// 7. _send

const PROTOCOL_VERSION = 0;
const AUTHENTIFICATION_METHOD = 'SCRAM-SHA-256';
const KEY_LENGTH = 32; // Because we are currently using SHA 256
const NULL_BUFFER = new Buffer('\0', 'binary');
const CACHE_PBKDF2 = {};
const CONN_CACHE_ENABLE = true;

class Connection extends EventEmitter {

  static cache = {};

  static connect(opts = {}) {
    debug('connect', opts);
    const defOpts = {
      host: 'localhost',
      port: 28015,
      db: 'test',
      user: 'admin',
      password: '',
      timeout: 20,
    };
    const options = { ...defOpts, ...opts };
    debug('options', options);
    const host = options;
    if (host.authKey && (host.password || host.user || host.username)) {
      throw new err.ReqlDriverError('Cannot use both authKey and password');
    }
    if (host.user && host.username) {
      throw new err.ReqlDriverError('Cannot use both user and username');
    } else if (host.authKey) {
      host.user = 'admin';
      host.password = host.authKey;
    } else {
      if (host.username) {
        host.user = host.username;
      }
    }
    const cache = Connection.cache[options.user];
    if (cache && CONN_CACHE_ENABLE) {
      cache.count++;
      debug('cache count', cache.count);
      return cache.conn;
    }
    Connection.cache[options.user] = { conn: new Connection(host), count: 1 };
    return Connection.cache[options.user].conn;
  }

  static xorBuffer(a, b) {
    const result = [];
    const len = Math.min(a.length, b.length);
    for(let i = 0; i < len; i++) {
      result.push(a[i] ^ b[i]);
    }
    return new Buffer(result);
  }

  static compareDigest(a, b) {
    let result;
    if (a.length === b.length) {
      result = 0;
    } else {
      result = 1;
    }
    const len = Math.min(a.length, b.length);
    for(let i = 0; i < len; i++) {
      result |= Connection.xorBuffer(a[i], b[i]);
    }
    return result === 0;
  }

  static splitCommaEqual(message) {
    const result = {};
    const messageParts = message.split(',');
    for(let i=0; i<messageParts.length; i++) {
      const equalPosition = messageParts[i].indexOf('=');
      result[messageParts[i].slice(0, equalPosition)] = messageParts[i].slice(equalPosition+1);
    }
    return result;
  }

  constructor(options) {
    debug('constructor', options);
    super();
    // this.netObservableCreate();
    return new Promise((resolve, reject) => {
      this.options = options;
      this.db = options.db;
      this.state = 0;
      this.open = false;
      this.token = this.token || 0;
      this.open = false;
      this.drained = true;
      this.queryQueue = [];
      if (!options.ssl) {
        const connectionArgs = {
          host: options.host,
          port: options.port,
        };
        this.connection = net.createConnection(connectionArgs);
      } else {
        const connectionArgs = {
          host: options.host,
          port: options.port,
          ca: options.ssl.ca,
        };
        this.options.password = this.options.authKey;
        this.connection = tls.connect(connectionArgs);
      }
      this.connection.setKeepAlive(true);
      this.connection.setNoDelay();
      this.connection.once('error', (error) => {
        debug('error', error.message);
        reject(error);
      });
      // ['end', 'close', 'timeout'].forEach(event =>
      //   this.connection.once(event, (msg) => {
      //     debug(event, `[${msg}]`);
      //   })
      // );
      this.connection.on('drain', () => {
        this.drained = true;
      });
      this.connection.once('connect', () => {
        const versionBuffer = new Buffer(4);
        versionBuffer.writeUInt32LE(protodef.VersionDummy.Version.V1_0, 0);
        this.randomString = new Buffer(crypto.randomBytes(18)).toString('base64');
        const authBuffer = new Buffer(JSON.stringify({
          protocol_version: PROTOCOL_VERSION,
          authentication_method: AUTHENTIFICATION_METHOD,
          authentication: `n,,n=${this.options.user},r=${this.randomString}`,
        }));
        debug(authBuffer.toString());
        this.connection.write(Buffer.concat([versionBuffer, authBuffer, NULL_BUFFER]));
      });
      this.buffer = new Buffer(0);
      this.connection.on('data', buffer => {
        this.buffer = Buffer.concat([this.buffer, buffer]);
        if(this.state < 3) {
          for (let i = 0; i < this.buffer.length; i++) {
            if (this.buffer[i] === 0) {
              const messageServerStr = this.buffer.slice(0, i).toString();
              this.buffer = this.buffer.slice(i+1); // +1 to remove the null byte
              const messageServer = JSON.parse(messageServerStr);
              if (!messageServer.success) {
                reject(`${messageServer.error_code}:${messageServer.error}`);
              }
              if (this.state === 0) {
                this._checkProtocolVersion(messageServer, reject);
              } else if (this.state === 1) {
                // Compute salt and send the proof
                this._computeSaltedPassword(messageServer, reject);
              } else if (this.state === 2) {
                this._compareDigest(messageServer, reject);
                resolve(this);
              }
            }
          }
        } else {
          while(this.buffer.length >= 12) {
            const token = this.buffer.readUInt32LE(0) + 0x100000000 * this.buffer.readUInt32LE(4);
            const responseLength = this.buffer.readUInt32LE(8);
            if (this.buffer.length < 12 + responseLength) break;
            const responseBuffer = this.buffer.slice(12, 12+responseLength);
            const response = JSON.parse(responseBuffer);
            // console.error('#####', token, response.t);
            this.emit(`${token}.token`, response);
            this.buffer = this.buffer.slice(12+responseLength);
          }
        }
      });
    });
  }

  writeQuery(token, query) {
    // debug('writeQuery', token, query);
    if (this.open === false) return;
    const queryStr = JSON.stringify(query);
    const querySize = Buffer.byteLength(queryStr);
    const buffer = new Buffer(8+4+querySize);
    buffer.writeUInt32LE(token & 0xFFFFFFFF, 0);
    buffer.writeUInt32LE(Math.floor(token / 0xFFFFFFFF), 4);
    buffer.writeUInt32LE(querySize, 8);
    buffer.write(queryStr, 12);
    this.drained = this.connection.write(buffer);
    // debug('drained', this.drained);
  }

  writeQueryQ(token, query, cb) {
    this.writeQuery(token, query);
    this.once(`${token}.token`, cb);
  }

  _send(query) {
    debug('_send', query);
    return Observable.create(observer => {
      const token = this._getToken();
      let done = false;
      this.writeQuery(token, query);
      this.on(`${token}.token`, (response) => {
        if(response.t < 6) {
          for(let i = 0; i < response.r.length ; i++) {
            if(response.p) {
              debug(response.p[0].description, response.p[0]['duration(ms)']);
            }
            debug(token, 'next');
            observer.next(response.r[i]);
          }
          if(response.t === 3) {
            this.writeQuery(token, [2]);
          } else {
            this.removeAllListeners(`${token}.token`);
            observer.completed();
            done = true;
          }
        } else {
          this.removeAllListeners(`${token}.token`);
          observer.error(response.r);
          done = true;
        }
      });
      return Disposable.create(() => {
        if (done) return;
        debug('disposed', token);
        this.writeQuery(token, [3]);
      });
    });
  }

  _sendQuery(query) {
    debug('_sendQuery', query);
    const data = [query.type];
    if (!(query.query === void 0)) {
      data.push(query.query);
      if (query.global_optargs && Object.keys(query.global_optargs).length > 0) {
        data.push(query.global_optargs);
      }
    }
    return this._send(data);
  }

  _start(term, opts = {}) {
    debug('_start', opts);

    if (!this.open) {
      return Observable.throw(new err.ReqlDriverError('connection not open'));
    }
    const query = {};
    query.global_optargs = {};
    const protoQueryType = protodef.Query.QueryType;
    query.type = protoQueryType.START;
    query.query = term.build();

    Object.keys(opts).forEach(key => {
      const value = opts[key];
      query.global_optargs[util.fromCamelCase(key)] = r.expr(value).build();
    });
    if (opts.db || this.db) {
      query.global_optargs.db = r.db(opts.db || this.db).build();
    }
    if (opts.noreply != null) {
      query.global_optargs.noreply = r.expr(!!opts.noreply).build();
    }
    if (opts.profile != null) {
      query.global_optargs.profile = r.expr(!!opts.profile).build();
    }
    return this._sendQuery(query);
  }

  _checkProtocolVersion(messageServer, reject) {
    debug('_checkProtocolVersion', messageServer);
    // Expect max_protocol_version, min_protocol_version, server_version, success
    const minVersion = messageServer.min_protocol_version;
    const maxVersion = messageServer.max_protocol_version;
    if (minVersion > PROTOCOL_VERSION || maxVersion < PROTOCOL_VERSION) {
      reject(`Unsupported protocol version: ${PROTOCOL_VERSION}, expected between ${minVersion} and ${maxVersion}`);
    }
    this.state = 1;
  }

  _computeSaltedPassword(messageServer, reject) {
    debug('_computeSaltedPassword', messageServer);
    const authentication = Connection.splitCommaEqual(messageServer.authentication);
    const randomNonce = authentication.r;
    const salt = new Buffer(authentication.s, 'base64');
    const iterations = parseInt(authentication.i, 10);
    if (randomNonce.substr(0, this.randomString.length) !== this.randomString) {
      this._abort().then(() =>
        reject(new err.ReqlDriverError('Invalid nonce from server')));
    } else {
      // The salt is constant, so we can cache the salted password.
      const cacheKey = `${this.options.password.toString('base64')},${salt.toString('base64')},${iterations}`;
      if (CACHE_PBKDF2.hasOwnProperty(cacheKey)) {
        this._sendProof(messageServer.authentication, randomNonce, CACHE_PBKDF2[cacheKey]);
      } else {
        crypto.pbkdf2(this.options.password, salt, iterations, KEY_LENGTH, 'sha256',
          (error, saltedPassword) => {
            if (error) {
              reject(error.toString());
            }
            CACHE_PBKDF2[cacheKey] = saltedPassword;
            this._sendProof(messageServer.authentication, randomNonce, saltedPassword);
          });
      }
    }
  }

  _sendProof(authentication, randomNonce, saltedPassword) {
    debug('_sendProof', authentication, randomNonce, saltedPassword);
    const clientFinalMessageWithoutProof = `c=biws,r=${randomNonce}`;
    const clientKey = crypto.createHmac('sha256', saltedPassword).update('Client Key').digest();
    const storedKey = crypto.createHash('sha256').update(clientKey).digest();

    const authMessage =
        `n=${this.options.user},r=${this.randomString},${authentication},${clientFinalMessageWithoutProof}`;

    const clientSignature = crypto.createHmac('sha256', storedKey).update(authMessage).digest();
    const clientProof = Connection.xorBuffer(clientKey, clientSignature);

    const serverKey = crypto.createHmac('sha256', saltedPassword).update('Server Key').digest();
    this.serverSignature = crypto.createHmac('sha256', serverKey).update(authMessage).digest();

    this.state = 2;
    const message = JSON.stringify({
      authentication: `${clientFinalMessageWithoutProof},p=${clientProof.toString('base64')}`,
    });
    debug(Buffer.concat([new Buffer(message.toString()), NULL_BUFFER]).toString());
    this.connection.write(Buffer.concat([new Buffer(message.toString()), NULL_BUFFER]));
  }

  _compareDigest(messageServer, reject) {
    debug('_compareDigest', messageServer);
    if (messageServer.error) {
      reject(messageServer.error);
      this.connection.removeAllListeners('error');
    } else {
      const firstEquals = messageServer.authentication.indexOf('=');
      const serverSignatureValue = messageServer.authentication.slice(firstEquals+1);

      if (! Connection.compareDigest(serverSignatureValue, this.serverSignature.toString('base64'))) {
        reject('Invalid server signature');
      }
      this.state = 4;
      this.connection.removeAllListeners('error');
      this.open = true;
    }
  }

  //////////////////////////////////////////////////////
  //////////////////////////////////////////////////////

  noreplyWait() {
    debug('noreplyWait');
    const query = [protodef.Query.QueryType.NOREPLY_WAIT];
    return this._send(query).toPromise();
  }

  getCount() {
    const { count } = Connection.cache[this.options.user];
    const { user } = this.options;
    return { count, user };
  }

  end() {
    debug('end');
    Connection.cache[this.options.user].count--;
    const { count } = Connection.cache[this.options.user];
    debug('cache delete', count, Object.keys(Connection.cache));
    if (count === 0) {
      debug('delete cache', this.options.user);
      delete Connection.cache[this.options.user];
      this.open = false;
      this.connection.end();
    }
    const { user } = this.options;
    return { count, user };
  }

  close(options = {}) {
    debug('close');
    if (options.noreplyWait === true) {
      return this.noreplyWait().then(() => this.end());
    }
    return Promise.resolve(this.end());
  }

  reconnect(options = {}) {
    debug('reconnect', options);
    return this.close(options).then(() =>
      this.constructor.call(this, this.options));
  }

  _abort() {
    debug('_abort');
    this.state = -1;
    this.removeAllListeners();
    return this.close();
  }

  //////////////////////////////////////////////////////
  //////////////////////////////////////////////////////

  use(db) {
    debug('use', db);
    // const this = this;
    if (typeof db !== 'string') throw new err.ReqlDriverError('First argument of `use` must be a string');
    this.db = db;
  }

  server() {
    // debug('server');
    const query = [protodef.Query.QueryType.SERVER_INFO];
    return this._send(query).toPromise();
  }

  _getToken() {
    // debug('_getToken');
    this.token += 1;
    this.token %= 0x80000000;
    return this.token;
  }

  _isOpen() {
    debug('_isOpen');
    return this.open;
  }

}

export default Connection;
