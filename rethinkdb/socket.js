import { Observable, Disposable } from 'rx';
import sizeof from 'object-sizeof';
import io from 'socket.io-client';
import r from './ast';
import util from './util';
import err from './errors.js';
import protodef from './proto-def.js';

const debug = require('debug')('app.socket');
const { hostname } = location;

class Connection {
  static connect(opts = {}) {
    debug('connect', opts);
    const defOpts = {
      nsp: '/rethinkdb',
      host: hostname || 'localhost',
      port: 4000,
      timeout: 20,
      db: 'test',
    };
    const options = { ...defOpts, ...opts };
    debug('options', options);
    return new Connection(options);
  }

  constructor(options) {
    return new Promise((resolve, reject) => {
      const { host, port, nsp, timeout, jwToken } = options;
      debug('new Connection', host, port);
      const socket = io(`http://${host}:${port}${nsp}`, {
        query: `jwToken=${jwToken}`,
      });
      if (isNaN(timeout)) {
        reject(new Error(`timeout ${timeout} is not a number`));
      }
      const tmr = setTimeout(
        () => reject(new Error(`connection timedout after ${timeout} seconds`)), timeout * 1000);
      socket.on('connect', () => {
        clearTimeout(tmr);
        this.db = options.db;
        this.socket = socket;
        this.open = true;
        this.id = 0;
        socket.once('net-connect', () => {
          debug('Connected');
          resolve(this);
        });
        socket.once('net-error', (error) => {
          debug('net-error', error);
          reject(error);
          socket.close();
        });
      });
      socket.once('error', (error) => {
        debug('Errored', error);
        throw error;
      });
    });
  }

  _getToken() {
    return new Promise(resolve => {
      this.id += 1;
      this.socket.emit('token', this.id);
      this.socket.once(`${this.id}.token`, (token) => resolve(token));
    });
  }

  _send(query) {
    debug('_send', query);
    return Observable.create(observer => {
      let done = false;
      const promise = this._getToken().then(token => {
        // debug('token', token);
        this.socket.emit('query', token, query);
        this.socket.on(`${token}.query`, (response) => {
          // debug('byteAck', token, sizeof(response));
          this.socket.emit('byteAck', token, sizeof(response));
          if(response.t < 6) {
            for(let i = 0; i < response.r.length ; i++) {
              if(response.p) {
                debug(response.p[0].description, response.p[0]['duration(ms)']);
              }
              // debug(token, 'next');
              observer.next(response.r[i]);
            }
            if(response.t === 3) {
              debug('more', token);
              this.socket.emit('more', token);
            } else {
              this.socket.removeAllListeners(`${token}.query`);
              observer.completed();
              done = true;
            }
          } else {
            this.socket.removeAllListeners(`${token}.query`);
            observer.error(response.r);
            done = true;
          }
        });
        return token;
      });
      return Disposable.create(() => {
        promise.then(token => {
          if (done) return;
          // debug('disposed', token);
          debug('stop', token);
          this.socket.emit('stop', token);
        });
      });
    });
  }

  _sendQuery(query) {
    // debug('_sendQuery', query);
    const data = [query.type];
    if (query.query) {
      data.push(query.query);
      if (query.global_optargs && Object.keys(query.global_optargs).length > 0) {
        data.push(query.global_optargs);
      }
    }
    return this._send(data);
  }

  _start(term, opts = {}) {
    // debug('_start', opts);
    if (!this.open) {
      return Observable.throw(new err.ReqlDriverError('connection not open'));
    }
    const protoQueryType = protodef.Query.QueryType;
    const query = {};
    query.global_optargs = {};
    query.type = protoQueryType.START;
    query.query = term.build();

    Object.keys(opts).forEach(key => {
      const value = opts[key];
      query.global_optargs[util.fromCamelCase(key)] = r.expr(value).build();
    });
    if (opts.db || this.db) {
      query.global_optargs.db = r.db(opts.db || this.db).build();
    }
    if (opts.noreply != null) {
      query.global_optargs.noreply = r.expr(!!opts.noreply).build();
    }
    if (opts.profile != null) {
      query.global_optargs.profile = r.expr(!!opts.profile).build();
    }
    return this._sendQuery(query);
  }

  /////////////////////////////////////////////////////////

  use(db) {
    debug('use', db);
    if (typeof db !== 'string') {
      throw new err.ReqlDriverError('First argument of `use` must be a string');
    }
    this.db = db;
  }
  server() {
    debug('server');
    return new Promise((resolve, reject) =>
      this.socket.emit('net-server', (error, server) => {
        if (error) reject(error);
        else resolve(server);
      }));
  }
  noreplyWait() {
    debug('noreplyWait');
    return new Promise((resolve, reject) =>
      this.socket.emit('net-noreplyWait', (error) => {
        if (error) reject(error);
        else resolve();
      }));
  }
  close(options = {}) {
    debug('close', options);
    return new Promise(resolve => {
      this.open = false;
      this.socket.close();
      resolve();
    });
  }
  reconnect(options = {}) {
    debug('reconnect', options);
    return new Promise((resolve, reject) => {
      if (!this.open) reject(new Error('socket not open'));
      this.socket.emit('net-reconnect', options, (error) => {
        debug('reconnect callback', error);
        if (error) reject(error);
        else resolve();
      });
    });
  }

}

export default Connection;
