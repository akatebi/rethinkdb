import expect from 'expect';
import r from '../rethinkdb/ast';

describe('Generate queries', () => {

  it('r.expr(1)', () => {
    const query = r.expr(1);
    expect(query.build()).toEqual(1);
  });

  it("r.table('tv_shows').filter(r.row('episodes').gt(100))", () => {
    const query = r.table('tv_shows').filter(r.row('episodes').gt(100));
    const expected = [39, [[15, ['tv_shows']], [69, [[2, [0]], [21, [[170, [[13, []], 'episodes']], 100]]]]]];
    expect(query.build()).toEqual(expected);
  });

  it("r.db('blog').table('users').filter({name: 'Michel'})", () => {
    const query = r.db('blog').table('users').filter({ name: 'Michel' });
    const expected = [39, [[15, [[14, ['blog']], 'users']], { name: 'Michel' }]];
    // console.log(query.toString());
    expect(query.build()).toEqual(expected);
  });

  it('Large query', () => {
    const users = [];
    for(let i = 0; i < 4; i++) {
      users.push({ name: 'ziggy', last: 'zaggy', address: '125 main street' });
    }
    const query = r.db('blog').table('users').insert(users);
    const expected = [56, [[15, [[14, ['blog']], 'users']], [2, [
      { name: 'ziggy', last: 'zaggy', address: '125 main street' },
      { name: 'ziggy', last: 'zaggy', address: '125 main street' },
      { name: 'ziggy', last: 'zaggy', address: '125 main street' },
      { name: 'ziggy', last: 'zaggy', address: '125 main street' },
    ]]]];
    expect(query.build()).toEqual(expected);
  });
});
