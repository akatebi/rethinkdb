import r from '../rethinkdb';
import expect from 'expect';
// import log from './log';

describe('Disposing', () => {
  let c;
  before('Create connection', async function() {
    c = await r.connect();
    expect(c.open).toBe(true);
  });
  after('Close connection', async function() {
    await c.close();
  });
  it('Dispose', (done) => {
    c._send([1, [97, ['hello', 'hello']]])
      .subscribe(x => console.log(x), done, done);
  });
});
