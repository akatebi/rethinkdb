import fs from 'fs';

export const log = (x, indent) => {
  console.log(indent ? JSON.stringify(x, 0, 2) : x);
  return x;
};

export const options = {
  host: 'localhost',
  port: 28015,
  user: 'admin',
  // password: '',
  authKey: '',
  ssl: {
    ca: fs.readFileSync(`${__dirname}/../cert/cert.pem`),
  },
};

export const optionsWS = {
  host: 'localhost',
  port: 3000,
};
