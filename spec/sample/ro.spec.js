const expect = require('expect');

import r from 'rethinkdb';


xdescribe('RethinkDB Specs', () => {

  let conn;

  before('should connect', async function() {
    conn = await r.connect({ host: 'localhost', user: 'admin', password: '', authKey: '' });
    // await conn.reconnect({ noreplayWait: true });
  });


  before('should drop all tables', async function() {
    const list = await r.dbList().run(conn);
    await Promise.all(
      list.filter(db => db !== 'rethinkdb').map(async function(db) {
        return r.dbDrop(db).run(conn);
      })
    );
  });

  before('should create test db', async function() {
    await r.dbCreate('test').run(conn);
  });

  after('should close connection', async function() {
    await conn.close({ noreplyWait: true });
  });

  it('should http', async function() {
    const query = 'https://www.google.com/?client=ubuntu#channel=fs&q=rethinkdb';
    const resp = await r.http(query).run(conn);
    // console.log(resp);
  });

  it('should expr', async function() {
    const resp = await r.expr(2 * 20 / 3).floor().run(conn);
    // console.log(resp);
  });

  it('should handle initial state', async function() {
    const resp = await r.dbList().run(conn);
    // const resp = await r.table("users").run(conn, {db: "blog"});
    // console.log(resp);
  });
  before('should reconnect', async function() {
    conn = await r.connect();
    await conn.reconnect({ noreplayWait: true });
  });


  before('should drop all tables', async function() {
    const list = await r.dbList().run(conn);
    await Promise.all(
      list.filter(db => db !== 'rethinkdb').map(async function(db) {
        return r.dbDrop(db).run(conn);
      })
    );
  });

  before('should create test db', async function() {
    await r.dbCreate('test').run(conn);
  });

  after('should close connection', async function() {
    await conn.close({ noreplyWait: true });
  });
  it('should do a valid long query', async function() {
    const dbName = 'db1';
    const tableName = 'tb1';
    await r.dbCreate(dbName).run(conn);
    await r.tableCreate('tb1').run(conn, { db: dbName });
    await r.db(dbName).table(tableName).insert([{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")},{a: r.js("Math.random()")}]).run(conn);
  });

  it('should do a long query', async function() {
    await r.tableCreate('foo').run(conn);
    try {
      await r.table("foo").add(1).add(1).add("hello-super-long-string")
      .add("another-long-string").add("one-last-string").map(
        function(doc) { return r.expr([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16])
        .map(function(test) {
          return test("b").add("hello-super-long-string")
          .add("another-long-string").add("one-last-string").add("hello-super-long-string").add("another-long-string").add("one-last-string").add("hello-super-long-string").add("another-long-string").add("one-last-string").add("hello-super-long-string").add("another-long-string").add("one-last-string").add("hello-super-long-string").add("another-long-string").add("one-last-string")
          .mul(test("b")).merge({ firstName: "xxxxxx", lastName: "yyyy", email: "xxxxx@yyyy.com", phone: "xxx-xxx-xxxx" }); }).add(2).map(function(doc) { return doc.add("hello-super-long-string").add("another-long-string").add("one-last-string").add("hello-super-long-string").add("another-long-string")
          .add("one-last-string").add("hello-super-long-string").add("another-long-string").add("one-last-string").add("hello-super-long-string").add("another-long-string").add("one-last-string").add("hello-super-long-string").add("another-long-string").add("one-last-string") }); }).run(conn);
        } catch(exc) {
          // console.log(exc.message);
          expect(exc.message).toMatch(/Expected type DATUM but found TABLE/);
        }
  });

  it('should do all specs', async function() {
    let resp;
    const dbName = 'dbName';
    const tableName = 'tableName';
    resp = await r.expr(1).run(conn);
    expect(resp).toBe(1);
    resp = await r.dbCreate(dbName).run(conn);
    expect(resp.dbs_created).toBe(1);
    resp = await r.db(dbName).tableCreate(tableName).run(conn);
    expect(resp.tables_created).toBe(1);
    resp = await r.db(dbName).table(tableName).insert(eval('['+new Array(100).join('{}, ')+'{}]')).run(conn);
    // log(resp);
    expect(resp.inserted).toBe(100);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.tableList().run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.tableList().run(conn);
    // await r.expr(1).run(conn);
    // await r.tableList().run(conn);
    // await r.expr(1).run(conn);
    // await r.expr(1).run(conn);
    // await r.expr(1).run(conn);
    // await r.expr(1).run(conn);
    // await r.expr(1).run(conn, {readMode: 'primary'});
    // await r.expr(1).run(conn, {readMode: 'majority'});
    // await r.expr(1).run(conn, {profile: false});
    // await r.expr(1).run(conn, {profile: true});
    // await r.expr(1).run(conn, {durability: 'soft'});
    // await r.expr(1).run(conn, {durability: 'hard'});
    // await r.now().run(conn);
    // await r.now().run(conn, {timeFormat: 'native'});
    // await r.now().run(conn, {timeFormat: 'raw'});
    // await r.binary(new Buffer([1,2,3])).run(conn, {binaryFormat: 'raw'});
    // await r.expr(true).run(conn, {profile: false});
    // await r.expr(true).run(conn, {profile: true});
    // await r.expr(true).run(conn, {profile: false});
    // await r.expr(1).run(conn)
    // await r.dbCreate(restrictedDbName).run(conn);
    // await r.db(restrictedDbName).tableCreate(restrictedTableName).run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(100).join('{}, ')+'{}]')).run(conn);
    // await r.db(dbName).config().run(conn);
    // await r.db(dbName).table(tableName).config().run(conn);
    // await r.db(dbName).table(tableName).status().run(conn);
    // await r.db(dbName).table(tableName).wait().run(conn);
    // await r.db(dbName).table(tableName).wait({waitFor: 'ready_for_writes', timeout: 2000}).run(conn);
    // await r.db(dbName).table(tableName).wait({waitFor: 'ready_for_writes'}).run(conn);
    // await r.db(dbName).table(tableName).reconfigure({shards: 1, replicas: 1}).run(conn);
    // await r.db(dbName).table(tableName).reconfigure({shards: 1, replicas: 1, dryRun: true}).run(conn);
    // await r.db(dbName).table(tableName).rebalance().run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.expr([1,2,3]).reduce(function(left, right) { return left.add(right) }).run(conn);
    // await r.db(dbName).table(tableName).reduce().run(conn);
    // await r.expr([1,2,3]).fold(10, function(left, right) { return left.add(right) }).run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5]).count().run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5]).count(r.row.eq(2)).run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5]).count(function(doc) { return doc.eq(2) }).run(conn);
    // await r.db(dbName).table(tableName).indexCreate("group").run(conn);
    // await r.db(dbName).table(tableName).indexWait("group").run(conn);
    // await r.db(dbName).table(tableName).group({index: "group"}).run(conn);
    // await r.expr([1,2,3]).contains(2).run(conn);
    // await r.expr([1,2,3]).contains(1, 2).run(conn);
    // await r.expr([1,2,3]).contains(1, 5).run(conn);
    // await r.expr([1,2,3]).contains(function(doc) { return doc.eq(1) }).run(conn);
    // await r.expr([1,2,3]).contains(r.row.eq(1)).run(conn);
    // await r.expr([1,2,3]).contains(r.row.eq(1), r.row.eq(2)).run(conn);
    // await r.expr([1,2,3]).contains(r.row.eq(1), r.row.eq(5)).run(conn);
    // await r.db(dbName).table(tableName).contains().run(conn);
    // await r.expr([1,2,3]).sum().run(conn);
    // await r.expr([{a: 2}, {a: 10}, {a: 9}]).sum('a').run(conn);
    // await r.expr([1,2,3]).avg().run(conn);
    // await r.avg([1,2,3]).run(conn);
    // await r.expr([{a: 2}, {a: 10}, {a: 9}]).avg('a').run(conn);
    // await r.avg([{a: 2}, {a: 10}, {a: 9}], 'a').run(conn);
    // await r.expr([1,2,3]).min().run(conn);
    // await r.min([1,2,3]).run(conn);
    // await r.expr([{a: 2}, {a: 10}, {a: 9}]).min('a').run(conn);
    // await r.min([{a: 2}, {a: 10}, {a: 9}], 'a').run(conn);
    // await r.expr([1,2,3]).max().run(conn);
    // await r.max([1,2,3]).run(conn);
    // await r.expr([1,2,3,1,2,1,3,2,2,1,4]).distinct().orderBy(r.row).run(conn);
    // await r.distinct([1,2,3,1,2,1,3,2,2,1,4]).orderBy(r.row).run(conn);
    // await r.db(dbName).table(tableName).distinct({index: "id"}).count().run(conn);
    // await r.db(dbName).table(tableName).count().run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(100).join('{}, ')+'{}]')).run(conn);
    // await r.dbDrop(1).run(conn)
    // await r.dbCreate(1).run(conn)
    // await r.dbList().do(function(x) { return x.add("a") }).run(conn)
    // await r.expr(2).do(function(x) { return x.add("a") }).run(conn)
    // await r.db(dbName).tableCreate(tableName).run(conn)
    // await r.db(dbName).tableDrop("nonExistingTable").run(conn)
    // await r.db(dbName).tableList().do(function(x) { return x.add("a") }).run(conn)
    // await r.expr(["zoo", "zoo"]).forEach(function(index) { return r.db(dbName).table(tableName).indexCreate(index) }).run(conn)
    // await r.db(dbName).table(tableName).indexDrop("nonExistingIndex").run(conn)
    // await r.db(dbName).table(tableName).indexList().do(function(x) { return x.add("a") }).run(conn)
    // await r.db(dbName).table(tableName).indexWait().do(function(x) { return x.add("a") }).run(conn)
    // await r.db(dbName).table(tableName).indexWait("foo", "bar").run(conn)
    // await r.db(dbName).table(tableName).indexStatus().and( r.expr(1).add("a")).run(conn)
    // await r.db(dbName).table(tableName).indexStatus("foo", "bar").do(function(x) { return x.add("a") }).run(conn)
    // await r.db(dbName).table("nonExistingTable").update({foo: "bar"}).run(conn)
    // await r.db(dbName).table("nonExistingTable").update(function(doc) { return doc("foo") }).run(conn)
    // await r.db(dbName).table("nonExistingTable").replace({foo: "bar"}).run(conn)
    // await r.db(dbName).table("nonExistingTable").replace(function(doc) { return doc("foo") }).run(conn)
    // await r.db(dbName).table("nonExistingTable").delete().run(conn)
    // await r.db(dbName).table("nonExistingTable").sync().run(conn)
    // await r.db("nonExistingDb").table("nonExistingTable").run(conn)
    // await r.db(dbName).table("nonExistingTable").run(conn)
    // await r.db(dbName).table(tableName).get(1).do(function(x) { return x.add(3) }).run(conn)
    // await r.db(dbName).table(tableName).getAll(1, 2, 3).do(function(x) { return x.add(3) }).run(conn)
    // await r.db(dbName).table(tableName).getAll(1, 2, 3, { index: "foo"}).do(function(x) { return x.add(3) }).run(conn)
    // await r.db(dbName).table(tableName).between(2, 3, { index: "foo"}).do(function(x) { return x.add(3) }).run(conn)
    // await r.db(dbName).table(tableName).filter({foo: "bar"}).do(function(x) { return x.add(3) }).run(conn)
    // await r.expr([1,2,3]).innerJoin( function(left, right) { return left.eq(right("bar").add(1)) }, r.db(dbName).table(tableName)).run(conn)
    // await r.expr([1,2,3]).innerJoin(r.expr([1,2,3]), function(left, right) { return r.expr(1).add("str").add(left.eq(right("bar").add(1))) }).run(conn)
    // await r.expr([1,2,3]).outerJoin( function(left, right) { return left.eq(right("bar").add(1)) }, r.db(dbName).table(tableName)).run(conn)
    // await r.expr([1,2,3]).eqJoin("id", r.db(dbName).table(tableName)).add(1).run(conn)
    // await r.expr([1,2,3]).eqJoin("id", r.db(dbName).table(tableName)).zip().add(1).run(conn)
    // await r.expr([1,2,3]).map(function(v) { return v}).add(1).run(conn)
    // await r.expr([1,2,3]).withFields("foo", "bar").add(1).run(conn)
    // await r.expr([1,2,3]).concatMap(function(v) { return v}).add(1).run(conn)
    // await r.expr([1,2,3]).orderBy("foo").add(1).run(conn)
    // await r.expr([1,2,3]).skip("foo").add(1).run(conn)
    // await r.expr([1,2,3]).limit("foo").add(1).run(conn)
    // await r.expr([1,2,3]).slice("foo", "bar").add(1).run(conn)
    // await r.expr([1,2,3]).nth("bar").add(1).run(conn)
    // await r.expr([1, 2, 3]).offsetsOf("bar").add("Hello").run(conn)
    // await r.expr([1,2,3]).isEmpty().add("Hello").run(conn)
    // await r.expr([1,2,3]).union([5,6]).add("Hello").run(conn)
    // await r.expr([1,2,3]).sample("Hello").run(conn)
    // await r.expr([1,2,3]).count(function() { return true}).add("Hello").run(conn)
    // await r.expr([1,2,3]).distinct().add("Hello").run(conn)
    // await r.expr([1,2,3]).contains("foo", "bar").add("Hello").run(conn)
    // await r.expr([1,2,3]).update(r.row("foo")).add("Hello").run(conn)
    // await r.expr([1,2,3]).pluck("foo").add("Hello").run(conn)
    // await r.expr([1,2,3]).without("foo").add("Hello").run(conn)
    // await r.expr([1,2,3]).merge("foo").add("Hello").run(conn)
    // await r.expr([1,2,3]).append("foo").add("Hello").run(conn)
    // await r.expr([1,2,3]).prepend("foo").add("Hello").run(conn)
    // await r.expr([1,2,3]).difference("foo").add("Hello").run(conn)
    // await r.expr([1,2,3]).setInsert("foo").add("Hello").run(conn)
    // await r.expr([1,2,3]).setUnion("foo").add("Hello").run(conn)
    // await r.expr([1,2,3]).setIntersection("foo").add("Hello").run(conn)
    // await r.expr([1, 2, 3])("foo").add("Hello").run(conn)
    // await r.expr([1,2,3]).hasFields("foo").add("Hello").run(conn)
    // await r.expr([1,2,3]).insertAt("foo", 2).add("Hello").run(conn)
    // await r.expr([1,2,3]).spliceAt("foo", 2).add("Hello").run(conn)
    // await r.expr([1,2,3]).deleteAt("foo", 2).add("Hello").run(conn)
    // await r.expr([1,2,3]).changeAt("foo", 2).add("Hello").run(conn)
    // await r.expr([1,2,3]).match("foo").add("Hello").run(conn)
    // await r.expr([1,2,3]).add("Hello").run(conn)
    // await r.expr([1,2,3]).sub("Hello").run(conn)
    // await r.expr([1,2,3]).mul("Hello").run(conn)
    // await r.expr([1,2,3]).div("Hello").run(conn)
    // await r.expr([1,2,3]).mod("Hello").run(conn)
    // await r.expr([1,2,3]).and(r.expr("Hello").add(2)).run(conn)
    // await r.expr(false).or(r.expr("Hello").add(2)).run(conn)
    // await r.expr([1,2,3]).eq(r.expr("Hello").add(2)).run(conn)
    // await r.expr([1,2,3]).ne(r.expr("Hello").add(2)).run(conn)
    // await r.expr([1,2,3]).gt(r.expr("Hello").add(2)).run(conn)
    // await r.expr([1,2,3]).lt(r.expr("Hello").add(2)).run(conn)
    // await r.expr([1,2,3]).le(r.expr("Hello").add(2)).run(conn)
    // await r.expr([1,2,3]).ge(r.expr("Hello").add(2)).run(conn)
    // await r.expr([1,2,3]).not().add(r.expr("Hello").add(2)).run(conn)
    // await r.now().add("Hello").run(conn)
    // await r.time(1023, 11, 3, "Z").add("Hello").run(conn)
    // await r.epochTime(12132131).add("Hello").run(conn)
    // await r.ISO8601("UnvalidISO961String").add("Hello").run(conn)
    // await r.now().inTimezone("noTimezone").add("Hello").run(conn)
    // await r.now().timezone().add(true).run(conn)
    // await r.now().during(r.now(), r.now()).add(true).run(conn)
    // await r.now().timeOfDay().add(true).run(conn)
    // await r.now().year().add(true).run(conn)
    // await r.now().month().add(true).run(conn)
    // await r.now().day().add(true).run(conn)
    // await r.now().dayOfWeek().add(true).run(conn)
    // await r.now().dayOfYear().add(true).run(conn)
    // await r.now().hours().add(true).run(conn)
    // await r.now().minutes().add(true).run(conn)
    // await r.now().seconds().add(true).run(conn)
    // await r.now().toISO8601().add(true).run(conn)
    // await r.now().toEpochTime().add(true).run(conn)
    // await r.expr(1).do(function(var_1) { return var_1("bah").add(3) }) .run(conn)
    // await r.branch(r.expr(1).add("hello"), "Hello", "World").run(conn)
    // await r.expr(1).forEach(function(foo) { return foo("bar") }).run(conn)
    // await r.error("foo").run(conn)
    // await r.expr({a:1})("b").default("bar").add(2).run(conn)
    // await r.expr({a:1}).add(2).run(conn)
    // await r.expr({a:1}).add(r.js("2")).run(conn)
    // await r.expr(2).coerceTo("ARRAY").run(conn)
    // await r.expr(2).add("foo").typeOf().run(conn)
    // await r.expr(2).add("foo").info().run(conn)
    // await r.expr(2).add(r.json("foo")).run(conn)
    // await r.db(dbName).table(tableName).replace({a:1}, {nonValid:true}).run(conn);
    // await r.db(dbName).table(tableName).replace({a:1}, {durability: "softt"}).run(conn);
    // await r.expr([1,2]).map(r.row.add("eh")).run(conn);
    // await r.expr({a:1, b:r.expr(1).add("eh")}).run(conn);
    // await r.db(dbName).table(tableName).replace({a:1}, {durability:"soft"}).add(2).run(conn)
    // await r.db(dbName).table(tableName).replace({a:1}, {durability:r.expr(1).add("heloo")}).run(conn)
    // await r.db(dbName).table(tableName).replace({a:1}, {durability:r.expr(1).add("heloo")}).run(conn)
    // await r.expr({a:r.expr(1).add("eh"), b: 2}).run(conn)
    // await r.expr([1,2,3]).add("eh").run(conn)
    // await r.expr({a:1}).add("eh").run(conn)
    // await r.expr([1,2,3]).group("foo").run(conn)
    // await r.expr([1,2,3]).ungroup().run(conn)
    // await r.expr([1,2,3,"hello"]).sum().run(conn)
    // await r.expr([1,2,3,"hello"]).avg().run(conn)
    // await r.expr([]).min().run(conn)
    // await r.expr([]).max().run(conn)
    // await r.expr([]).avg().run(conn)
    // await r.expr(1).upcase().run(conn)
    // await r.expr(1).downcase().run(conn)
    // await r.expr(1).do(function(v) { return r.object(1, 2) }).run(conn)
    // await r.expr(1).do(function(v) { return r.object("a") }).run(conn)
    // await r.random(1,2,{float: true}).sub("foo").run(conn)
    // await r.random("foo", "bar").run(conn)
    // await r.random("foo", "bar", "buzz", "lol").run(conn)
    // await r.db(dbName).table(tableName).changes().add(2).run(conn)
    // await r.http("").add(2).run(conn)
    // await r.args(["foo", "bar"]).add(2).run(conn)
    // await r.do(1,function( b) { return b.add("foo") }).run(conn)
    // await r.db(dbName).table(tableName).between("foo", "bar", {index: "id"}).add(1).run(conn)
    // await r.db(dbName).table(tableName).orderBy({index: "id"}).add(1).run(conn)
    // await r.binary("foo").add(1).run(conn)
    // await r.binary(new Buffer([0,1,2,3,4])).add(1).run(conn)
    // await r.do(1,function( b) { return r.point(1, 2).add("foo") }).run(conn)
    // await r.do(1,function( b) { return r.line(1, 2).add("foo") }).run(conn)
    // await r.do(1,function( b) { return r.circle(1, 2).add("foo") }).run(conn)
    // await r.do(1,function( b) { return r.polygon(1, 2, 3).add("foo") }).run(conn)
    // await r.do(1,function( b) { return r.polygon([0,0], [1,1], [2,3]).polygonSub(3).add("foo") }).run(conn)
    // await r.do(1,function( b) { return r.polygon([0,0], [1,1], [2,3]).fill().polygonSub(3).add("foo") }).run(conn)
    // await r.do(1,function( b) { return r.polygon([0,0], [1,1], [2,3]).distance(r.expr("foo").polygonSub(3)).add("foo") }).run(conn)
    // await r.db(dbName).table(tableName).getIntersecting(r.circle(0, 1), 3).run(conn)
    // await r.db(dbName).table(tableName).getNearest(r.circle(0, 1), 3).run(conn)
    // await r.polygon([0, 0], [0, 1], [1, 1]).includes(r.expr([0, 1, 3])).run(conn)
    // await r.polygon([0, 0], [0, 1], [1, 1]).intersects(r.expr([0, 1, 3])).run(conn)
    // await r.polygon([0, 0], [0, 1], [1, 1]).includes(r.expr([0, 1, 3])).run(conn)
    // await r.db(dbName).table(tableName).orderBy(r.desc("foo")).add(1).run(conn)
    // await r.db(dbName).table(tableName).orderBy(r.asc("foo")).add(1).run(conn)
    // await r.range("foo").run(conn)
    // await r.range(1,10).do(function(x) { return x.add(4) }).run(conn)
    // await r.range(1,10).toJSON().do(function(x) { return x.add(4) }).run(conn)
    // await r.db(dbName).table(tableName).config().do(function(x) { return x.add(4) }).run(conn)
    // await r.db(dbName).table(tableName).status().do(function(x) { return x.add(4) }).run(conn)
    // await r.db(dbName).table(tableName).wait().do(function(x) { return x.add(4) }).run(conn)
    // await r.db(dbName).table(tableName).reconfigure({ shards: 1 }).do(function(x) { return x.add(4) }).run(conn)
    // await r.expr(1).add("foo").add(r.db(dbName).table(tableName).rebalance().do(function(x) { return x.add(4) })).run(conn)
    // await r.map([1,2,3], [1,2,3], function(var_1) { return var_1("bah").add(3) }).run(conn)
    // await r.map([1,2,3], [1,2,3], function(var_1, var_2) { return var_1("bah").add(3) }).run(conn)
    // await r.expr([1,2,3]).split(",", 3).add(3).run(conn)
    // await r.expr({}).merge({a: r.literal({foo: "bar"})}).add(2).run(conn)
    // await r.monday.add([1]).run(conn)
    // await r.november.add([1]).run(conn)
    // await r.expr({a: r.wednesday}).add([1]).run(conn)
    // await r.db(dbName).table(tableName).between(r.minval, r.maxval, {index: "foo"}).add(1).run(conn)
    // await r.expr(1).add("bar").add(r.ISO8601("dadsa",{defaultTimezone: "dsada"})).run(conn)
    // await r.expr({foo: "bar"}).merge({foo: r.literal(), bar: r.expr("lol").add(1)}).run(conn)
    // await r.floor("hello").run(conn)
    // await r.floor().run(conn)
    // await r.round("hello").run(conn)
    // await r.ceil("hello").run(conn)
    // await r.expr({a:1, b:2, c: 3}).values().add(2).run(conn)
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(100).join('{}, ')+'{}]')).run(conn);
    // await r.db(dbName).table(tableName).map(function(doc) { return doc("key").add(NaN)}).run(conn)
    // await r.db(dbName).table(tableName).map(function(doc) { return doc("key").add(Infinity)}).run(conn)
    // await r.db(dbName).table(tableName).map(function(doc) { return doc("key").add(undefined)}).run(conn)
    // await r.db(dbName).table(tableName).merge(function(user) { return r.branch( user("location").eq("US"), { adult: user("age").gt(NaN) }, {radult: user("age").gt(18) }) }).run(conn)
    // await r.expr({a: 1}).do( function(doc) { return doc("a") }).run(conn);
    // await r.do(1, 2, function(a, b) { return a }).run(conn);
    // await r.do(1, 2, function(a, b) { return b }).run(conn);
    // await r.expr(1).do().run(conn);
    // await r.branch(true, 1, 2).run(conn);
    // await r.branch(false, 1, 2).run(conn);
    // await r.expr(false).branch('foo', false, 'bar', 'lol').run(conn)
    // await r.expr(true).branch('foo', false, 'bar', 'lol').run(conn)
    // await r.expr(false).branch('foo', true, 'bar', 'lol').run(conn)
    // await r.branch().run(conn);
    // await r.branch(true).run(conn);
    // await r.branch(true, true).run(conn);
    // await r.expr(true).branch(2, 3).run(conn);
    // await r.expr(false).branch(2, 3).run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.expr([{foo: "bar"}, {foo: "foo"}]).forEach().run(conn);
    // await r.range(10).run(conn);
    // await r.range(3,10).run(conn);
    // await r.range(1,2,3).run(conn)
    // await r.range().run(conn)
    // await r.expr({a:1})("b").default("Hello").run(conn);
    // await r.expr({})("").default().run(conn);
    // await r.js("1").run(conn);
    // await r.expr(1).js("foo").run(conn);
    // await r.js().run(conn);
    // await r.expr(1).coerceTo("STRING").run(conn);
    // await r.expr(1).coerceTo().run(conn);
    // await r.expr(1).typeOf().run(conn);
    // await r.typeOf(1).run(conn);
    // await r.json(JSON.stringify({a:1})).run(conn);
    // await r.json("{}").run(conn);
    // await r.json().run(conn);
    // await r.expr(1).json("1").run(conn);
    // await r.expr({a:1}).toJSON().run(conn);
    // await r.expr({a:1}).toJsonString().run(conn);
    // await r.expr({a:1}).toJSON('foo').run(conn);
    // await r.args([10, 20, 30]).run(conn);
    // await r.expr({foo: 1, bar: 2, buzz: 3}).pluck(r.args(["foo", "buzz"])).run(conn)
    // await r.table("foo").eqJoin(r.args([r.row, r.table("bar")])).run(conn);
    // await r.http('http://google.com').run(conn);
    // await r.http('http://google.com', {timeout: 60}).run(conn);
    // await r.http('http://google.com', {foo: 60}).run(conn);
    // await r.uuid().run(conn);
    // await r.uuid("rethinkdbdash").run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn)
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(numDocs).join('{}, ')+'{}]')).run(conn);
    // await r.db(dbName).table(tableName2).insert(eval('['+new Array(smallNumDocs).join('{}, ')+'{}]')).run(conn);
    // await r.db(dbName).table(tableName).run({cursor: true});
    // await r.db(dbName).table(tableName).run({cursor: true});
    // await r.db(dbName).table(tableName).run({cursor: true});
    // await r.db(dbName).table(tableName).run({cursor: true});
    // await r.db(dbName).table(tableName).run({cursor: true});
    // await r.db(dbName).table(tableName).run({cursor: true});
    // await r.db(dbName).table(tableName).run({cursor: true, profile: true});
    // await r.expr([1,2,3]).run({cursor: true});
    // await r.db(dbName).table(tableName2).run({cursor: true});
    // await r.db(dbName).table(tableName2).run({cursor: true});
    // await r.db(dbName).table(tableName2).run({cursor: true});
    // await r.db(dbName).table(tableName2).run({cursor: true});
    // await r.db(dbName).table(tableName).run({cursor: true});
    // await r.db(dbName).table(tableName).update({val: 1}).run(conn);
    // await r.db(dbName).table(tableName).run(conn, {cursor: true, maxBatchRows: 1});
    // await r.db(dbName).table(tableName).run(conn, {maxBatchRows: 1});
    // await r.db(dbName).table(tableName).run(conn, {cursor: true, maxBatchRows: 1});
    // await r.db(dbName).table(tableName).changes().run(conn);
    // await r.db(dbName).table(tableName).changes({squash: true}).run(conn);
    // await r.db(dbName).table(tableName).get(1).changes().run(conn);
    // await r.db(dbName).table(tableName).orderBy({index: 'id'}).limit(2).changes().run(conn);
    // await r.db(dbName).table(tableName2).changes().run(conn);
    // await r.db(dbName).table(tableName2).get(idValue).changes({includeInitial: true}).run(conn);
    // await r.db(dbName).table(tableName2).changes().run(conn);
    // await r.db(dbName).table(tableName2).changes().run(conn);
    // await r.db(dbName).table(tableName2).changes().run(conn);
    // await r.db(dbName).table(tableName2).run({cursor: true});
    // await r.db(dbName).table(tableName2).changes().run(conn);
    // await r.db(dbName).table(tableName2).changes().run(conn);
    // await r.db(dbName).table(tableName2).changes().run(conn);
    // await r.db(dbName).table(tableName2).get(1).changes().run(conn);
    // await r.db(dbName).table(tableName2).changes().run(conn);
    // await r.db(dbName).table(tableName).orderBy({index: 'id'}).limit(10).changes({includeStates: true, includeInitial: true}).run(conn);
    // await r.db(dbName).table(tableName).changes().run(conn);
    // await r.db(dbName).table(tableName).changes().run(conn);
    // await r.now().run(conn);
    // await r.expr({a: r.now()}).run(conn);
    // await r.expr([r.now()]).run(conn);
    // await r.expr([{}, {a: r.now()}]).run(conn);
    // await r.expr({b: [{}, {a: r.now()}]}).run(conn);
    // await r.expr(1).now("foo").run(conn);
    // await r.time(1986, 11, 3, 12, 0, 0, 'Z').run(conn);
    // await r.time(1986, 11, 3, 12, 20, 0, 'Z').minutes().run(conn);
    // await r.time(r.args([1986, 11, 3, 12, 0, 0, 'Z'])).run(conn);
    // await r.time(1986, 11, 3, 'Z').run(conn);
    // await r.time(1986, 11, 3, 0, 0, 0, 'Z').run(conn);
    // await r.time().run(conn);
    // await r.time(1, 1, 1, 1, 1).run(conn);
    // await r.expr(1).time(1, 2, 3, 'Z').run(conn);
    // await r.epochTime(now.getTime()/1000).run(conn);
    // await r.epochTime().run(conn);
    // await r.expr(1).epochTime(Date.now()).run(conn);
    // await r.ISO8601("1986-11-03T08:30:00-08:00").run(conn);
    // await r.ISO8601("1986-11-03T08:30:00", {defaultTimezone: "-08:00"}).run(conn);
    // await r.ISO8601().run(conn);
    // await r.ISO8601(1, 1, 1).run(conn);
    // await r.expr(1).ISO8601('validISOstring').run(conn);
    // await r.now().inTimezone().run(conn);
    // await r.ISO8601("1986-11-03T08:30:00-08:00").timezone().run(conn);
    // await r.now().during(r.time(2013, 12, 1, "Z"), r.now().add(1000)).run(conn);
    // await r.now().during(r.time(2013, 12, 1, "Z"), r.now(), {leftBound: "closed", rightBound: "closed"}).run(conn);
    // await r.now().during(r.time(2013, 12, 1, "Z"), r.now(), {leftBound: "closed", rightBound: "open"}).run(conn);
    // await r.now().during().run(conn);
    // await r.now().during(1).run(conn);
    // await r.now().during(1, 1, 1, 1, 1).run(conn);
    // await r.now().date().hours().run(conn);
    // await r.now().date().minutes().run(conn);
    // await r.now().date().seconds().run(conn);
    // await r.now().timeOfDay().run(conn);
    // await r.now().inTimezone(new Date().toString().match(' GMT([^ ]*)')[1]).year().run(conn);
    // await r.now().inTimezone(new Date().toString().match(' GMT([^ ]*)')[1]).month().run(conn);
    // await r.now().inTimezone(new Date().toString().match(' GMT([^ ]*)')[1]).day().run(conn);
    // await r.now().inTimezone(new Date().toString().match(' GMT([^ ]*)')[1]).dayOfYear().run(conn);
    // await r.now().inTimezone(new Date().toString().match(' GMT([^ ]*)')[1]).dayOfWeek().run(conn);
    // await r.now().toISO8601().run(conn);
    // await r.now().toEpochTime().run(conn);
    // await r.monday.run(conn);
    // await r.expr([r.monday, r.tuesday, r.wednesday, r.thursday, r.friday, r.saturday, r.sunday, r.january, r.february, r.march, r.april, r.may, r.june, r.july, r.august, r.september, r.october, r.november, r.december]).run(conn);
    // await r.epochTime(now.getTime()/1000).run({timeFormat: "raw"});
    // await r.expr(1).run(conn);
    // await r.expr(null).run(conn);
    // await r.expr(false).run(conn);
    // await r.expr(true).run(conn);
    // await r.expr("Hello").run(conn);
    // await r.expr([0, 1, 2]).run(conn);
    // await r.expr({a: 0, b: 1}).run(conn);
    // await r.expr(1).expr("foo").run(conn);
    // await r.expr({a :{b: {c: {d: 1}}}}).run(conn);
    // await r.expr({a :{b: {c: {d: 1}}}}).run(conn);
    // await r.expr([0,1,2,3,4,5,6,8,9]).run({arrayLimit: 2});
    // await r.expr([0,1,2,3,4,5,6,8,9]).run({arrayLimit: 2});
    // await r.expr([0,1,2,3,4,5,6,8,9]).run(conn);
    // await r.expr([0,1,2,3,4,5,6,8,9]).run(conn);
    // await r.expr(NaN).run(conn);
    // await r.expr(Infinity).run(conn);
    // await r.expr('“').run(conn);
    // await r.binary(new Buffer([1,2,3,4,5,6])).run(conn);
    // await r.binary(r.expr("foo")).run(conn);
    // await r.expr(result).coerceTo("STRING").run(conn);
    // await r.expr(new Buffer([1,2,3,4,5,6])).run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.expr([1,2,3]).map(r.row).run(conn);
    // await r.db(dbName).table(tableName).insert({}).run(conn);
    // await r.db(dbName).table(tableName).update({idCopyUpdate: r.row("id")}).run(conn);
    // await r.db(dbName).table(tableName).replace(r.row).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.expr({a: 0, b: 1, c: 2}).pluck("a", "b").run(conn);
    // await r.expr([{a: 0, b: 1, c: 2}, {a: 0, b: 10, c: 20}]).pluck("a", "b").run(conn);
    // await r.db(dbName).table(tableName).pluck().run(conn);
    // await r.expr({a: 0, b: 1, c: 2}).without("c").run(conn);
    // await r.expr([{a: 0, b: 1, c: 2}, {a: 0, b: 10, c: 20}]).without("a", "c").run(conn);
    // await r.db(dbName).table(tableName).without().run(conn);
    // await r.expr({a: 0}).merge({b: 1}).run(conn);
    // await r.expr([{a: 0}, {a: 1}, {a: 2}]).merge({b: 1}).run(conn);
    // await r.expr({a: 0, c: {l: "tt"}}).merge({b: {c: {d: {e: "fff"}}, k: "pp"}}).run(conn);
    // await r.expr({a: 1}).merge({date: r.now()}).run(conn);
    // await r.expr({a: 1}).merge({nested: r.row}, {b: 2}).run(conn);
    // await r.expr({a: {b: 1}}).merge({a: r.literal({c: 2})}).run(conn);
    // await r.expr(1).literal("foo").run(conn);
    // await r.db(dbName).table(tableName).merge().run(conn);
    // await r.expr({foo: 'bar'}).merge({foo: r.literal()}).run(conn);
    // await r.expr([1,2,3]).append(4).run(conn);
    // await r.db(dbName).table(tableName).append().run(conn);
    // await r.expr([1,2,3]).prepend(4).run(conn);
    // await r.db(dbName).table(tableName).prepend().run(conn);
    // await r.expr([1,2,3]).prepend(4).run(conn);
    // await r.db(dbName).table(tableName).difference().run(conn);
    // await r.expr([1,2,3]).setInsert(4).run(conn);
    // await r.expr([1,2,3]).setInsert(2).run(conn);
    // await r.db(dbName).table(tableName).setInsert().run(conn);
    // await r.expr([1,2,3]).setUnion([2,4]).run(conn);
    // await r.db(dbName).table(tableName).setUnion().run(conn);
    // await r.expr([1,2,3]).setIntersection([2,4]).run(conn);
    // await r.db(dbName).table(tableName).setIntersection().run(conn);
    // await r.expr([1,2,3]).setDifference([2,4]).run(conn);
    // await r.db(dbName).table(tableName).setDifference().run(conn);
    // await r.expr({a:0, b:1})("a").run(conn);
    // await r.expr({a:0, b:1}).getField("a").run(conn);
    // await r.expr([{a:0, b:1}, {a:1}])("a").run(conn);
    // await r.db(dbName).table(tableName)().run(conn);
    // await r.db(dbName).table(tableName).getField().run(conn);
    // await r.expr([{a: 0, b: 1, c: 2}, {a: 0, b: 10, c: 20}, {b:1, c:3}]).hasFields("a", "c").run(conn);
    // await r.db(dbName).table(tableName).hasFields().run(conn);
    // await r.expr([1,2,3,4]).insertAt(0, 2).run(conn);
    // await r.expr([1,2,3,4]).insertAt(3, 2).run(conn);
    // await r.db(dbName).table(tableName).insertAt().run(conn);
    // await r.expr([1,2,3,4]).spliceAt(1, [9, 9]).run(conn);
    // await r.db(dbName).table(tableName).spliceAt().run(conn);
    // await r.expr([1,2,3,4]).deleteAt(1).run(conn);
    // await r.expr([1,2,3,4]).deleteAt(1, 3).run(conn);
    // await r.db(dbName).table(tableName).deleteAt().run(conn);
    // await r.db(dbName).table(tableName).deleteAt(1, 1, 1, 1).run(conn);
    // await r.expr([1,2,3,4]).changeAt(1, 3).run(conn);
    // await r.db(dbName).table(tableName).changeAt().run(conn);
    // await r.expr({a:0, b:1, c:2}).keys().orderBy(r.row).run(conn);
    // await r.expr("hello").keys().orderBy(r.row).run(conn);
    // await r.expr({a:0, b:1, c:2}).values().orderBy(r.row).run(conn);
    // await r.object("a", 1, r.expr("2"), "foo").run(conn);
    // await r.expr([1,2,3,4]).run({arrayLimit: 2});
    // await r.expr(1).add("foo").run(conn);
    // await r.db('DatabaseThatDoesNotExist').tableList().run(conn);
    // await r.branch(r.error('a'), 1, 2).run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).wait().run(conn);
    // await r.db(dbName).tableCreate(tableName)('tables_created').run(conn);
    // await r.tableList().run({db: dbName});
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).table(tableName).indexCreate("location", {geo: true}).run(conn);
    // await r.db(dbName).table(tableName).indexWait("location").run(conn);
    // await r.db(dbName).table(tableName).insert(insert_docs).run(conn);
    // await r.circle([0, 0], 2).run(conn);
    // await r.circle(r.point(0, 0), 2).run(conn);
    // await r.circle(r.point(0, 0), 2, {numVertices: 40}).run(conn);
    // await r.circle(r.point(0, 0), 2, {numVertices: 40, fill: false}).run(conn);
    // await r.circle(r.point(0, 0), 1, {unit: "km"}).eq(r.circle(r.point(0, 0), 1000, {unit: "m"})).run(conn);
    // await r.circle(r.point(0, 0), 1, {foo: "bar"}).run(conn)
    // await r.circle(r.point(0, 0)).run(conn)
    // await r.circle(0, 1, 2, 3, 4).run(conn)
    // await r.point(0, 0).distance(r.point(1,1)).run(conn);
    // await r.distance(r.point(0, 0), r.point(1,1)).run(conn);
    // await r.point(0, 0).distance(r.point(1,1), {unit: "km"}).run(conn);
    // await r.point(0, 0).distance().run(conn);
    // await r.point(0, 0).distance(1, 2, 3).run(conn);
    // await r.circle(r.point(0, 0), 2, {numVertices: 40, fill: false}).fill().run(conn);
    // await r.circle(r.point(0, 0), 2, {numVertices: 40, fill: false}).fill(1).run(conn);
    // await r.geojson( {"coordinates":[0,0],"type":"Point"} ).run(conn)
    // await r.geojson(1,2,3).run(conn)
    // await r.geojson( {"coordinates":[0,0],"type":"Point"}).toGeojson().run(conn)
    // await r.point(0, 0).toGeojson(1, 2, 3).run(conn)
    // await r.db(dbName).table(tableName).getIntersecting(r.polygon([0, 0], [0,1], [1,1], [1,0]), {index: "location"}).count().run(conn)
    // await r.db(dbName).table(tableName).getIntersecting(r.polygon([0, 0], [0,1], [1,1], [1,0])).count().run(conn)
    // await r.db(dbName).table(tableName).getNearest(r.point(0, 0), {index: "location", maxResults: 5}).run(conn)
    // await r.db(dbName).table(tableName).getNearest(r.point(0, 0)).count().run(conn)
    // await r.circle(point1, 2000).includes(point2).run(conn);
    // await r.circle([0,0], 2000).includes().run(conn);
    // await r.line([0, 0], [1, 2]).run(conn);
    // await r.line(r.point(0, 0), r.point(1, 2)).run(conn);
    // await r.line().run(conn);
    // await r.point(0, 0).run(conn);
    // await r.point().run(conn);
    // await r.polygon([0, 0], [0, 1], [1, 1]).run(conn);
    // await r.polygon().run(conn);
    // await r.polygon([0, 0], [0, 1], [1, 1], [1, 0]).polygonSub(r.polygon([0.4, 0.4], [0.4, 0.5], [0.5, 0.5])).run(conn);
    // await r.polygon([0, 0], [0, 1], [1, 1]).polygonSub().run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).table(tableName).insert([{val:1}, {val: 2}, {val: 3}]).run(conn);
    // await r.db(dbName).table(tableName).indexCreate("val").run(conn);
    // await r.db(dbName).table(tableName).indexWait("val").run(conn);
    // await r.expr([1,2,3]).innerJoin(r.expr([1,2,3]), function(left, right) { return left.eq(right) }).run(conn);
    // await r.expr([1,2,3]).innerJoin(r.db(dbName).table(tableName), function(left, right) { return left.eq(right("val")) }).run(conn);
    // await r.db(dbName).table(tableName).innerJoin(r.db(dbName).table(tableName), function(left, right) { return left.eq(right) }).run(conn);
    // await r.db(dbName).table(tableName).innerJoin().run(conn);
    // await r.db(dbName).table(tableName).innerJoin(r.expr([1,2,3])).run(conn);
    // await r.expr([1,2,3]).outerJoin(r.expr([1,2,3]), function(left, right) { return left.eq(right) }).run(conn);
    // await r.expr([1,2,3,4]).outerJoin(r.db(dbName).table(tableName), function(left, right) { return left.eq(right("val")) }).run(conn);
    // await r.expr([1,2,3,4]).outerJoin(r.db(dbName).table(tableName), function(left, right) { return left.eq(right("val")) }).run(conn);
    // await r.db(dbName).table(tableName).outerJoin(r.db(dbName).table(tableName), function(left, right) { return left.eq(right) }).run(conn);
    // await r.db(dbName).table(tableName).outerJoin().run(conn);
    // await r.db(dbName).table(tableName).outerJoin(r.expr([1,2,3])).run(conn);
    // await r.expr(pks).eqJoin(function(doc) { return doc; }, r.db(dbName).table(tableName)).run(conn);
    // await r.expr(pks).eqJoin(r.row, r.db(dbName).table(tableName)).run(conn);
    // await r.expr([1,2,3]).eqJoin(r.row, r.db(dbName).table(tableName), {index: "val"}).run(conn);
    // await r.db(dbName).table(tableName).eqJoin().run(conn);
    // await r.expr([1,2,3]).eqJoin(r.row, r.db(dbName).table(tableName), {nonValidKey: "val"}).run(conn);
    // await r.db(dbName).table(tableName).eqJoin("id").run(conn);
    // await r.db(dbName).table(tableName).eqJoin(1, 1, 1, 1, 1).run(conn);
    // await r.expr(pks).eqJoin(function(doc) { return doc; }, r.db(dbName).table(tableName)).zip().run(conn);
    // await r.expr(1).run(conn);
    // await r.dbList().run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.dbCreate().run(conn);
    // await r.expr(1).dbCreate("foo").run(conn);
    // await r.expr(1).db("foo").run(conn);
    // await r.db("-_-").run(conn);
    // await r.dbList().run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbDrop("foo", "bar", "ette").run(conn);
    // await r.dbDrop().run(conn);
    // await r.expr(1).dbDrop("foo").run(conn);
    // await r.expr(1).dbList("foo").run(conn);
    // await r.dbList().run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableList().run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).tableList().run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).tableCreate(tableName, {primaryKey: "foo"}).run(conn);
    // await r.db(dbName).table(tableName).info().run(conn);
    // await r.db(dbName).tableCreate(tableName, {durability: "soft", primaryKey: "foo"}).run(conn);
    // await r.db(dbName).table(tableName).info().run(conn);
    // await r.db(dbName).tableCreate(tableName, {nonValidArg: true}).run(conn);
    // await r.db(dbName).tableCreate().run(conn);
    // await r.db(dbName).tableCreate("-_-").run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).tableList().run(conn);
    // await r.db(dbName).tableDrop(tableName).run(conn);
    // await r.db(dbName).tableList().run(conn);
    // await r.db(dbName).tableDrop().run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).table(tableName).indexCreate("newField").run(conn);
    // await r.db(dbName).table(tableName).indexList().run(conn);
    // await r.db(dbName).table(tableName).indexWait().pluck('index', 'ready').run(conn);
    // await r.db(dbName).table(tableName).indexStatus().pluck('index', 'ready').run(conn);
    // await r.db(dbName).table(tableName).indexDrop("newField").run(conn);
    // await r.db(dbName).table(tableName).indexCreate("field1", function(doc) { return doc("field1") }).run(conn);
    // await r.db(dbName).table(tableName).indexWait('field1').pluck('index', 'ready').run(conn);
    // await r.db(dbName).table(tableName).indexStatus('field1').pluck('index', 'ready').run(conn);
    // await r.db(dbName).table(tableName).indexDrop("field1").run(conn);
    // await r.db(dbName).table(tableName).indexCreate("foo", {multi: true}).run(conn);
    // await r.db(dbName).table(tableName).indexCreate("foo1", r.row("foo"), {multi: true}).run(conn);
    // await r.db(dbName).table(tableName).indexCreate("foo2", function(doc) { return doc("foo")}, {multi: true}).run(conn);
    // await r.db(dbName).table(tableName).indexWait().run(conn);
    // await r.db(dbName).table(tableName).insert({foo: ["bar1", "bar2"], buzz: 1}).run(conn)
    // await r.db(dbName).table(tableName).insert({foo: ["bar1", "bar3"], buzz: 2}).run(conn)
    // await r.db(dbName).table(tableName).getAll("bar1", {index: "foo"}).count().run(conn)
    // await r.db(dbName).table(tableName).getAll("bar1", {index: "foo1"}).count().run(conn)
    // await r.db(dbName).table(tableName).getAll("bar1", {index: "foo2"}).count().run(conn)
    // await r.db(dbName).table(tableName).getAll("bar2", {index: "foo"}).count().run(conn)
    // await r.db(dbName).table(tableName).getAll("bar2", {index: "foo1"}).count().run(conn)
    // await r.db(dbName).table(tableName).getAll("bar2", {index: "foo2"}).count().run(conn)
    // await r.db(dbName).table(tableName).getAll("bar3", {index: "foo"}).count().run(conn)
    // await r.db(dbName).table(tableName).getAll("bar3", {index: "foo1"}).count().run(conn)
    // await r.db(dbName).table(tableName).getAll("bar3", {index: "foo2"}).count().run(conn)
    // await r.db(dbName).table(tableName).indexCreate("buzz", [r.row("buzz")]).run(conn);
    // await r.db(dbName).table(tableName).indexWait().run(conn);
    // await r.db(dbName).table(tableName).getAll([1], {index: "buzz"}).count().run(conn)
    // await r.db(dbName).table(tableName).indexCreate().run(conn);
    // await r.db(dbName).table(tableName).indexDrop().run(conn);
    // await r.db(dbName).table(tableName).indexCreate(toRename).run(conn);
    // await r.db(dbName).table(tableName).indexRename(toRename, renamed).run(conn);
    // await r.db(dbName).table(tableName).indexCreate(existing).run(conn);
    // await r.db(dbName).table(tableName).indexRename(renamed, existing, {overwrite: true}).run(conn);
    // await r.db(dbName).table(tableName).indexCreate(name).run(conn);
    // await r.db(dbName).table(tableName).indexCreate(otherName).run(conn);
    // await r.db(dbName).table(tableName).indexRename(otherName, name).run(conn);
    // await r.db(dbName).table(tableName).indexRename("foo", "bar", {nonValidArg: true}).run(conn);
    // await r.expr(1).add(1).run(conn);
    // await r.expr(1).add(1).add(1).run(conn);
    // await r.expr(1).add(1, 1).run(conn);
    // await r.add(1, 1, 1).run(conn);
    // await r.expr(1).add().run(conn);
    // await r.add().run(conn);
    // await r.add(1).run(conn);
    // await r.expr(1).sub(1).run(conn);
    // await r.sub(5, 3, 1).run(conn);
    // await r.expr(1).sub().run(conn);
    // await r.sub().run(conn);
    // await r.sub(1).run(conn);
    // await r.expr(2).mul(3).run(conn);
    // await r.mul(2, 3, 4).run(conn);
    // await r.expr(1).mul().run(conn);
    // await r.mul().run(conn);
    // await r.mul(1).run(conn);
    // await r.expr(24).div(2).run(conn);
    // await r.div(20, 2, 5, 1).run(conn);
    // await r.expr(1).div().run(conn);
    // await r.div().run(conn);
    // await r.div(1).run(conn);
    // await r.expr(24).mod(7).run(conn);
    // await r.mod(24, 7).run(conn);
    // await r.expr(1).mod().run(conn);
    // await r.mod(24, 7, 2).run(conn);
    // await r.expr(true).and(false).run(conn);
    // await r.expr(true).and(true).run(conn);
    // await r.and(true, true, true).run(conn);
    // await r.and(true, true, true, false).run(conn);
    // await r.and(r.args([true, true, true])).run(conn);
    // await r.and().run(conn);
    // await r.expr(true).or(false).run(conn);
    // await r.expr(false).or(false).run(conn);
    // await r.or(true, true, true).run(conn);
    // await r.or(r.args([false, false, true])).run(conn);
    // await r.or(false, false, false, false).run(conn);
    // await r.or().run(conn);
    // await r.expr(1).eq(1).run(conn);
    // await r.expr(1).eq(2).run(conn);
    // await r.eq(1, 1, 1, 1).run(conn);
    // await r.eq(1, 1, 2, 1).run(conn);
    // await r.expr(1).eq().run(conn);
    // await r.eq().run(conn);
    // await r.eq(1).run(conn);
    // await r.expr(1).ne(1).run(conn);
    // await r.expr(1).ne(2).run(conn);
    // await r.ne(1, 1, 1, 1).run(conn);
    // await r.ne(1, 1, 2, 1).run(conn);
    // await r.expr(1).ne().run(conn);
    // await r.ne().run(conn);
    // await r.ne(1).run(conn);
    // await r.expr(1).gt(2).run(conn);
    // await r.expr(2).gt(2).run(conn);
    // await r.expr(3).gt(2).run(conn);
    // await r.gt(10, 9, 7, 2).run(conn);
    // await r.gt(10, 9, 9, 1).run(conn);
    // await r.expr(1).gt().run(conn);
    // await r.gt().run(conn);
    // await r.gt(1).run(conn);
    // await r.expr(1).ge(2).run(conn);
    // await r.expr(2).ge(2).run(conn);
    // await r.expr(3).ge(2).run(conn);
    // await r.ge(10, 9, 7, 2).run(conn);
    // await r.ge(10, 9, 9, 1).run(conn);
    // await r.ge(10, 9, 10, 1).run(conn);
    // await r.expr(1).ge().run(conn);
    // await r.ge().run(conn);
    // await r.ge(1).run(conn);
    // await r.expr(1).lt(2).run(conn);
    // await r.expr(2).lt(2).run(conn);
    // await r.expr(3).lt(2).run(conn);
    // await r.lt(0, 2, 4, 20).run(conn);
    // await r.lt(0, 2, 2, 4).run(conn);
    // await r.lt(0, 2, 1, 20).run(conn);
    // await r.expr(1).lt().run(conn);
    // await r.lt().run(conn);
    // await r.lt(1).run(conn);
    // await r.expr(1).le(2).run(conn);
    // await r.expr(2).le(2).run(conn);
    // await r.expr(3).le(2).run(conn);
    // await r.le(0, 2, 4, 20).run(conn);
    // await r.le(0, 2, 2, 4).run(conn);
    // await r.le(0, 2, 1, 20).run(conn);
    // await r.expr(1).le().run(conn);
    // await r.le().run(conn);
    // await r.le(1).run(conn);
    // await r.expr(true).not().run(conn);
    // await r.expr(false).not().run(conn);
    // await r.random().run(conn);
    // await r.random(10).run(conn);
    // await r.random(5, 10).run(conn);
    // await r.random(5, 10, {float: true}).run(conn);
    // await r.random(5, {float: true}).run(conn);
    // await r.floor(1.2).run(conn);
    // await r.expr(1.2).floor().run(conn);
    // await r.floor(1.8).run(conn);
    // await r.expr(1.8).floor().run(conn);
    // await r.ceil(1.2).run(conn);
    // await r.expr(1.2).ceil().run(conn);
    // await r.ceil(1.8).run(conn);
    // await r.expr(1.8).ceil().run(conn);
    // await r.round(1.8).run(conn);
    // await r.expr(1.8).round().run(conn);
    // await r.round(1.2).run(conn);
    // await r.expr(1.2).round().run(conn);
    // await r.now().run(conn, {timeFormat: "raw"})
    // await r.expr(1).run(conn)
    // await r.expr(1).run({noreply: true});
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(10000).join('{}, ')+'{}]')).run(conn);
    // await r.expr(1).run(conn);
    // await r.expr(1).run(conn);
    // await r.expr(1).run(conn);
    // await r.expr(1).run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(100).join('{}, ')+'{}]')).run(conn);
    // await r.db(dbName).info().run(conn);
    // await r.db(dbName).table(tableName).info().run(conn);
    // await r.db(dbName).table(tableName).run(conn);
    // await r.db(dbName).table(tableName, {readMode: 'majority'}).run(conn);
    // await r.db(dbName).table(tableName, {readMode: 'majority'}).run(conn);
    // await r.db(dbName).table(tableName, {nonValidKey: false}).run(conn);
    // await r.db(dbName).table(tableName).get(pks[0]).run(conn);
    // await r.db(dbName).table(tableName).get().run(conn);
    // await r.db(dbName).table(tableName).getAll().run(conn);
    // await r.db(dbName).table(tableName).getAll({index: 'id'}).run(conn);
    // await r.db(dbName).table(tableName).update({field: 0}).run(conn);
    // await r.db(dbName).table(tableName).sample(20).update({field: 10}).run(conn);
    // await r.db(dbName).table(tableName).indexCreate('field').run(conn);
    // await r.db(dbName).table(tableName).indexWait('field').pluck('index', 'ready').run(conn);
    // await r.db(dbName).table(tableName).getAll(10, {index: 'field'}).run(conn);
    // await r.db(dbName).table(tableName).insert({field: -1, date: r.now()}).run(conn);
    // await r.db(dbName).table(tableName).getAll(-1, {index: 'field'}).run(conn);
    // await r.db(dbName).table(tableName).getAll(-1, {index: 'field'}).delete().run(conn);
    // await r.db(dbName).table(tableName).indexCreate('fieldAddOne', function(doc) { return doc('field').add(1) }).run(conn);
    // await r.db(dbName).table(tableName).indexWait('fieldAddOne').pluck('index', 'ready').run(conn);
    // await r.db(dbName).table(tableName).getAll(11, {index: 'fieldAddOne'}).run(conn);
    // await r.db(dbName).table(tableName).between(5, 20, {index: 'fieldAddOne'}).run(conn);
    // await r.db(dbName).table(tableName).between(5, 20, {index: 'fieldAddOne', leftBound: 'open', rightBound: 'closed'}).run(conn);
    // await r.db(dbName).table(tableName).between().run(conn);
    // await r.db(dbName).table(tableName).between(1, 2, {nonValidKey: true}).run(conn);
    // await r.db(dbName).table(tableName).filter({field: 10}).run(conn);
    // await r.db(dbName).table(tableName).filter({nonExistingField: 10}).run(conn);
    // await r.db(dbName).table(tableName).filter(function(doc) { return doc("field").eq(10) }).run(conn);
    // await r.db(dbName).table(tableName).filter({nonExistingField: 10}, {default: true}).run(conn);
    // await r.db(dbName).table(tableName).filter({nonExistingField: 10}, {default: false}).run(conn);
    // await r.expr([{a:1}, {}]).filter(r.row("a"), {default: r.error()}).run(conn);
    // await r.db(dbName).table(tableName).filter().run(conn);
    // await r.db(dbName).table(tableName).filter(true, {nonValidKey: false}).run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn)
    // await r.db(dbName).wait().run(conn)
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(numDocs).join('{}, ')+'{}]')).run(conn);
    // await r.db(dbName).table(tableName2).insert(eval('['+new Array(smallNumDocs).join('{}, ')+'{}]')).run(conn);
    // await r.db(dbName).table(tableName).run({stream: true});
    // await r.expr(data).run({stream: true});
    // await r.db(dbName).table(tableName).changes().run({stream: true});
    // await r.db(dbName).table(tableName).insert(data).run(conn);
    // await r.db(dbName).table(tableName).get(1).changes().run({stream: true});
    // await r.db(dbName).table(tableName).insert({id: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).update({update: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).update({update: 2}).run(conn);
    // await r.db(dbName).table(tableName).run(conn, {stream: true, maxBatchRows: 1});
    // await r.db(dbName).table(tableName).run(conn, {stream: true, maxBatchRows: 1});
    // await r.db(dbName).table(tableName).run(conn, {stream: true, maxBatchRows: 1});
    // await r.db(dbName).table(tableName).run(conn, {stream: true, maxBatchRows: 1});
    // await r.db(dbName).table(tableName).limit(10).union([null]).union(r.db(dbName).table(tableName).limit(10)).run(conn, {stream: true, maxBatchRows: 1});
    // await r.db(dbName).table(tableName).run(conn, {stream: true, maxBatchRows: 1});
    // await r.expr("hello").match("hello").run(conn)
    // await r.expr("foo").match().run(conn);
    // await r.expr("helLo").upcase().run(conn);
    // await r.expr("HElLo").downcase().run(conn);
    // await r.expr("foo  bar bax").split().run(conn);
    // await r.expr("12,37,,22,").split(",").run(conn);
    // await r.expr("foo  bar bax").split(null, 1).run(conn);
    // await r.expr(1).add(2).run(conn);
    // await r.expr(3).add(4).run(conn);
    // await r.expr(3).add(4).run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(100).join('{}, ')+'{}]')).run(conn);
    // await r.db(dbName).table(tableName).update({val: r.js("Math.random()")}, {nonAtomic: true}).run(conn);
    // await r.db(dbName).table(tableName).indexCreate('val').run(conn);
    // await r.db(dbName).table(tableName).indexWait('val').run(conn);
    // await r.expr([1,2,3]).map(r.row).run(conn);
    // await r.expr([1,2,3]).map(r.row.add(1)).run(conn);
    // await r.expr([1,2,3]).map(function(doc) { return doc }).run(conn);
    // await r.expr([1,2,3]).map(function(doc) { return doc.add(2)}).run(conn);
    // await r.db(dbName).table(tableName).map().run(conn);
    // await r.expr([{a: 0, b: 1, c: 2}, {a: 4, b: 4, c: 5}, {a:9, b:2, c:0}]).withFields("a").run(conn);
    // await r.expr([{a: 0, b: 1, c: 2}, {a: 4, b: 4, c: 5}, {a:9, b:2, c:0}]).withFields("a", "c").run(conn);
    // await r.db(dbName).table(tableName).withFields().run(conn);
    // await r.expr([[1, 2], [3], [4]]).concatMap(function(doc) { return doc}).run(conn);
    // await r.expr([[1, 2], [3], [4]]).concatMap(r.row).run(conn);
    // await r.db(dbName).table(tableName).concatMap().run(conn);
    // await r.expr([{a:23}, {a:10}, {a:0}, {a:100}]).orderBy("a").run(conn);
    // await r.expr([{a:23}, {a:10}, {a:0}, {a:100}]).orderBy(r.row("a")).run(conn);
    // await r.db(dbName).table(tableName).orderBy({index: "id"}).run(conn);
    // await r.db(dbName).table(tableName).orderBy({index: "val"}).run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).table(tableName).orderBy("id", "a").run(conn);
    // await r.db(dbName).table(tableName).orderBy().run(conn);
    // await r.expr([{a:23}, {a:10}, {a:0}, {a:100}]).orderBy(r.asc(r.row("a"))).run(conn);
    // await r.expr([{a:23}, {a:10}, {a:0}, {a:100}]).orderBy(r.desc(r.row("a"))).run(conn);
    // await r.expr([{a:23}, {a:10}, {a:0}, {a:100}]).orderBy(r.desc("a")).run(conn);
    // await r.expr([{a:23}, {a:10}, {a:0}, {a:100}]).orderBy(r.asc("a")).run(conn);
    // await r.expr(1).desc("foo").run(conn);
    // await r.expr(1).asc("foo").run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).skip(3).run(conn);
    // await r.db(dbName).table(tableName).skip().run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).limit(3).run(conn);
    // await r.db(dbName).table(tableName).limit().run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).slice(3, 5).run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).slice(3).run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).slice(3, {leftBound: "open"}).run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).slice(3, 5, {leftBound: "open"}).run(conn);
    // await r.expr([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22, 23]).slice(5, 10, {rightBound:'closed'}).run(conn);
    // await r.expr([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22, 23]).slice(5, 10, {rightBound:'open'}).run(conn);
    // await r.expr([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22, 23]).slice(5, 10, {leftBound:'open'}).run(conn);
    // await r.expr([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22, 23]).slice(5, 10, {leftBound:'closed'}).run(conn);
    // await r.expr([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22, 23]).slice(5, 10, {leftBound:'closed', rightBound: 'closed'}).run(conn);
    // await r.db(dbName).table(tableName).slice().run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).nth(3).run(conn);
    // await r.db(dbName).table(tableName).nth().run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).nth(3).run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).indexesOf(r.row.eq(3)).run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).indexesOf(function(doc) { return doc.eq(3)}).run(conn);
    // await r.db(dbName).table(tableName).indexesOf().run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).isEmpty().run(conn);
    // await r.expr([]).isEmpty().run(conn);
    // await r.expr([0, 1, 2]).union([3, 4, 5]).run(conn);
    // await r.union([0, 1, 2], [3, 4, 5], [6, 7]).run(conn);
    // await r.union().run(conn);
    // await r.expr([0, 1, 2]).union([3, 4, 5], {interleave: false}).run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).sample(2).run(conn);
    // await r.expr([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).sample(-1).run(conn);
    // await r.db(dbName).table(tableName).sample().run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).wait().run(conn);
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(numDocs).join('{}, ')+'{}]')).run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn)
    // await r.db(dbName).wait().run(conn)
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(numDocs).join('{}, ')+'{}]')).run(conn);
    // await r.dbDrop(dbName).run(conn);
    // await r.dbCreate(dbName).run(conn);
    // await r.db(dbName).tableCreate(tableName).run(conn);
    // await r.db(dbName).table(tableName).insert({}).run(conn);
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(100).join('{}, ')+'{}]')).run(conn);
    // await r.db(dbName).table(tableName).insert([{}, {}]).run(conn);
    // await r.db(dbName).table(tableName).insert(eval('['+new Array(100).join('{}, ')+'{}]')).run(conn);
    // await r.db(dbName).table(tableName).insert({}, {returnChanges: true}).run(conn);
    // await r.db(dbName).table(tableName).insert({}, {returnChanges: false}).run(conn);
    // await r.db(dbName).table(tableName).insert({}, {durability: 'soft'}).run(conn);
    // await r.db(dbName).table(tableName).insert({}, {durability: 'hard'}).run(conn);
    // await r.db(dbName).table(tableName).insert({}, {conflict: 'update'}).run(conn);
    // await r.db(dbName).table(tableName).insert({id: pk, val:1}, {conflict: 'update'}).run(conn);
    // await r.db(dbName).table(tableName).insert({id: pk, val:2}, {conflict: 'replace'}).run(conn);
    // await r.db(dbName).table(tableName).insert({id: pk, val:3}, {conflict: 'error'}).run(conn);
    // await r.db(dbName).table(tableName).insert().run(conn);
    // await r.db(dbName).table(tableName).insert({name: "Michel", age: 27, birthdate: new Date()}).run(conn)
    // await r.db(dbName).table(tableName).insert([{name: "Michel", age: 27, birthdate: new Date()}, {name: "Sophie", age: 23}]).run(conn)
    // await r.db(dbName).table(tableName).insert({}, {nonValidKey: true}).run(conn);
    // await r.db(dbName).table(tableName).replace().run(conn);
    // await r.db(dbName).table(tableName).replace({}, {nonValidKey: true}).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({}).run(conn);
    // await r.db(dbName).table(tableName).delete({durability: "soft"}).run(conn);
    // await r.db(dbName).table(tableName).insert({}).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({}).run(conn);
    // await r.db(dbName).table(tableName).delete({durability: "hard"}).run(conn);
    // await r.db(dbName).table(tableName).insert({}).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).delete({nonValidKey: true}).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({id: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).update({foo: "bar"}).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert([{id: 1}, {id: 2}]).run(conn);
    // await r.db(dbName).table(tableName).update({foo: "bar"}).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);
    // await r.db(dbName).table(tableName).get(2).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({id: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).update({foo: "bar"}, {durability: "soft"}).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({id: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).update({foo: "bar"}, {durability: "hard"}).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({id: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).update({foo: "bar"}, {returnChanges: true}).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({id: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).update({foo: "bar"}, {returnChanges: false}).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);
    // await r.db(dbName).table(tableName).update().run(conn);
    // await r.db(dbName).table(tableName).update({}, {nonValidKey: true}).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({id: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).replace({id: 1, foo: "bar"}).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert([{id: 1}, {id: 2}]).run(conn);
    // await r.db(dbName).table(tableName).replace(r.row.merge({foo: "bar"})).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);
    // await r.db(dbName).table(tableName).get(2).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({id: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).replace({id: 1, foo: "bar"}, {durability: "soft"}).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({id: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).replace({id: 1, foo: "bar"}, {durability: "hard"}).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({id: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).replace({id: 1, foo: "bar"}, {returnChanges: true}).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);
    // await r.db(dbName).table(tableName).delete().run(conn);
    // await r.db(dbName).table(tableName).insert({id: 1}).run(conn);
    // await r.db(dbName).table(tableName).get(1).replace({id: 1, foo: "bar"}, {returnChanges: false}).run(conn);
    // await r.db(dbName).table(tableName).get(1).run(conn);

  });


});
