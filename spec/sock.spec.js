import fs from 'fs';
import r from '../rethinkdb/web';
// import { Observable } from 'rx';
import expect from 'expect';

const debug = require('debug')('app.spec.sock');

// const lorem = 'lorem';
const lorem = fs.readFileSync(`${__dirname}/lorem.txt`, 'utf8');

describe('Socket Connection Class', () => {

  let conn;
  const size = 1000;
  const db = `test_blog${Math.round(Math.random()*100000000)}`;
  // const db = 'test_blog';

  before('Create connection', async function() {
    conn = await r.connect({
      // host: 'localhost',
      user: 'email',
      // password: 'secret',
      timeout: '5',
      db,
    });
    expect(conn.open).toBe(true);
  });

  after('Close connection', async function() {
    await conn.use(db);
    debug(await r.dbDrop(db).promise(conn));
    await conn.close({ noreplyWait: true });
  });

  before('Initialize db', async function () {
    const list = await r.dbList().promise(conn);
    const same = list.find(x => x === db);
    if (same) {
      await r.dbDrop(same).promise(conn);
    }
    await r.dbCreate(db).promise(conn);
    // await r.tableDrop('users').observable(conn);
    await r.db(db).tableCreate('users').promise(conn);
    // r.db(db).table('users').changes()
    //   .filter(r.row('old_val').eq(null))
    //   .observable(conn).subscribe(x => x);
    for(let i = 0; i < size; i++) {
      const score = r.js('Math.round(Math.random() * 10000)');
      const user = {
        name: 'ziggy',
        last: 'zaggy',
        address: '125 main street',
        i, score,
        lorem,
      };
      await r.db(db).table('users').insert(user).promise(conn);
    }
    await r.db(db).table('users').count().promise(conn);
  });

  before('reconnect', async function() {
    // await conn.reconnect({ noreplyWait: true });
    // conn.use(db);
    await r.table('users').insert({ id: '1234', value: '87jhhgg' }).promise(conn);
  });

  it('query table', (done) => {
    const opts = { profile: !true, max_batch_rows: 900 };
    r.table('users')
      .observable(conn, opts).subscribe(x => x, done, done);
  });

  it('Server info', async function() {
    const server = await conn.server();
    expect(server).toIncludeKeys(['id', 'proxy']);
  });

  it('Simple 1', (done) => {
    r.expr('foo').observable(conn)
      .subscribe(resp => expect(resp).toBe('foo'), done, done);
  });

  it('Simple 2', async function() {
    const resp = await r.db(db).table('users').observable(conn).toArray().toPromise();
    expect(resp.length).toBe(size + 1);
    // throw new Error('fake');
  });

  it('Simple 3', async function() {
    const prom = [];
    for (let i = 0; i < 1000; i++) {
      prom.push(r.db(db).table('users').count().promise(conn));
    }
    await Promise.all(prom);
  });

  it('Simple 4', (done) => {
    const source = r.db(db).table('users').observable(conn, { max_batch_rows: 10000 }).count();
    const subscription = source.subscribe(
        (resp) => {
          debug('resp', resp);
          expect(resp).toBe(size + 1);
        },
        done,
        () => {
          debug('completed');
          done();
        });
    setTimeout(() => {
      debug('dispose');
      subscription.dispose();
      setTimeout(done, 5);
    });
  });
});
