import fs from 'fs';
import expect from 'expect';
import jwt from 'jsonwebtoken';
import r from '../rethinkdb/web';

const debug = require('debug')('app.spec.conn');

describe('Connection Class', () => {

  const conn = [];
  const size = 1000;
  const mod = 100;
  let result;

  const signature = fs.readFileSync(`${__dirname}/../.ssh/id_rsa`);
  const algorithm = 'HS256';

  const createToken = (user) => {
    const id = user;
    const email = `${user}@gmail.com`;
    const domain = `${user}.reqlapi.com`;
    const expiresIn = 1000;
    const provider = 'facebook';
    const claims = { domain, id, email, provider };
    const options = { algorithm, expiresIn };
    return jwt.sign(claims, signature, options);
  };

  before('Create connections', async function() {
    for(let i = 0; i < size; i++) {
      const user = `user${i%(size/mod)}`;
      const jwToken = createToken(user);
      const db = 'test';
      conn[i] = await r.connect({ jwToken, db });
      expect(conn[i].open).toBe(true);
    }
  });

  before('Create a table', async function() {
    try {
      result = await r.tableDrop('changes').promise(conn[0]);
      debug(result);
    } catch(err) { debug(err); }
    try {
      result = await r.tableCreate('changes').promise(conn[0]);
      debug(result);
    } catch(err) { debug(err); }
    try {
      const doc = { a: 1, b: 2 };
      result = await r.table('changes').insert(doc).promise(conn[0]);
      debug(result);
    } catch(err) { debug(err); }
  });

  after('Close connection', async function() {
    for(let i = 0; i < size; i++) {
      result = await conn[size -1 - i].close();
      debug(result);
    }
  });

  it('Do some queries', (done) => {
    const c = conn[(size-1)%mod];
    r.db('test').table('changes').observable(c)
      .subscribe(x => debug(x), done, done);
  });

});
