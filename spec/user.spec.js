import r from '../rethinkdb/web';
import expect from 'expect';
import { Observable, Disposable } from 'rx';

const debug = require('debug')('app.spec.user');

describe('Socket User Authentication', () => {

  let conn;
  let result;
  const db = 'test';
  const options = {
    timeout: '5',
    db,
  };

  before('Create connection', async function() {
    conn = await r.connect(options);
    expect(conn.open).toBe(true);
    await conn.reconnect();
    try {
      result = await r.tableCreate('changes').promise(conn);
      debug(result);
    } catch(err) { debug(err); }
  });

  it('should fail query', async function() {
    await conn.close();
    try {
      result = await r.uuid().promise(conn);
      debug(result);
    } catch(error) {
      expect(error.msg).toBe('connection not open');
    }
    conn = await r.connect(options);
    result = await r.uuid().promise(conn);
    debug(result.replace(/-/g, '_'));
  });

  xit('test dispose', (done) => {
    const source = Observable.create((observer) => {
      const tmr = setInterval(() => observer.next('hey'), 1000);
      return Disposable.create(() => {
        clearInterval(tmr);
        done();
      });
    }).subscribe(x => debug(x), (e) => debug(e), () => debug('complete'));
    setTimeout(() => source.dispose(), 10000);
  });

  it('should dispose changes', (done) => {
    const source = Observable.interval(1000)
      .subscribe((x) =>
        r.table('changes').insert({ x }).promise(conn));
    let count = 0;
    const source2 = r.table('changes').changes().observable(conn)
      .subscribe((x) => {
        debug(x);
        if (count++ === 10) {
          source.dispose();
          source2.dispose();
          setTimeout(done, 5000);
        }
      });
  });

});
