import webpack from 'webpack';
import config from '../webpack.config';
import koaWebpackDevMiddleware from 'koa-webpack-dev-middleware';
import koaWebpackHotMiddleware from 'koa-webpack-hot-middleware';

const webpackConfig = (app) => {
  const compiler = webpack(config);
  app.use(koaWebpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
    historyApiFallback: true,
    contentBase: '.',
    stats: {
      colors: true,
    },
  }));
  app.use(koaWebpackHotMiddleware(compiler));
};

export default webpackConfig;
