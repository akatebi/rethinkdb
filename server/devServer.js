/* eslint no-console:0, no-unused-vars:1, func-names:0 */

// babel server -d build
// node -r babel-register --expose-gc build/devServer.js
// const debug = require('debug')('app:devServer');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
// process.env.NODE_ENV = 'production';
import 'babel-polyfill';
import koa from 'koa';
import http from 'http';
import './proxy';
import './dbInit';
import './socketio';
import routes from './routes';

const app = koa();
const port = 3000;
const hostname = '0.0.0.0';

import webpackConfig from './webpack';

if (process.env.NODE_ENV === 'development') {
  webpackConfig(app);
}

routes(app);

const httpServer = http.createServer(app.callback());

httpServer.listen(port, hostname, (error) => {
  if (error) {
    console.error(error);
  } else {
    console.info('==> ✅  Web Server is listening');
    console.info('==> 🌎  Go to http://%s:%s', hostname, port);
  }
});
