import http from 'http';
import SocketIO from 'socket.io';
import connect from '../rethinkdb/server';
import authorize from './authorize';

// const debug = require('debug')('app.socketio');

const socketio = (httpServer) => {

  const io = new SocketIO(httpServer);

  // Allow hosts for CORS
  // io.origins('s1.reqlapi.com:* xeqlapi.com:*');

  // Verify access token
  io.of('rethinkdb').use((socket, next) => {
    // debug('Handshake', socket.request.headers);
    if (authorize(socket.request._query) || 1) next();
    else next(new Error('Authentication error'));
  });

  io.of('rethinkdb').on('connection', (socket) => {
    const claims = authorize(socket.request._query);
    connect(socket, claims);
  });

};

const port = 4000;
const hostname = '0.0.0.0';
const httpServer = http.createServer();

httpServer.listen(port, hostname, (error) => {
  if (error) {
    console.error(error);
  } else {
    console.info('==> ✅  API Server is listening');
    console.info('==> 🌎  Go to reqlapi://%s:%s', hostname, port);
  }
});

socketio(httpServer);

export default socketio;
