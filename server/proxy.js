import koa from 'koa';
import proxy from 'koa-proxy';

const app = koa();
app.use(proxy({
  host: 'http://rdb.com:8080',
}));
app.listen(18888);

import fs from 'fs';
import proxy2 from 'koa-http-proxy';
//
// Create your proxy server and set the target in the options.
//
const key = fs.readFileSync(`${__dirname}/../cert/key.pem`, 'utf8');
const cert = fs.readFileSync(`${__dirname}/../cert/cert.pem`, 'utf8');
const app2 = koa();
app2.use(proxy2({
  target: {
    host: 'localhost',
    port: 8080,
  },
  secure: true,
  ssl: {
    key,
    cert,
  },
})).listen(8888);
