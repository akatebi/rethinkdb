import koa from 'koa';
import vhost from 'koa-vhost';
import touch from 'touch';

const debug = require('debug')('app:subdomain');

export const vhostDynamic = (server) =>
  function*() {
    const app = koa();
    let { host } = this;
    const { subdomain } = this.request.body;
    this.status = 200;
    host = host.split(':')[0];
    host = `${subdomain}.${host}`;
    debug(host);
    server.use(vhost({ app, host }));
    app.use(function* () {
      this.body = host;
    });
    setTimeout(() => touch(`${__dirname}/vhost.js`, { mtime: true }, (x) => debug('touch', x)), 1000);
  };

export const vhostStatic = (server, subdomain) => {
  const app = koa();
  let host = 'reqlapi.com';
  host = `${subdomain}.${host}`;
  debug(subdomain, host);
  server.use(vhost({ app, host }));
  app.use(function* () {
    this.body = `server ==>> ${subdomain}`;
  });
};

// curl -H "Content-Type: application/json" -X POST -d  '{"subdomain": "s1"}' reqlapi.com:3000/vhost
